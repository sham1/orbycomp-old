;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((eval . (setq-local compile-command "ninja -k 0 -C build "))))
 (c++-mode .
	 ((comment-style . extra-line)
	  (eval . (c-set-style "linux"))
	  (eval . (when (fboundp 'eglot-ensure)
		    (eglot-ensure))))))
