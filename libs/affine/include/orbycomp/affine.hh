/*
 * affine.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <array>

namespace affine
{
class Vec4
{
	std::array<float, 4> data_{};

public:
	Vec4() = default;
	constexpr Vec4(float x, float y, float z, float w) : data_({x, y, z, w}) {}

	operator std::array<float, 4>() const { return std::array<float, 4>(data_); }

	enum class Axis { X, Y, Z, W };

	float operator[](Axis axis) const;
	float &operator[](Axis axis);

	Vec4 operator*(float rhs) const;
	Vec4 &operator*=(float rhs);

	Vec4 operator/(float rhs) const;
	Vec4 &operator/=(float rhs);

	bool operator==(const Vec4 &rhs) const;
	bool operator!=(const Vec4 &rhs) const;
};

Vec4 operator+(const Vec4 &lhs, const Vec4 &rhs);
Vec4 operator*(float lhs, const Vec4 &rhs);

class Mat4
{
	Vec4 i_;
	Vec4 j_;
	Vec4 k_;
	Vec4 w_;

public:
	Mat4() = default;

	constexpr Mat4(const Vec4 &i, const Vec4 &j, const Vec4 &k, const Vec4 &w)
	    : i_(i), j_(j), k_(k), w_(w)
	{
	}

	static constexpr Mat4
	get_identity()
	{
		constexpr Vec4 i = {1, 0, 0, 0};
		constexpr Vec4 j = {0, 1, 0, 0};
		constexpr Vec4 k = {0, 0, 1, 0};
		constexpr Vec4 w = {0, 0, 0, 1};

		return Mat4{i, j, k, w};
	}

	operator std::array<float, 16>() const;

	Mat4 &operator*=(const Mat4 &rhs);

	enum class Basis { I, J, K, W };

	Vec4 operator[](Basis basis) const;
	Vec4 &operator[](Basis basis);

	bool operator==(const Mat4 &rhs) const;
	bool operator!=(const Mat4 &rhs) const;
};

Mat4 operator*(float lhs, const Mat4 &rhs);
Mat4 operator*(const Mat4 &lhs, const Mat4 &rhs);

Vec4 operator*(const Mat4 &lhs, const Vec4 &rhs);

struct Vec3 {
	float x, y, z;

	constexpr operator Vec4() const { return Vec4{x, y, z, 0}; }

	float dot(const Vec3 &other) const;
	Vec3 cross(const Vec3 &other) const;
	float magnitude() const;

	Vec3 normalized() const;
	Vec3 &normalize();

	Vec3 operator/(float rhs) const;
	Vec3 &operator/=(float rhs);

	Vec3 operator+(const Vec3 &rhs) const;
	Vec3 &operator+=(const Vec3 &rhs);

	Vec3 operator-() const;

	Vec3 operator-(const Vec3 &rhs) const;
	Vec3 &operator-=(const Vec3 &rhs);

	bool operator==(const Vec3 &rhs) const;
	bool operator!=(const Vec3 &rhs) const;
};

Vec3 operator*(float lhs, const Vec3 &rhs);
Vec3 operator*(const Mat4 &lhs, const Vec3 &rhs);

struct Point3 {
	float x, y, z;

	constexpr operator Vec4() const { return Vec4{x, y, z, 1}; }

	bool operator==(const Point3 &rhs) const;
	bool operator!=(const Point3 &rhs) const;
};

Point3 operator+(const Vec3 &lhs, const Point3 &rhs);
Vec3 operator-(const Point3 &lhs, const Point3 &rhs);
Point3 operator*(const Mat4 &lhs, const Point3 &rhs);

Mat4 create_xy_rotation_matrix(float sin, float cos);
Mat4 create_translation_matrix(const Point3 &new_origin);
Mat4 create_uniform_scaling_matrix(float scale);
Mat4 create_scaling_matrix(float x, float y, float z);

Mat4 create_ortho_matrix(float left, float right, float bottom, float top, float near, float far);

} // namespace affine
