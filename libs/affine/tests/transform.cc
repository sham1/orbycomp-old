/*
 * transform.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <orbycomp/affine.hh>

using namespace affine;

SCENARIO("Vectors and points can be transformed in various ways", "[transform]")
{
	GIVEN("A point at (2, 3, 4)")
	{
		Point3 point{2, 3, 4};

		WHEN("a vector (1, 0, 0) is added to it")
		{
			Vec3 vector{1, 0, 0};

			point = vector + point;

			THEN("the point is then at (3, 3, 4)") { REQUIRE(point == Point3{3, 3, 4}); }
		}
	}

	AND_GIVEN("Two points, first from (1, 0, 0) and other from (6, 5, 9)")
	{
		Point3 point1{1, 0, 0};
		Point3 point2{6, 5, 9};

		WHEN("they are subtracted from each other")
		{
			Vec3 result = point2 - point1;

			THEN("the resulting vector is (5, 5, 9)") { REQUIRE(result == Vec3{5, 5, 9}); }

			AND_THEN("adding it to the first gives second")
			{
				REQUIRE(result + point1 == point2);
			}
		}
	}

	AND_GIVEN("A translation matrix and a point and a vector")
	{
		Mat4 matrix = Mat4::get_identity();
		matrix[Mat4::Basis::W] = {2, 3, 4, 1};

		Point3 point;
		Vec3 vector{1, 0, 0};

		WHEN("they get transformed by the matrix")
		{
			Point3 translated_point = matrix * point;
			Vec3 translated_vector = matrix * vector;

			THEN("the point moves to (2, 3, 4)") { REQUIRE(translated_point == Point3{2, 3, 4}); }

			AND_THEN("but the vector doesn't get translated")
			{
				REQUIRE(translated_vector == vector);
			}
		}
	}
}
