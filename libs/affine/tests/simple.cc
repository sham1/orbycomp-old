/*
 * simple.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <orbycomp/affine.hh>

using namespace affine;

SCENARIO("Check that stuff works as it should", "[simple]")
{
	GIVEN("A default vector")
	{
		Vec4 vec;

		THEN("all the components are zero")
		{
			REQUIRE(vec[Vec4::Axis::X] == Approx(0));
			REQUIRE(vec[Vec4::Axis::Y] == Approx(0));
			REQUIRE(vec[Vec4::Axis::Z] == Approx(0));
			REQUIRE(vec[Vec4::Axis::W] == Approx(0));
		}
	}
	AND_GIVEN("A default matrix")
	{
		Mat4 matrix;

		THEN("all the basis are zero-vectors")
		{
			REQUIRE(matrix[Mat4::Basis::I] == Vec4{});
			REQUIRE(matrix[Mat4::Basis::J] == Vec4{});
			REQUIRE(matrix[Mat4::Basis::K] == Vec4{});
			REQUIRE(matrix[Mat4::Basis::W] == Vec4{});
		}
	}
	AND_GIVEN("An identity matrix")
	{
		Mat4 matrix = Mat4::get_identity();

		THEN("the basis are what one would expect")
		{
			REQUIRE(matrix[Mat4::Basis::I] == Vec4{1, 0, 0, 0});
			REQUIRE(matrix[Mat4::Basis::J] == Vec4{0, 1, 0, 0});
			REQUIRE(matrix[Mat4::Basis::K] == Vec4{0, 0, 1, 0});
			REQUIRE(matrix[Mat4::Basis::W] == Vec4{0, 0, 0, 1});
		}
		AND_THEN("it serializes as expected")
		{
			std::array<float, 16> data = matrix;
			std::array<float, 16> expected{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
			REQUIRE(data == expected);
		}
	}
}
