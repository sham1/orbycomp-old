/*
 * matrices.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <orbycomp/affine.hh>

using namespace affine;

SCENARIO("Transformation matrices can be created with transform factories", "[matrices]")
{
	GIVEN("A point at (1, 0, 0)")
	{
		Point3 point{1, 0, 0};

		WHEN("a translation matrix to (4, 5, 2) is applied")
		{
			Mat4 matrix = create_translation_matrix({4, 5, 2});
			Point3 new_point = matrix * point;

			THEN("the new point is at (5, 5, 2)") { REQUIRE(new_point == Point3{5, 5, 2}); }
		}
	}

	AND_GIVEN("Rotation matrices for 90 degree and -90 degree rotations")
	{
		// sin(90 degrees) = 1, cos(90 degrees) = 0
		Mat4 matrix1 = create_xy_rotation_matrix(1, 0);
		// sin(-90 degrees) = -1, cos(-90 degrees) = 0
		Mat4 matrix2 = create_xy_rotation_matrix(-1, 0);

		THEN("they are as expected")
		{
			REQUIRE(matrix1 == Mat4{{0, 1, 0, 0}, {-1, 0, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});

			REQUIRE(matrix2 == Mat4{{0, -1, 0, 0}, {1, 0, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});
		}

		AND_WHEN("they are multiplied together")
		{
			Mat4 mult = matrix1 * matrix2;

			THEN("they produce an identity matrix") { REQUIRE(mult == Mat4::get_identity()); }
		}

		AND_WHEN("there is a vector (1, 0, 0)")
		{
			Vec3 vector{1, 0, 0};

			THEN("with first it becomes (0, 1, 0)")
			{
				REQUIRE(matrix1 * vector == Vec3{0, 1, 0});
			}

			AND_THEN("with second it becomes (0, -1, 0)")
			{
				REQUIRE(matrix2 * vector == Vec3{0, -1, 0});
			}
		}
	}
}
