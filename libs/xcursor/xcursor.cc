/*
 * xcursor.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <algorithm>
#include <cstdio>
#include <filesystem>
#include <fstream>
#include <istream>
#include <optional>
#include <orbycomp/xcursor.hh>
#include <string>
#include <vector>

namespace orbycomp
{

namespace xcursor
{

// TODO: Do this totally with std::string_view when C++20 becomes official
std::vector<std::string>
split_path(const std::string_view &path)
{
	std::vector<std::string> ret{};

	auto chunk_begin = std::begin(path);
	while (chunk_begin != std::end(path)) {
		auto chunk_end = std::find(chunk_begin, std::end(path), ':');
		ret.emplace_back(chunk_begin, chunk_end);
		if (chunk_end == std::end(path)) {
			chunk_begin = chunk_end;
		} else {
			chunk_begin = chunk_end + 1;
		}
	}

	return ret;
}

namespace fs = std::filesystem;

CursorContext
CursorContext::load_theme(const std::string &name)
{
	auto library_paths = split_path(XcursorLibraryPath());
	CursorContext ctx{};

	load_theme_with_name(ctx, library_paths, name);

	return ctx;
}

void
CursorContext::load_theme_with_name(CursorContext &ctx,
				    const std::vector<std::string> &library_paths,
				    const std::string &name)
{
	bool found = false;

	for (const auto &library_path : library_paths) {
		fs::path theme_dir_path{library_path};
		theme_dir_path /= name;

		fs::directory_entry theme_dir{theme_dir_path};
		if (!theme_dir.exists() || !theme_dir.is_directory()) {
			continue;
		}

		load_theme_from(ctx, library_paths, theme_dir_path);

		found = true;
	}

	if (!found) {
		std::stringstream ss;
		ss << "Could not find cursor theme \"" << name << "\"";
		throw std::runtime_error(ss.str());
	}
}

std::optional<std::vector<std::string>> parse_index_theme(const fs::path &index_theme_path);

void
CursorContext::load_theme_from(CursorContext &ctx,
			       const std::vector<std::string> &library_paths,
			       const fs::path &theme_dir_path)
{
	bool has_xcursors = false;
	bool has_index_theme = false;

	for (auto &p : fs::directory_iterator{theme_dir_path}) {
		fs::path p_ = p;
		if (p_.filename() == "index.theme") {
			has_index_theme = true;
		}
		if (p_.filename() == "cursors") {
			has_xcursors = true;
		}
	}

	if (!has_index_theme) {
		throw std::runtime_error("Could not find index.theme for theme");
	}

	fs::path index_theme_path = theme_dir_path / "index.theme";

	std::optional<std::vector<std::string>> inherits_from = parse_index_theme(index_theme_path);

	if (!has_xcursors && !inherits_from) {
		throw std::runtime_error("Theme has no cursors");
	}

	if (inherits_from) {
		for (const auto &theme_name : *inherits_from) {
			load_theme_with_name(ctx, library_paths, theme_name);
		}
	}

	if (has_xcursors) {
		for (auto &cursor_file : fs::directory_iterator{theme_dir_path / "cursors"}) {

			if (!cursor_file.is_regular_file()) {
				continue;
			}

			fs::path cursor_path = cursor_file;
			std::unique_ptr<std::FILE, decltype(&std::fclose)> cursor_file_ptr{
			    fopen(cursor_path.c_str(), "rb"), std::fclose};
			if (!cursor_file_ptr) {
				continue;
			}

			CursorPtr cursor_ptr{XcursorFileLoadAllImages(cursor_file_ptr.get()),
					     XcursorImagesDestroy};
			if (!cursor_ptr) {
				throw std::bad_alloc();
			}

			XcursorImagesSetName(cursor_ptr.get(), cursor_path.filename().c_str());

			ctx.cursor_images_.insert_or_assign(cursor_path.filename(), std::move(cursor_ptr));
		}
	}
}

std::optional<std::vector<std::string>>
parse_index_theme(const fs::path &index_theme_path)
{
	std::fstream index_theme_stream{index_theme_path, std::ios::binary | std::ios::in};

	bool found_icon_theme_section = false;
	for (std::string line; std::getline(index_theme_stream, line);) {
		if (!found_icon_theme_section && line == "[Icon Theme]") {
			found_icon_theme_section = true;
		}

		if (!found_icon_theme_section) {
			continue;
		}

		if (line.find("Inherits") != std::string::npos) {
			std::vector<std::string> ret{};
			auto inherits_theme_list_iter = std::find(line.begin(), line.end(), '=') + 1;

			auto chunk_begin = inherits_theme_list_iter;
			while (chunk_begin != std::end(line)) {
				auto chunk_end = std::find(chunk_begin, std::end(line), ',');
				ret.emplace_back(chunk_begin, chunk_end);
				if (chunk_end == std::end(line)) {
					chunk_begin = chunk_end;
				} else {
					chunk_begin = chunk_end + 1;
				}
			}

			return std::make_optional(std::move(ret));
		}
	}

	return std::nullopt;
}

XcursorImage *
CursorContext::get_cursor(const std::string &name, uint32_t size)
{
	auto cursor_entry = cursor_images_.find(name);
	if (cursor_entry == cursor_images_.end()) {
		return nullptr;
	}

	XcursorImages *imgs = cursor_entry->second.get();
	for (int i = 0; i < imgs->nimage; ++i) {
		XcursorImage *img = imgs->images[i];
		if (img->size == size) {
			return img;
		}
	}

	return nullptr;
}

} // namespace xcursor

} // namespace orbycomp
