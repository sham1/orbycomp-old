/*
 * xcursor.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <X11/Xcursor/Xcursor.h>
#include <filesystem>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace orbycomp
{

namespace xcursor
{

class CursorContext
{
	using CursorPtr = std::unique_ptr<XcursorImages, decltype(&XcursorImagesDestroy)>;

	std::unordered_map<std::string, CursorPtr> cursor_images_{};

	static void load_theme_from(CursorContext &ctx,
				    const std::vector<std::string> &library_paths,
				    const std::filesystem::path &theme_dir_path);

	static void load_theme_with_name(CursorContext &ctx,
					 const std::vector<std::string> &library_paths,
					 const std::string &name);

public:
	XcursorImage *get_cursor(const std::string &name, uint32_t size = 24);

	static CursorContext load_theme(const std::string &name = "default");
};

}; // namespace xcursor

}; // namespace orbycomp
