/*
 * lua.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <lua.hpp>
#include <memory>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>

namespace orbycomp
{

namespace lua
{

namespace impl
{

void push(struct lua_State *L, std::nullptr_t);

void push(struct lua_State *L, const std::string &str);

void push(struct lua_State *L, lua_Integer n);

void push(struct lua_State *L, lua_Unsigned n);

void push(struct lua_State *L, lua_Number n);

void push(struct lua_State *L, bool b);

void push(struct lua_State *L, int n);

void push(struct lua_State *L, unsigned int n);

void push(struct lua_State *L, const char *str);

void push(struct lua_State *L, lua_CFunction func);

template <typename... Args>
inline void
push_all(struct lua_State *L, Args... args)
{
	if constexpr (sizeof...(args) == 0) {
		return;
	}

	lua_checkstack(L, sizeof...(args));
	(push(L, std::forward<Args>(args)), ...);
}

template <typename T> T get(struct lua_State *L, int stack_pos) = delete;

template <> std::nullptr_t get<std::nullptr_t>(struct lua_State *L, int stack_pos);

template <> std::string get<std::string>(struct lua_State *L, int stack_pos);

template <> lua_Integer get<lua_Integer>(struct lua_State *L, int stack_pos);

template <> lua_Unsigned get<lua_Unsigned>(struct lua_State *L, int stack_pos);

template <> lua_Number get<lua_Number>(struct lua_State *L, int stack_pos);

template <> bool get<bool>(struct lua_State *L, int stack_pos);

template <typename T> T get_unchecked(struct lua_State *L, int stack_pos) = delete;

template <> std::nullptr_t get_unchecked<std::nullptr_t>(struct lua_State *L, int stack_pos);

template <> std::string get_unchecked<std::string>(struct lua_State *L, int stack_pos);

template <> lua_Integer get_unchecked<lua_Integer>(struct lua_State *L, int stack_pos);

template <> lua_Unsigned get_unchecked<lua_Unsigned>(struct lua_State *L, int stack_pos);

template <> lua_Number get_unchecked<lua_Number>(struct lua_State *L, int stack_pos);

template <> bool get_unchecked<bool>(struct lua_State *L, int stack_pos);

// Error throwing functions. Warning: these throw C++ exceptions! Please guard
// accordingly!
[[noreturn]] void throw_type_error(struct lua_State *L, int arg, const std::string &tname);

[[noreturn]] void throw_argument_error(struct lua_State *L, int arg, const std::string &extramsg);

[[noreturn]] void throw_error(struct lua_State *L, const std::string &msg);

}; // namespace impl

class Context
{
public:
	class LuaWrapper;

private:
	struct Lua_State_Destroyer {
		inline void
		operator()(lua_State *L)
		{
			lua_close(L);
		}
	};
	std::unique_ptr<lua_State, Lua_State_Destroyer> L_;

	// Wrap around an existing lua_State
	Context(struct lua_State *L) : L_{L} {}

	// Used to implement `Context::wrap_with_exceptions`.
	// If an exception occured within `f_`, `had_error` will be
	// `true` and the top of the Lua stack shall contain the
	// error string.
	static void
	wrap_with_try(struct lua_State *L, std::function<void(LuaWrapper &)> &&f_, bool &had_error);

	// Used to implement `Context::wrap_function`.
	static void wrap_function_real(struct lua_State *L,
				       std::function<int(LuaWrapper &)> &&f,
				       int &ret_count,
				       bool &had_error);

public:
	template <typename T> class Userdata;

	inline lua_State *
	L()
	{
		return L_.get();
	}

	static Context create_with_stdlibs();

	Context();
	~Context();

	Context(const Context &other) = delete;
	Context &operator=(const Context &other) = delete;

	Context(Context &&other) = default;
	Context &operator=(Context &&other) = default;

	template <typename T>
	void
	push(T val)
	{
		impl::push(L(), val);
	}

	template <typename... Args>
	void
	push_all(Args... args)
	{
		impl::push_all(L(), args...);
	}

	template <typename T>
	T
	get(int stack_pos)
	{
		return impl::get<T>(L(), stack_pos);
	}

	template <typename T>
	T
	get_unchecked(int stack_pos)
	{
		return impl::get_unchecked<T>(L(), stack_pos);
	}

	class LuaWrapper
	{
	private:
		friend Context;
		Context *ctx_;

		LuaWrapper(Context *ctx) : ctx_{ctx} {}
		~LuaWrapper() {}

	public:
		inline struct lua_State *
		L()
		{
			return ctx_->L();
		}

		template <typename T>
		void
		push(T val)
		{
			ctx_->push<T>(val);
		}

		template <typename... Args>
		void
		push_all(Args... args)
		{
			ctx_->push_all(args...);
		}

		template <typename T>
		T
		get(int stack_pos)
		{
			return ctx_->get<T>(stack_pos);
		}

		template <typename T>
		T
		get_unchecked(int stack_pos)
		{
			return ctx_->get_unchecked<T>(stack_pos);
		}

		void
		set_top(int index)
		{
			ctx_->set_top(index);
		}

		void
		pop(int count)
		{
			ctx_->pop(count);
		}

		void
		push_value(int index)
		{
			ctx_->push_value(index);
		}

		void
		remove(int index)
		{
			ctx_->remove(index);
		}

		void
		insert(int index)
		{
			ctx_->insert(index);
		}

		void
		replace(int index)
		{
			ctx_->replace(index);
		}

		void
		get_global(const std::string &name)
		{
			ctx_->get_global(name);
		}

		int
		pcall(int arg_count, int result_count, int err_func_idx = 0)
		{
			return ctx_->pcall(arg_count, result_count, err_func_idx);
		}

		template <typename T, typename... Args>
		Userdata<T>
		create_userdata(Args... args)
		{
			return ctx_->create_userdata<T>(args...);
		}

		template <typename T>
		Userdata<T>
		create_unowned_userdata(T *t)
		{
			return ctx_->create_unowned_userdata(t);
		}

		template <typename T>
		Userdata<T>
		get_userdata(int idx = -1)
		{
			return ctx_->get_userdata<T>(idx);
		}

		template <typename T>
		Userdata<T>
		get_userdata_with_meta(const std::string &name, int idx = -1)
		{
			return Userdata<T>::get_checked_from_stack(L(), name, idx);
		}

		void
		ensure_type(int idx, int tag)
		{
			int val_tag = lua_type(L(), idx);
			if (val_tag != tag) {
				impl::throw_type_error(L(), idx, lua_typename(L(), tag));
			}
		}

		void
		new_table()
		{
			ctx_->new_table();
		}

		void
		new_metatable(const std::string &str)
		{
			ctx_->new_metatable(str);
		}

		void
		get_metatable(const std::string &str)
		{
			ctx_->get_metatable(str);
		}

		void
		set_metatable(int idx = -1)
		{
			ctx_->set_metatable(idx);
		}

		void
		push_closure(lua_CFunction func, int upvalue_count)
		{
			ctx_->push_closure(func, upvalue_count);
		}

		void
		set_table(int table_idx)
		{
			ctx_->set_table(table_idx);
		}

		void
		set_table_field(const std::string &name, int table_idx)
		{
			ctx_->set_table_field(name, table_idx);
		}

		void
		register_lib(const struct luaL_Reg *registry, int shared_upvalue_count = 0)
		{
			ctx_->register_lib(registry, shared_upvalue_count);
		}

		void
		push_light_userdata(void *light_userdata)
		{
			ctx_->push_light_userdata(light_userdata);
		}
	};

	// Wraps a call to `f` in such a way, that exceptions thrown by
	// `f` get automatically converted to Lua exceptions.
	//
	// Please allocate any RAII-enabled resources within `f` so they
	// get managed properly if an exception occurs. Also if at all
	// possible, try to avoid calling any function within `f` that
	// would call `lua_error` or equivalent, since those functions
	// use `longjmp` under most configurations and thus do not take
	// RAII into account, leading to leaked memory alongside other
	// such problems.
	static void wrap_with_exceptions(struct lua_State *L, std::function<void(LuaWrapper &)> &&f);

	// Wraps the call to `f` in such a way that exceptions thrown by
	// `f` get automatically converted to Lua exceptions.
	//
	// Works similarly to ``Context::wrap_with_exceptions` except that
	// `f` returns the number of return values left in the stack.
	// Used to implement Lua-callable functions in C++.
	static int wrap_function(struct lua_State *L, std::function<int(LuaWrapper &)> &&f);

	void
	set_top(int index)
	{
		lua_settop(L(), index);
	}

	void
	pop(int count)
	{
		lua_pop(L(), count);
	}

	void
	push_value(int index)
	{
		lua_pushvalue(L(), index);
	}

	void
	remove(int index)
	{
		lua_remove(L(), index);
	}

	void
	insert(int index)
	{
		lua_insert(L(), index);
	}

	void
	replace(int index)
	{
		lua_replace(L(), index);
	}

	void
	get_global(const std::string &name)
	{
		lua_getglobal(L(), name.c_str());
	}

	void
	set_global(const std::string &name)
	{
		lua_setglobal(L(), name.c_str());
	}

	void
	set_table(int idx)
	{
		lua_settable(L(), idx);
	}

	int
	pcall(int arg_count, int result_count, int err_func_idx = 0)
	{
		return lua_pcall(L(), arg_count, result_count, err_func_idx);
	}

	class Ref
	{
		struct lua_ref {
			Context *ctx_;
			int idx{LUA_NOREF};

			lua_ref(Context *ctx) : ctx_{ctx}, idx{luaL_ref(ctx_->L(), LUA_REGISTRYINDEX)} {}

			~lua_ref() { luaL_unref(ctx_->L(), LUA_REGISTRYINDEX, idx); }
		};

	public:
		Ref(Context &ctx) : ref_{std::make_shared<lua_ref>(&ctx)} {}

		Ref(LuaWrapper &wrap) : Ref{*wrap.ctx_} {}

		Ref(const Ref &) = default;
		Ref &operator=(const Ref &) = default;

		Ref(Ref &&) = default;
		Ref &operator=(Ref &&) = default;

		static Ref
		for_index(Context &ctx, int idx = -1)
		{
			ctx.push_value(idx);
			return Ref{ctx};
		}

		void
		get_value() const
		{
			lua_rawgeti(ref_->ctx_->L(), LUA_REGISTRYINDEX, ref_->idx);
		}

		void
		set_value()
		{
			lua_rawseti(ref_->ctx_->L(), LUA_REGISTRYINDEX, ref_->idx);
		}

		~Ref() {}

		Context *
		ctx()
		{
			return ref_->ctx_;
		}

	private:
		std::shared_ptr<lua_ref> ref_;
	};

	template <typename T> class Userdata
	{
		friend Context;
		friend Context::LuaWrapper;

	public:
		// Only call in __gc-metamethod!
		//
		// This function is safe to call for objects created
		// with `Context::create_unowned_userdata` or
		// `LuaWrapper::create_unowned_userdata`, because this
		// will not call `delete` on data it doesn't own.
		void
		clear()
		{
			if ((wrap_) && (wrap_->t_) && wrap_->owned_) {
				delete (wrap_->t_);
				wrap_->t_ = nullptr;
			}
			wrap_ = nullptr;
		}

		~Userdata() {}

		Userdata(const Userdata &) = default;
		Userdata(Userdata &&) = default;

		Userdata &operator=(const Userdata &) = default;
		Userdata &operator=(Userdata &&) = default;

		T *
		operator->() const noexcept
		{
			return wrap_->t_;
		}

		typename std::add_lvalue_reference<T>::type
		operator*() const
		{
			return wrap_->t_;
		}

		T *
		get() const noexcept
		{
			return wrap_->t_;
		}

	private:
		struct TWrap {
			T *t_;

			// Wheter the object pointed to is
			// owned by Userdata.
			bool owned_{false};
		};

		TWrap *wrap_{nullptr};

		Userdata() = default;

		static Userdata<T>
		get_from_stack(struct lua_State *L, int idx)
		{
			Userdata ret;

			ret.wrap_ = static_cast<TWrap *>(lua_touserdata(L, idx));

			return ret;
		}

		static Userdata<T>
		get_checked_from_stack(struct lua_State *L, const std::string &name, int idx)
		{
			Userdata<T> ret;
			void *to_wrap = luaL_testudata(L, idx, name.c_str());
			if (!to_wrap) {
				impl::throw_type_error(L, idx, name);
			}

			ret.wrap_ = static_cast<TWrap *>(to_wrap);

			return ret;
		}

		static Userdata<T>
		create_unowned(struct lua_State *L, T *t)
		{
			Userdata<T> ret;
			ret.wrap_ = static_cast<TWrap *>(lua_newuserdata(L, sizeof(*(ret.wrap_))));
			ret.wrap_->t_ = t;
			ret.wrap_->owned_ = false;
			return ret;
		}

		template <typename... Args>
		static Userdata<T>
		create(struct lua_State *L, Args... args)
		{
			Userdata<T> ret;
			ret.wrap_ = static_cast<TWrap *>(lua_newuserdata(L, sizeof(*(ret.wrap_))));
			ret.wrap_->t_ = new T(args...);
			ret.wrap_->owned_ = true;

			return ret;
		}
	};

	template <typename T, typename... Args>
	Userdata<T>
	create_userdata(Args... args)
	{
		return Userdata<T>::create(L(), args...);
	}

	template <typename T>
	Userdata<T>
	create_unowned_userdata(T *t)
	{
		return Userdata<T>::create_unowned(L(), t);
	}

	template <typename T>
	Userdata<T>
	get_userdata(int idx = -1)
	{
		return Userdata<T>::get_from_stack(L(), idx);
	}

	void
	new_table()
	{
		lua_newtable(L());
	}

	void
	new_metatable(const std::string &str)
	{
		luaL_newmetatable(L(), str.c_str());
	}

	void
	get_metatable(const std::string &str)
	{
		luaL_getmetatable(L(), (str.c_str()));
	}

	void
	set_metatable(int idx = -1)
	{
		lua_setmetatable(L(), idx);
	}

	void
	push_closure(lua_CFunction func, int upvalue_count)
	{
		lua_pushcclosure(L(), func, upvalue_count);
	}

	void
	set_table_field(const std::string &name, int table_idx)
	{
		lua_setfield(L(), table_idx, name.c_str());
	}

	void
	register_lib(const struct luaL_Reg *registry, int shared_upvalue_count = 0)
	{
		luaL_setfuncs(L(), registry, shared_upvalue_count);
	}

	void
	push_light_userdata(void *light_userdata)
	{
		lua_pushlightuserdata(L(), light_userdata);
	}
};

}; // namespace lua

}; // namespace orbycomp
