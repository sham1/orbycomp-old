/*
 * intersection.cc
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <orbycomp/region.hh>

using Rect = Region::Rect;

SCENARIO("Regions may intersect with each other", "[intersect]")
{
	GIVEN("A region with rectangle from (0, 0) to (10, 10)")
	{
		Region region(Rect{0, 0, 10, 10});

		WHEN("an intersection is taken with the region itself")
		{
			Region intersection = region.intersect(region);

			THEN("the intersection is equal to the original") { REQUIRE(intersection == region); }
		}

		AND_WHEN("a region with rectangle from (10, 0) to (20, 10) exists")
		{
			Region region2(Rect{10, 0, 20, 10});

			THEN("they will not intersect") { REQUIRE_FALSE(region.intersects_with(region2)); }

			AND_THEN("their intersection region is empty")
			{
				REQUIRE(region.intersect(region2) == Region{});
			}

			AND_THEN("the result is symmetric")
			{
				REQUIRE_FALSE(region2.intersects_with(region));
				REQUIRE(region2.intersect(region) == Region{});
			}
		}

		AND_WHEN("a region with rectangle from (5, 5) to (15, 15) exists")
		{
			Region region2(Rect{5, 5, 15, 15});

			THEN("they will intersect") { REQUIRE(region.intersects_with(region2)); }

			AND_THEN("their intersection has one rectangle from "
				 "(5, 5) to (10, 10)")
			{
				Region intersection = region.intersect(region2);
				REQUIRE(intersection.size() == 1);
				REQUIRE(intersection[0] == Rect{5, 5, 10, 10});
			}

			AND_THEN("the result is symmetric")
			{
				Region intersection = region2.intersect(region);
				REQUIRE(intersection.size() == 1);
				REQUIRE(intersection[0] == Rect{5, 5, 10, 10});
			}
		}
	}

	AND_GIVEN("A region with rectangles (0, 0)-(3, 5) and (5, 0)-(8, 5)")
	{
		Region region{{0, 0, 3, 5}, {5, 0, 8, 5}};

		WHEN("a region with rectangle (2, 3)-(7, 8) exists")
		{
			Region region2(Rect{2, 3, 7, 8});

			THEN("the regions will intersect") { REQUIRE(region.intersects_with(region2)); }

			AND_THEN("the intersection rectangles are"
				 "(2, 3)-(3, 5) and (5, 3)-(7, 5)")
			{
				Region intersection = region.intersect(region2);
				REQUIRE(intersection.size() == 2);
				REQUIRE(intersection[0] == Rect{2, 3, 3, 5});
				REQUIRE(intersection[1] == Rect{5, 3, 7, 5});
			}

			AND_THEN("the result is symmetric")
			{
				Region intersection = region2.intersect(region);
				REQUIRE(intersection.size() == 2);
				REQUIRE(intersection[0] == Rect{2, 3, 3, 5});
				REQUIRE(intersection[1] == Rect{5, 3, 7, 5});
			}
		}
	}
}
