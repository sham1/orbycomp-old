/*
 * union.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <orbycomp/region.hh>

using Rect = Region::Rect;

SCENARIO("Regions and rectangles can be added to regions", "[union]")
{
	GIVEN("An empty region")
	{
		Region region;

		REQUIRE(region.size() == 0);

		WHEN("a rectangle is added")
		{
			Rect rect = Rect::from_xy_width_height(0, 0, 10, 10);
			region += rect;

			THEN("the size changes") { REQUIRE(region.size() == 1); }

			AND_THEN("the first element is the rectangle") { REQUIRE((*region.begin()) == rect); }

			AND_THEN("the bounding box equals the rectangle")
			{
				REQUIRE(region.get_bounds() == rect);
			}
		}
	}

	AND_GIVEN("A region with a rectangle of corners (0,0) and (10,10)")
	{
		Region region({0, 0, 10, 10});
		REQUIRE(region.size() == 1);
		REQUIRE(*(region.begin()) == Rect{0, 0, 10, 10});
		REQUIRE(region.get_bounds() == Rect{0, 0, 10, 10});

		WHEN("a rectangle with corners (5, 5) and (15, 7) is added")
		{
			Rect rect{5, 5, 15, 7};

			region += rect;

			THEN("the amount of rectangles is three") { REQUIRE(region.size() == 3); }

			AND_THEN("the bounding box is from (0, 0) to (15,10)")
			{
				REQUIRE(region.get_bounds() == Rect{0, 0, 15, 10});
			}

			AND_THEN("the first rectangle is from (0, 0) to (10, 5)")
			{
				REQUIRE(region[0] == Rect{0, 0, 10, 5});
			}

			AND_THEN("the second rectangle is from (0, 5) to (15, 7)")
			{
				REQUIRE(region[1] == Rect{0, 5, 15, 7});
			}

			AND_THEN("the third rectangle is from (0, 7) to (10, 10)")
			{
				REQUIRE(region[2] == Rect{0, 7, 10, 10});
			}
		}
	}

	AND_GIVEN("Two regions where first has a rectangle from (0, 0) to (10, 10) & "
		  "the second region has a rectangle from (15, 5) to (25, 15)")
	{
		Rect rect1{0, 0, 10, 10};
		Rect rect2{15, 5, 25, 15};

		Region region1(rect1);
		Region region2(rect2);

		WHEN("they are added together")
		{
			Region region = region1 + region2;

			THEN("there are still only two rectangles") { REQUIRE(region.size() == 2); }

			AND_THEN("the first rectangle in the region is the "
				 "first one")
			{
				REQUIRE(region[0] == rect1);
			}

			AND_THEN("the second rectangle in the region is the "
				 "second one")
			{
				REQUIRE(region[1] == rect2);
			}

			AND_THEN("the bounding box is from (0, 0) to (25, 15)")
			{
				REQUIRE(region.get_bounds() == Rect{0, 0, 25, 15});
			}
		}
	}

	AND_GIVEN("A region with two rectangles,"
		  "the first being from (0, 0) to (10, 10) & "
		  "the second being from (0, 10) to (10, 20)")
	{
		Region region;
		region += Rect{0, 0, 10, 10};
		region += Rect{0, 10, 10, 20};

		WHEN("a rectangle from (5, 5) to (15, 15) is added")
		{
			region += Rect{5, 5, 15, 15};

			THEN("the region has four rectangles") { REQUIRE(region.size() == 4); }

			AND_THEN("the first rectangle is from (0, 0) to (10, 5)")
			{
				REQUIRE(region[0] == Rect{0, 0, 10, 5});
			}

			AND_THEN("the second rectangle is from (0, 5) to (15, 10)")
			{
				REQUIRE(region[1] == Rect{0, 5, 15, 10});
			}

			AND_THEN("the third rectangle is from (0, 10) to (15, 15)")
			{
				REQUIRE(region[2] == Rect{0, 10, 15, 15});
			}

			AND_THEN("the fourth rectangle is from (0, 15) to (10, 20)")
			{
				REQUIRE(region[3] == Rect{0, 15, 10, 20});
			}
		}
	}
}
