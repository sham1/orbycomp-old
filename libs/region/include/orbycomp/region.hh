/*
 * region.hh
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <iosfwd>
#include <vector>

/*
 * A class representing a region made out of many
 * non-overlapping axis-aligned rectangles.
 */
class Region
{
public:
	/*
	 * A struct representing a single, axis-aligned rectangle.
	 * Instances must satisfy the following:
	 * `x1` < `x2`, `y1` < `y2`
	 */
	struct Rect {
		std::int32_t x1, y1, x2, y2;

		inline bool
		operator==(const Rect &other) const
		{
			return this->x1 == other.x1 && this->x2 == other.x2 && this->y1 == other.y1 &&
			       this->y2 == other.y2;
		}

		inline bool
		operator!=(const Rect &other) const
		{
			return !this->operator==(other);
		}

		static inline constexpr Rect
		from_xy_width_height(const int32_t x,
				     const int32_t y,
				     const int32_t width,
				     const int32_t height)
		{
			int32_t min_x = std::min(x, x + width);
			int32_t min_y = std::min(y, y + height);
			int32_t max_x = std::max(x, x + width);
			int32_t max_y = std::max(y, y + height);

			return Rect{min_x, min_y, max_x, max_y};
		}
	};

private:
	/*
	 * The "bounding rectangle" of the region
	 * i.e. the largest rectangle that will encompass
	 * all of the rectangles within the region.
	 */
	Rect bounds{0, 0, 0, 0};

	std::vector<Rect> rects{};

	Region(Rect &bounds_, std::vector<Rect> &&rects_) : bounds(bounds_), rects(std::move(rects_)) {}

	friend Region merge_rects(std::vector<Region::Rect> &&rects_);
	friend Region subtract_rects(std::vector<Region::Rect> &&minuend_,
				     const std::vector<Region::Rect> &subtrahend);

	using iterator = std::vector<Rect>::iterator;
	using const_iterator = std::vector<Rect>::const_iterator;

public:
	Region() = default;
	explicit Region(const Rect &rect) : bounds(rect), rects({rect}) {}
	Region(const Region &other) : bounds(other.bounds), rects(other.rects) {}
	Region(std::initializer_list<Rect> rect_list);
	Region(Region &&other) : bounds(other.bounds), rects(std::move(other.rects))
	{
		// Since we're moving the region rectangles from
		// `other` to `this`, it makes sense that the bounding
		// box of `other` also resets.
		other.bounds = {0, 0, 0, 0};
	}
	virtual ~Region() {}

	Region operator+(const Region &other) const;
	Region operator-(const Region &other) const;

	Region operator+(const Rect &rect) const;
	Region operator-(const Rect &rect) const;

	Region &operator+=(const Region &other);
	Region &operator-=(const Region &other);

	Region &operator+=(const Rect &rect);
	Region &operator-=(const Rect &rect);

	Region &operator=(const Region &other) = default;

	Region &
	operator=(Region &&other)
	{
		this->bounds = other.bounds;
		this->rects = std::move(other.rects);

		// Since we're moving the region rectangles from
		// `other` to `this`, it makes sense that the bounding
		// box of `other` also resets.
		other.bounds = {0, 0, 0, 0};

		return *this;
	}

	bool intersects_with(const Region &other) const;

	Region intersect(const Region &other) const;

	Region &translate(int32_t delta_x, int32_t delta_y);
	Region translated(int32_t delta_x, int32_t delta_y) const;

	inline size_t
	size() const
	{
		return this->rects.size();
	}

	inline const Rect &
	get_bounds() const
	{
		return this->bounds;
	}

	iterator
	begin()
	{
		return this->rects.begin();
	}

	iterator
	end()
	{
		return this->rects.end();
	}

	const_iterator
	begin() const
	{
		return this->rects.begin();
	}

	const_iterator
	end() const
	{
		return this->rects.end();
	}

	const_iterator
	cbegin() const
	{
		return this->rects.begin();
	}

	const_iterator
	cend() const
	{
		return this->rects.end();
	}

	inline const Rect &
	operator[](size_t idx) const
	{
		return this->rects[idx];
	}

	inline Rect &
	operator[](size_t idx)
	{
		return this->rects[idx];
	}

	bool operator==(const Region &rhs) const;

	inline bool
	operator!=(const Region &rhs) const
	{
		return !(this->operator==(rhs));
	}

	bool contains_point(int32_t x, int32_t y) const;
};

std::ostream &operator<<(std::ostream &stream, const Region::Rect &rect);
std::ostream &operator<<(std::ostream &stream, const Region &region);
