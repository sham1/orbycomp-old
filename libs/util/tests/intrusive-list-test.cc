/*
 * intrusive-list-test.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <catch2/catch.hpp>

#include <orbycomp/util/intrusive-list.hh>

using namespace orbycomp::util;

class TestNode : public IntrusiveListNode<TestNode>
{
public:
	int foo_{0};

	TestNode() = default;
	~TestNode() = default;

	TestNode(const TestNode &other) = default;
	TestNode(TestNode &&other) = default;

	TestNode &operator=(const TestNode &other) = default;
	TestNode &operator=(TestNode &&other) = default;

	TestNode(int foo) : foo_{foo} {}

	bool
	operator==(const TestNode &other) const
	{
		return foo_ == other.foo_;
	}

	bool
	operator!=(const TestNode &other) const
	{
		return foo_ != other.foo_;
	}
};

SCENARIO("Intrusive list operations", "[intrusive test]")
{

	GIVEN("An empty list")
	{
		IntrusiveList<TestNode> list{};

		WHEN("taking its begin and end iterators")
		{
			auto begin = list.begin();
			auto end = list.end();

			THEN("the iterators are equal") { REQUIRE(begin == end); }

			AND_THEN("as are the backwards iterator")
			{
				auto rbegin = list.rbegin();
				auto rend = list.rend();
				REQUIRE(rbegin == rend);
			}
		}
	}

	AND_GIVEN("A list with one item")
	{
		IntrusiveList<TestNode> list{};
		TestNode node{5};
		list.insert_back(node);

		WHEN("taking its begin and end iterators")
		{
			auto begin = list.begin();
			auto end = list.end();

			THEN("the iterators are not equal") { REQUIRE(begin != end); }
			AND_THEN("the value pointed to begin is the element") { REQUIRE(*begin == node); }
			AND_THEN("the pointer at begin is same as the element's")
			{
				REQUIRE(&*begin == &node);
			}
		}
	}

	AND_GIVEN("A list with one item")
	{
		IntrusiveList<TestNode> list{};
		TestNode node{5};
		list.insert_back(node);

		WHEN("another item is appended")
		{
			TestNode node2{6};
			list.insert_back(node2);

			THEN("iterated forwards values are 5 and 6")
			{
				auto it = list.begin();
				REQUIRE(*it++ == node);
				REQUIRE(*it++ == node2);
			}
		}

		AND_WHEN("the item is removed by RAII")
		{
			THEN("the list is as it was before appending")
			{
				auto it = list.begin();
				REQUIRE(*it++ == node);
				REQUIRE(it == list.end());
			}

			AND_THEN("and is also same backwards")
			{
				auto it = list.rbegin();
				REQUIRE(*it++ == node);
				REQUIRE(it == list.rend());
			}
		}
	}

	AND_GIVEN("A list with one item")
	{
		IntrusiveList<TestNode> list{};
		TestNode node{5};
		list.insert_back(node);

		WHEN("another item is prepended")
		{
			TestNode node2{6};
			list.insert_front(node2);

			THEN("iterated forwards values are 6 and 5")
			{
				auto it = list.begin();
				REQUIRE(*it++ == node2);
				REQUIRE(*it++ == node);
			}

			AND_THEN("iterated backwards values are 5 and 6")
			{
				auto it = list.rbegin();
				REQUIRE(*it++ == node);
				REQUIRE(*it++ == node2);
			}
		}
	}
}
