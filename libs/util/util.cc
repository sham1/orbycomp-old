/*
 * util.cc
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/util/format-string.hh>
#include <orbycomp/util/idle-event-queue.hh>
#include <orbycomp/util/intrusive-list.hh>
#include <orbycomp/util/time.hh>

#include <errno.h>
#include <time.h>

namespace orbycomp
{

namespace util
{

namespace impl
{

// idle-event-queue.hh
EventCallbackBase::~EventCallbackBase()
{
	if (token) {
		token->cb_base = nullptr;
		token = nullptr;
	}
}

} // namespace impl

// time.hh
std::int64_t
get_monotonic_time()
{
	struct timespec ts;

	int res = clock_gettime(CLOCK_MONOTONIC, &ts);
	if (res < 0) {
		throw std::system_error(errno, std::generic_category(),
					"get_monotonic_time requires working CLOCK_MONOTONIC");
	}

	return (static_cast<std::int64_t>(ts.tv_sec) * 1000000) + (ts.tv_nsec / 1000);
}

} // namespace util

} // namespace orbycomp
