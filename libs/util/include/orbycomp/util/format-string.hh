/*
 * format-string.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <cstdio>
#include <memory>
#include <string>
#include <system_error>

namespace orbycomp
{

namespace util
{

template <typename... Args>
std::string
format_str(const std::string &fmt, Args... args)
{
	int buf_len = std::snprintf(nullptr, 0, fmt.c_str(), args...);
	if (buf_len < 0) {
		throw std::system_error(errno, std::generic_category());
	}

	std::unique_ptr<char[]> buf{new char[buf_len + 1]};
	std::snprintf(buf.get(), buf_len + 1, fmt.c_str(), args...);
	return std::string{buf.get()};
}

}; // namespace util
}; // namespace orbycomp
