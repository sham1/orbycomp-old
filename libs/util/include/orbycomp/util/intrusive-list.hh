/*
 * intrusive-list.hh
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <iterator>
#include <type_traits>

namespace orbycomp
{

namespace util
{

template <class T, class M>
static inline constexpr ptrdiff_t
offset_of(const M T::*member)
{
	return reinterpret_cast<ptrdiff_t>(&(reinterpret_cast<T *>(0)->*member));
}

template <class T, class M>
static inline constexpr T *
container_of(M *ptr, const M T::*member)
{
	return reinterpret_cast<T *>(reinterpret_cast<intptr_t>(ptr) - offset_of(member));
}

template <class T, class M>
static inline constexpr const T *
container_of(const M *ptr, const M T::*member)
{
	return reinterpret_cast<T *>(reinterpret_cast<intptr_t>(ptr) - offset_of(member));
}

template <typename T> class IntrusiveList;

template <typename T> class IntrusiveListNode
{
public:
	typename IntrusiveList<T>::Node node_{};

	IntrusiveListNode() = default;
	~IntrusiveListNode() = default;

	IntrusiveListNode(const IntrusiveListNode &){};
	IntrusiveListNode(IntrusiveListNode &&other)
	{
		node_.prev_ = other.node_.prev_;
		node_.next_ = other.node_.next_;

		node_.prev_->next_ = &node_;
		node_.next_->prev_ = &node_;

		other.node_.prev_ = other.node_.next_ = nullptr;
	}

	IntrusiveListNode &operator=(const IntrusiveListNode &){};
	IntrusiveListNode &
	operator=(IntrusiveListNode &&other)
	{
		node_.prev_ = other.node_.prev_;
		node_.next_ = other.node_.next_;

		node_.prev_->next_ = &node_;
		node_.next_->prev_ = &node_;

		other.node_.prev_ = other.node_.next_ = nullptr;
	}
};

template <typename T> class IntrusiveList
{
public:
	class Node
	{
		friend IntrusiveList<T>;
		friend IntrusiveListNode<T>;

		Node *next_{nullptr};
		Node *prev_{nullptr};

		~Node()
		{
			Node *tmp = next_;
			if (prev_) {
				prev_->next_ = tmp;
			}

			if (next_) {
				tmp->prev_ = prev_;
			}
		}

		Node() : next_{nullptr}, prev_{nullptr} {}
		Node(Node *next, Node *prev) : next_{next}, prev_{prev} {}

		Node(const Node &) {}
	};

	void
	insert_front(IntrusiveListNode<T> &node)
	{
		node.node_.prev_ = &root_;
		node.node_.next_ = root_.next_;

		root_.next_ = &node.node_;
		node.node_.next_->prev_ = &node.node_;
	}

	void
	insert_back(IntrusiveListNode<T> &node)
	{
		node.node_.next_ = &root_;
		node.node_.prev_ = root_.prev_;

		root_.prev_ = &node.node_;
		node.node_.prev_->next_ = &node.node_;
	}

	class Iterator
	{
		friend IntrusiveList<T>;

		const Node *root_{nullptr};
		Node *current_node_{nullptr};

		Iterator(const Node *root, Node *node) : root_{root}, current_node_{node} {}

	public:
		Iterator() = default;
		~Iterator() = default;

		Iterator(Iterator &&other) = default;
		Iterator(const Iterator &other) = default;

		Iterator &operator=(Iterator &&other) = default;
		Iterator &operator=(const Iterator &other) = default;

		typedef std::bidirectional_iterator_tag iterator_category;
		typedef T value_type;
		typedef value_type &reference;
		typedef T *pointer;
		typedef typename std::pointer_traits<pointer>::difference_type difference_type;

		bool
		operator==(const Iterator &other) const
		{
			return root_ == other.root_ && current_node_ == other.current_node_;
		}

		bool
		operator!=(const Iterator &other) const
		{
			return !this->operator==(other);
		}

		reference
		operator*()
		{
			IntrusiveListNode<T> *node =
			    container_of(current_node_, &IntrusiveListNode<T>::node_);
			T *ret = static_cast<T *>(node);
			return *ret;
		}

		pointer
		operator->()
		{
			IntrusiveListNode<T> *node = container_of(current_node_, IntrusiveListNode<T>::node_);
			T *ret = static_cast<T *>(node);
			return ret;
		}

		Iterator &
		operator++()
		{
			current_node_ = current_node_->next_;
			return *this;
		}

		Iterator
		operator++(int)
		{
			Iterator ret{root_, current_node_};
			current_node_ = current_node_->next_;
			return ret;
		}

		Iterator &
		operator--()
		{
			current_node_ = current_node_->prev_;
			return *this;
		}

		Iterator
		operator--(int)
		{
			Iterator ret{root_, current_node_};
			current_node_ = current_node_->prev_;
			return ret;
		}
	};

	class ConstIterator
	{
		friend IntrusiveList<T>;

		const Node *root_{nullptr};
		const Node *current_node_{nullptr};

		ConstIterator(const Node *root, const Node *node) : root_{root}, current_node_{node} {}

	public:
		ConstIterator() = default;
		~ConstIterator() = default;

		ConstIterator(ConstIterator &&other) = default;
		ConstIterator(const ConstIterator &other) = default;

		ConstIterator &operator=(ConstIterator &&other) = default;
		ConstIterator &operator=(const ConstIterator &other) = default;

		typedef std::bidirectional_iterator_tag iterator_category;
		typedef T value_type;
		typedef const value_type &reference;
		typedef const T *pointer;
		typedef typename std::pointer_traits<pointer>::difference_type difference_type;

		bool
		operator==(const ConstIterator &other) const
		{
			return root_ == other.root_ && current_node_ == other.current_node_;
		}

		bool
		operator!=(const ConstIterator &other) const
		{
			return !this->operator==(other);
		}

		reference
		operator*()
		{
			const IntrusiveListNode<T> *node =
			    container_of(current_node_, &IntrusiveListNode<T>::node_);
			const T *ret = static_cast<const T *>(node);
			return *ret;
		}

		pointer
		operator->()
		{
			const IntrusiveListNode<T> *node =
			    container_of(current_node_, &IntrusiveListNode<T>::node_);
			const T *ret = static_cast<const T *>(node);
			return ret;
		}

		ConstIterator &
		operator++()
		{
			current_node_ = current_node_->next_;
			return *this;
		}

		ConstIterator
		operator++(int)
		{
			ConstIterator ret{root_, current_node_};
			current_node_ = current_node_->next_;
			return ret;
		}

		ConstIterator &
		operator--()
		{
			current_node_ = current_node_->prev_;
			return *this;
		}

		ConstIterator
		operator--(int)
		{
			ConstIterator ret{root_, current_node_};
			current_node_ = current_node_->prev_;
			return ret;
		}
	};

	static_assert(std::is_move_constructible<Iterator>::value);
	static_assert(std::is_move_assignable<Iterator>::value);
	static_assert(std::is_copy_constructible<Iterator>::value);
	static_assert(std::is_copy_assignable<Iterator>::value);
	static_assert(std::is_destructible<Iterator>::value);
	static_assert(std::is_swappable<Iterator>::value);
	static_assert(std::is_default_constructible<Iterator>::value);

	static_assert(std::is_move_constructible<ConstIterator>::value);
	static_assert(std::is_move_assignable<ConstIterator>::value);
	static_assert(std::is_copy_constructible<ConstIterator>::value);
	static_assert(std::is_copy_assignable<ConstIterator>::value);
	static_assert(std::is_destructible<ConstIterator>::value);
	static_assert(std::is_swappable<ConstIterator>::value);
	static_assert(std::is_default_constructible<ConstIterator>::value);

	using reverse_iterator = typename std::reverse_iterator<Iterator>;
	using reverse_const_iterator = typename std::reverse_iterator<ConstIterator>;

	inline Iterator
	begin()
	{
		return Iterator(&root_, root_.next_);
	}

	inline Iterator
	end()
	{
		return Iterator(&root_, &root_);
	}

	inline reverse_iterator
	rbegin()
	{
		return reverse_iterator{end()};
	}

	inline reverse_iterator
	rend()
	{
		return reverse_iterator{begin()};
	}

	inline ConstIterator
	begin() const
	{
		return ConstIterator(&root_, root_.next_);
	}

	inline ConstIterator
	end() const
	{
		return ConstIterator(&root_, &root_);
	}

	inline reverse_const_iterator
	rbegin() const
	{
		return reverse_iterator{end()};
	}

	inline reverse_const_iterator
	rend() const
	{
		return reverse_const_iterator{begin()};
	}

	inline ConstIterator
	cbegin()
	{
		return ConstIterator(&root_, root_.next_);
	}

	inline ConstIterator
	cend()
	{
		return ConstIterator(&root_, &root_);
	}

	inline reverse_const_iterator
	crbegin()
	{
		return reverse_const_iterator{end()};
	}

	inline reverse_const_iterator
	crend()
	{
		return reverse_const_iterator{begin()};
	}

	IntrusiveList()
	{
		static_assert(std::is_base_of<IntrusiveListNode<T>, T>::value,
			      "Types in the IntrusiveList must derive from "
			      "IntrusiveListNode");
	}

	~IntrusiveList()
	{
		Node *current = root_.next_;
		while (current && current != &root_) {
			Node *tmp = current->next_;
			current->prev_ = nullptr;
			current->next_ = nullptr;
			current = tmp;
		}
	}

private:
	Node root_{&root_, &root_};
};

} // namespace util

} // namespace orbycomp
