/*
 * main.cc
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <ev.h>
#include <functional>
#include <memory>
#include <orbycomp/base/compositor.hh>
#include <stdexcept>
#include <wayland-server-core.h>

int
main(int argc, char **argv)
{
	std::unique_ptr<struct wl_display, decltype(&wl_display_destroy)> display(wl_display_create(),
										  wl_display_destroy);

	std::unique_ptr<struct ev_loop, decltype(&ev_loop_destroy)> loop(ev_loop_new(0), ev_loop_destroy);

	try {
		orbycomp::Compositor compositor(std::move(display), std::move(loop));
		compositor.run();
	} catch (const std::runtime_error &exception) {
		std::fprintf(stderr, "Compositor exited with an error: %s\n", exception.what());
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
