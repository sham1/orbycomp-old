/*
 * ev-dbus.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <new>
#include <orbycomp/ev-dbus.hh>
#include <sstream>
#include <stdexcept>

namespace orbycomp
{

namespace dbus
{

void
Context::dispatch_dbus_cb(EV_P_ struct ev_async *w, int revents)
{
	Context *self = static_cast<Context *>(w->data);

	while (dbus_connection_get_dispatch_status(self->connection_.get()) == DBUS_DISPATCH_DATA_REMAINS) {
		dbus_connection_dispatch(self->connection_.get());
	}
}

void
Context::wakeup_main_loop_cb(void *user_data)
{
	Context *self = static_cast<Context *>(user_data);

	ev_async_send(self->loop_, &self->dispatch_dbus);
}

Context::Context(struct ev_loop *&loop, Connection &&connection)
    : loop_{loop}, connection_{std::move(connection)}, dispatch_dbus{}
{
	ev_async_init(&dispatch_dbus, Context::dispatch_dbus_cb);
	this->dispatch_dbus.data = this;
	ev_async_start(loop_, &dispatch_dbus);

	dbus_connection_set_wakeup_main_function(connection_.get(), Context::wakeup_main_loop_cb, this,
						 nullptr);

	if (!dbus_connection_set_watch_functions(connection_.get(), Watcher::add_watch_cb,
						 Watcher::remove_watch_cb, Watcher::toggle_watch_cb, this,
						 nullptr)) {
		throw std::bad_alloc();
	}
}

Context
Context::get_from_bus(struct ev_loop *loop, DBusBusType bus)
{
	// Let's not let DBus manage our SIGPIPEs!
	dbus_connection_set_change_sigpipe(FALSE);

	// Now we shall get a connection to the specified bus!
	// We shall also catch any error that we may get.
	Error error;

	Connection c = Connection(dbus_bus_get_private(bus, &error), dbus_connection_unref);
	if (c == nullptr) {
		std::stringstream ss{};
		ss << "Couldn't initialize D-Bus: ";
		ss << error.message();
		throw std::runtime_error(ss.str());
	}

	// We shouldn't exit just because the DBus connection dies...
	dbus_connection_set_exit_on_disconnect(c.get(), FALSE);

	return {loop, std::move(c)};
}

Context::~Context() { dbus_connection_close(connection_.get()); }

DBusConnection *
Context::get_connection()
{
	return connection_.get();
}

dbus_bool_t
Context::Watcher::add_watch_cb(DBusWatch *watch, void *user_data)
{
	Context *self = static_cast<Context *>(user_data);
	try {
		Watcher *watcher = new Watcher(*self, watch);
		dbus_watch_set_data(watch, watcher, Watcher::watcher_fini);

		return TRUE;
	} catch (const std::bad_alloc &) {
		return FALSE;
	}
}

Context::Watcher::Watcher(Context &context, DBusWatch *watch) : context_{context}, watch_{watch}
{
	if (dbus_watch_get_enabled(watch_)) {
		int fd = dbus_watch_get_unix_fd(watch_);
		unsigned int flags = dbus_watch_get_flags(watch_);

		int events = 0;
		if (flags & DBUS_WATCH_READABLE) {
			events |= EV_READ;
		}

		if (flags & DBUS_WATCH_WRITABLE) {
			events |= EV_WRITE;
		}

		if (flags & DBUS_WATCH_ERROR) {
			events |= EV_ERROR;
		}

		ev_io_init(&watcher, Watcher::on_watch_cb, fd, events);
		watcher.data = this;
		ev_io_start(context_.loop_, &watcher);
	}
}

void
Context::Watcher::remove_watch_cb(DBusWatch *watch, void *user_data)
{
	Watcher *self = static_cast<Watcher *>(dbus_watch_get_data(watch));
	if (self == nullptr) {
		return;
	}

	if (ev_is_active(&self->watcher)) {
		ev_io_stop(self->context_.loop_, &self->watcher);
	}
}

void
Context::Watcher::toggle_watch_cb(DBusWatch *watch, void *user_data)
{
	Watcher *self = static_cast<Watcher *>(dbus_watch_get_data(watch));
	if (self == nullptr) {
		return;
	}

	if (dbus_watch_get_enabled(watch)) {
		if (!ev_is_active(&self->watcher)) {
			ev_io_start(self->context_.loop_, &self->watcher);
		}
	} else {
		if (ev_is_active(&self->watcher)) {
			ev_io_stop(self->context_.loop_, &self->watcher);
		}
	}
}

void
Context::Watcher::watcher_fini(void *user_data)
{
	Watcher *self = static_cast<Watcher *>(user_data);

	delete self;
}

Context::Watcher::~Watcher()
{
	if (ev_is_active(&watcher)) {
		ev_io_stop(context_.loop_, &watcher);
	}
}

void
Context::Watcher::on_watch_cb(EV_P_ struct ev_io *w, int revents)
{
	Watcher *self = static_cast<Watcher *>(w->data);

	unsigned int flags = 0;
	if (revents & EV_READ) {
		flags |= DBUS_WATCH_READABLE;
	}

	if (revents & EV_WRITE) {
		flags |= DBUS_WATCH_WRITABLE;
	}

	if (revents & EV_ERROR) {
		flags |= DBUS_WATCH_ERROR;
	}

	dbus_watch_handle(self->watch_, flags);
}

dbus_bool_t
Context::Timeout::add_timeout_cb(DBusTimeout *timeout, void *user_data)
{
	Context *self = static_cast<Context *>(user_data);
	try {
		Timeout *timer = new Timeout(*self, timeout);
		dbus_timeout_set_data(timeout, timer, Timeout::timeout_fini);

		return TRUE;
	} catch (const std::bad_alloc &) {
		return FALSE;
	}
}

Context::Timeout::Timeout(Context &context, DBusTimeout *timeout) : context_{context}, timeout_{timeout}
{
	ev_timer_init(&timer, Timeout::on_timer_cb, 0, 0);
	timer.data = this;

	adjust_timer();
}

void
Context::Timeout::on_timer_cb(EV_P_ struct ev_timer *w, int revents)
{
	Timeout *self = static_cast<Timeout *>(w->data);
	if (dbus_timeout_get_enabled(self->timeout_)) {
		dbus_timeout_handle(self->timeout_);
	}
}

void
Context::Timeout::remove_timeout_cb(DBusTimeout *timeout, void *user_data)
{
	Timeout *self = static_cast<Timeout *>(dbus_timeout_get_data(timeout));

	if (self == nullptr) {
		return;
	}

	if (ev_is_active(&self->timer)) {
		ev_timer_stop(self->context_.loop_, &self->timer);
	}
}

void
Context::Timeout::toggle_timeout_cb(DBusTimeout *timeout, void *user_data)
{
	Timeout *self = static_cast<Timeout *>(dbus_timeout_get_data(timeout));

	if (self == nullptr) {
		return;
	}

	self->adjust_timer();
}

void
Context::Timeout::timeout_fini(void *user_data)
{
	Timeout *self = static_cast<Timeout *>(user_data);

	delete self;
}

void
Context::Timeout::adjust_timer()
{
	if (ev_is_active(&timer)) {
		ev_timer_stop(context_.loop_, &timer);
	}

	if (dbus_timeout_get_enabled(timeout_)) {
		int64_t timeout_time = dbus_timeout_get_interval(timeout_);
		ev_tstamp timeout_count = (ev_tstamp) timeout_time / 1000;

		ev_timer_set(&timer, timeout_count + ev_now(context_.loop_) - ev_time(), 0.);
		ev_timer_start(context_.loop_, &timer);
	}
}

Context::Timeout::~Timeout()
{
	if (ev_is_active(&timer)) {
		ev_timer_stop(context_.loop_, &timer);
	}
}

Context::SignalMatcher
Context::add_signal_matcher(const std::string &sender,
			    const std::string &iface,
			    const std::string &member,
			    const std::string &path)
{
	return SignalMatcher(*this, sender, iface, member, path);
}

Context::SignalMatcher::SignalMatcher(Context &ctx,
				      const std::string &sender,
				      const std::string &iface,
				      const std::string &member,
				      const std::string &path)
    : active{true}, context{&ctx}, matcher_str{}
{
	std::stringstream ss;
	ss << "type='signal',";
	ss << "sender='" << sender << "',";
	ss << "interface='" << iface << "',";
	ss << "member='" << member << "',";
	ss << "path='" << path << "'";
	matcher_str = ss.str();

	Error error;

	dbus_bus_add_match(context->connection_.get(), matcher_str.c_str(), &error);
	if (dbus_error_is_set(&error)) {
		throw std::runtime_error(error.message());
	}
}

Context::SignalMatcher &
Context::SignalMatcher::operator=(SignalMatcher &&other)
{
	context = std::move(other.context);
	matcher_str = std::move(other.matcher_str);

	active = other.active;
	other.active = false;

	return *this;
}

Context::SignalMatcher::~SignalMatcher()
{
	if (active) {
		dbus_bus_remove_match(context->connection_.get(), matcher_str.c_str(), nullptr);
	}
}

} // namespace dbus

} // namespace orbycomp
