/*
 * gles-renderer.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "backend.hh"
#include "renderer.hh"
#include <wayland-egl.h>

namespace orbycomp
{

namespace wl
{

class GLESRenderer : public Renderer
{
	static RendererHandle load_renderer(Backend *backend);

	struct OutputInfo {
		GLESRenderer *renderer_;
		Output *output_;

		std::unique_ptr<struct wl_egl_window, decltype(&wl_egl_window_destroy)> egl_surface_;

		OutputInfo(GLESRenderer *renderer, Output *output);
		~OutputInfo();

		void render();
	};
	std::unordered_map<Output *, std::unique_ptr<OutputInfo>> outputs_;

	static void on_frame_done(void *data, struct wl_callback *wl_callback, uint32_t callback_data);
	static constexpr wl_callback_listener frame_cb_listener = {
	    GLESRenderer::on_frame_done,
	};

public:
	GLESRenderer(Backend *backend);
	~GLESRenderer();

	void setup_output(Output *output) override;
	void render_output(Output *output) override;
};

}; // namespace wl

}; // namespace orbycomp
