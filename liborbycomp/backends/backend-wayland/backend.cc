/*
 * backend.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "backend.hh"

#include <orbycomp/base/compositor.hh>
#include <orbycomp/util/time.hh>

#include <string_view>
#include <system_error>

extern "C" orbycomp::BackendBase *
backend_init(orbycomp::Compositor *compositor)
{
	return new orbycomp::wl::Backend(compositor);
}

namespace orbycomp
{

namespace wl
{

static constexpr int32_t frame_edge_width = 8;
static constexpr int32_t frame_title_height = 32;

Backend::Backend(Compositor *compositor)
    : BackendBase{compositor}, display_{Backend::get_display()}, registry_{get_registry()},
      renderer_{Renderer::choose_renderer(this)}
{
	ev_io_init(&wayland_io_, &Backend::on_wayland_event, wl_display_get_fd(display_.get()),
		   EV_READ | EV_WRITE);
	wayland_io_.data = this;
	ev_io_start(compositor_->loop(), &wayland_io_);

	wl_registry_add_listener(registry_.get(), &Backend::registry_listener, this);

	wl_display_roundtrip(display_.get());

	output_ = std::make_unique<Output>(this);
	renderer_->setup_output(output_.get());

	compositor_->outputs_.emplace_back(output_.get());
	compositor_->on_output_added_signal(output_.get());
}

Backend::~Backend() {}

orbycomp::Renderer *
Backend::get_renderer()
{
	return renderer_->get();
}

void
Backend::repaint_output(orbycomp::Output *output)
{
	renderer_->render_output(output_.get());
}

Backend::DisplayPtr
Backend::get_display()
{
	if (getenv("WAYLAND_DISPLAY") == nullptr) {
		throw std::runtime_error("No WAYLAND_DISPLAY defined in current environment");
	}

	struct wl_display *ret = wl_display_connect(nullptr);
	if (!ret) {
		throw std::system_error(errno, std::generic_category(),
					"Could not connect to Wayland compositor");
	}

	return {ret, wl_display_disconnect};
}

Backend::RegistryPtr
Backend::get_registry()
{
	RegistryPtr ret{wl_display_get_registry(display_.get()), wl_registry_destroy};
	if (!ret) {
		throw std::runtime_error("Could not get Wayland registry");
	}
	return ret;
}

void
Backend::on_registry_global(
    void *user_data, struct wl_registry *registry, uint32_t id, const char *interface, uint32_t version)
{
	Backend *self = static_cast<Backend *>(user_data);
	std::string_view name{interface};

	if (name == wl_compositor_interface.name) {
		self->globals_.compositor_ = static_cast<struct wl_compositor *>(
		    wl_registry_bind(registry, id, &wl_compositor_interface, 4));
	}

	if (name == wl_shm_interface.name) {
		self->globals_.shm_ =
		    static_cast<struct wl_shm *>(wl_registry_bind(registry, id, &wl_shm_interface, 1));
	}

	if (name == xdg_wm_base_interface.name) {
		self->globals_.wm_base_ = static_cast<struct xdg_wm_base *>(
		    wl_registry_bind(registry, id, &xdg_wm_base_interface, std::min(version, uint32_t{3})));
		xdg_wm_base_add_listener(self->globals_.wm_base_.get(), &Backend::wm_base_listener, self);
	}

	if (name == wl_subcompositor_interface.name) {
		self->globals_.subcomp_ = static_cast<struct wl_subcompositor *>(
		    wl_registry_bind(registry, id, &wl_subcompositor_interface, 1));
	}

	if (name == zwp_keyboard_shortcuts_inhibit_manager_v1_interface.name) {
		self->globals_.shortcuts_inhibit_manager_ =
		    static_cast<struct zwp_keyboard_shortcuts_inhibit_manager_v1 *>(wl_registry_bind(
			registry, id, &zwp_keyboard_shortcuts_inhibit_manager_v1_interface, 1));
	}

	if (name == wl_seat_interface.name) {
		self->globals_.seats_.try_emplace(id, std::make_unique<Seat>(self, id, version));
	}
}

void
Backend::on_registry_global_remove(void *user_data, struct wl_registry *registry, uint32_t id)
{
	Backend *self = static_cast<Backend *>(user_data);

	self->globals_.seats_.erase(id);
}

void
Backend::wm_base_ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial)
{
	xdg_wm_base_pong(xdg_wm_base, serial);
}

void
Backend::on_wayland_event(EV_P, struct ev_io *w, int revents)
{
	Backend *self = static_cast<Backend *>(w->data);
	if (revents & EV_READ) {
		wl_display_dispatch(self->display_.get());
	}
	if (revents & EV_WRITE) {
		wl_display_flush(self->display_.get());
	}
}

Output::Output(Backend *backend)
    : orbycomp::Output{backend->compositor(),
		       0,
		       0,
		       std::vector<orbycomp::Output::Mode>{Mode{800, 600, 60000, WL_OUTPUT_MODE_CURRENT}},
		       0,
		       0,
		       WL_OUTPUT_SUBPIXEL_UNKNOWN,
		       1,
		       WL_OUTPUT_TRANSFORM_NORMAL,
		       "Wayland",
		       "Virtual"},
      backend_{backend}, frame_{create_frame()}, output_surface_{wl_compositor_create_surface(
						     backend_->globals_.compositor_.get())},
      output_subsurf_{wl_subcompositor_get_subsurface(
	  backend_->globals_.subcomp_.get(), output_surface_.get(), frame_.surface_.get())},
      shortcuts_inhibitor_{get_inhibitor()}
{
	{
		auto accessor{frame_.pool_.access_current_buffer()};
		std::fill(accessor.begin(), accessor.end(), 0x80);
	}

	wl_subsurface_set_desync(output_subsurf_.get());
	wl_subsurface_set_position(output_subsurf_.get(), frame_edge_width, frame_title_height);
}

Output::Frame
Output::create_frame()
{
	Frame ret{};

	ret.output_ = this;

	ret.surface_ = wl_compositor_create_surface(backend_->globals_.compositor_.get());

	ret.pool_ = SHMPool(backend_->globals_.shm_.get(), 800 + 2 * frame_edge_width,
			    600 + frame_edge_width + frame_title_height, WL_SHM_FORMAT_XRGB8888);

	ret.shell_surface_ =
	    xdg_wm_base_get_xdg_surface(backend_->globals_.wm_base_.get(), ret.surface_.get());
	xdg_surface_add_listener(ret.shell_surface_.get(), &Output::shell_surface_listener, this);

	ret.toplevel_ = xdg_surface_get_toplevel(ret.shell_surface_.get());
	xdg_toplevel_add_listener(ret.toplevel_.get(), &Output::toplevel_listener, this);

	wl_surface_commit(ret.surface_.get());

	xdg_toplevel_set_title(ret.toplevel_.get(), "Orbycomp Wayland Output");
	xdg_toplevel_set_app_id(ret.toplevel_.get(), "xyz.sham1.Orbycomp");

	return ret;
}

Proxy<struct zwp_keyboard_shortcuts_inhibitor_v1, zwp_keyboard_shortcuts_inhibitor_v1_destroy>
Output::get_inhibitor()
{
	Proxy<struct zwp_keyboard_shortcuts_inhibitor_v1, zwp_keyboard_shortcuts_inhibitor_v1_destroy> ret{};

	if (backend_->globals_.shortcuts_inhibit_manager_.get()) {
		ret = zwp_keyboard_shortcuts_inhibit_manager_v1_inhibit_shortcuts(
		    backend_->globals_.shortcuts_inhibit_manager_.get(), frame_.surface_.get(),
		    backend_->globals_.seats_.begin()->second->seat_.get());
	}

	return ret;
}

void
Output::frame_configure(void *data, struct xdg_surface *surface, uint32_t serial)
{
	Output *self = static_cast<Output *>(data);

	xdg_surface_ack_configure(self->frame_.shell_surface_.get(), serial);

	if (!self->frame_.has_been_configured_) {
		struct wl_buffer *buf = self->frame_.pool_.swap_active();
		wl_surface_attach(self->frame_.surface_.get(), buf, 0, 0);
		wl_surface_commit(self->frame_.surface_.get());

		self->frame_.has_been_configured_ = true;

		// Do first render here
		self->backend_->renderer_->render_output(self);
	}
}

void
Output::frame_toplevel_configure(
    void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height, struct wl_array *states)
{
	Output *self = static_cast<Output *>(data);
}

void
Output::frame_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel)
{
	Output *self = static_cast<Output *>(data);

	ev_break(self->backend_->compositor_->loop(), EVBREAK_ALL);
}

Output::~Output() {}

Seat::Seat(Backend *backend, uint32_t id, uint32_t version)
    : backend_{backend}, seat_{static_cast<wl_seat *>(wl_registry_bind(
			     backend_->registry_.get(), id, &wl_seat_interface, std::max(version, 5u)))},
      virtual_seat_{backend_->compositor_->get_seat("wl-virtual")}
{
	wl_seat_add_listener(seat_.get(), &Seat::iface, this);
}

void
Seat::apply_capabilities()
{
	auto capabilities{capabilities_apply_.value()};

	if (capabilities & WL_SEAT_CAPABILITY_KEYBOARD) {
		if (!has_keyboard_) {
			virtual_seat_->add_keyboard_capability();
			has_keyboard_ = true;

			if (!keyboard_) {
				keyboard_.emplace(this);
			}
		}
	} else {
		if (has_keyboard_) {
			virtual_seat_->remove_keyboard_capability();
			has_keyboard_ = false;

			if (keyboard_) {
				keyboard_.reset();
			}
		}
	}

	if (capabilities & WL_SEAT_CAPABILITY_POINTER) {
		if (!has_pointer_) {
			virtual_seat_->add_pointer_capability();
			has_pointer_ = true;

			if (!pointer_) {
				pointer_.emplace(this);
			}
		}
	} else {
		if (has_pointer_) {
			virtual_seat_->remove_pointer_capability();
			has_pointer_ = false;

			if (pointer_) {
				pointer_.reset();
			}
		}
	}

	if (capabilities & WL_SEAT_CAPABILITY_TOUCH) {
		if (!has_touch_) {
			virtual_seat_->add_touch_capability();
			has_touch_ = true;
		}
	} else {
		if (has_touch_) {
			virtual_seat_->remove_touch_capability();
			has_touch_ = false;
		}
	}

	capabilities_apply_.reset();
}

void
Seat::capabilities(void *data, struct wl_seat *wl_seat, uint32_t capabilities)
{
	Seat *self = static_cast<Seat *>(data);

	if (!self->capabilities_apply_) {
		self->capabilities_apply_.emplace(static_cast<enum ::wl_seat_capability>(capabilities));
		self->capabilities_apply_event_ = self->backend_->compositor_->add_idle_task([self]() {
			self->apply_capabilities();
		});
	} else {
		self->capabilities_apply_ = static_cast<enum ::wl_seat_capability>(capabilities);
	}
}

void
Seat::name(void *data, struct wl_seat *wl_seat, const char *name)
{
	Seat *self = static_cast<Seat *>(data);

	self->name_ = name;
}

Seat::~Seat() {}

Seat::Keyboard::Keyboard(Seat *seat) : seat_{seat}, keyboard_{wl_seat_get_keyboard(seat_->seat_.get())}
{
	wl_keyboard_add_listener(keyboard_.get(), &Keyboard::iface, this);
}

void
Seat::Keyboard::keymap(
    void *data, struct wl_keyboard *wl_keyboard, uint32_t format, int32_t fd, uint32_t size)
{
	Keyboard *self = static_cast<Keyboard *>(data);
}

void
Seat::Keyboard::enter(void *data,
		      struct wl_keyboard *wl_keyboard,
		      uint32_t serial,
		      struct wl_surface *surface,
		      struct wl_array *keys)
{
}

void
Seat::Keyboard::leave(void *data,
		      struct wl_keyboard *wl_keyboard,
		      uint32_t serial,
		      struct wl_surface *surface)
{
}

void
Seat::Keyboard::key(
    void *data, struct wl_keyboard *wl_keyboard, uint32_t serial, uint32_t time, uint32_t key, uint32_t state)
{
	Keyboard *self = static_cast<Keyboard *>(data);

	orbycomp::Keyboard::KeyState keystate;
	if (state == WL_KEYBOARD_KEY_STATE_PRESSED) {
		keystate = orbycomp::Keyboard::KeyState::Press;
	} else {
		keystate = orbycomp::Keyboard::KeyState::Release;
	}

	self->seat_->virtual_seat_->keyboard()->feed_native_keypress(key, keystate, time);
}

void
Seat::Keyboard::modifiers(void *data,
			  struct wl_keyboard *wl_keyboard,
			  uint32_t serial,
			  uint32_t mods_depressed,
			  uint32_t mods_latched,
			  uint32_t mods_locked,
			  uint32_t group)
{
	Keyboard *self = static_cast<Keyboard *>(data);
	self->seat_->virtual_seat_->keyboard()->feed_modifiers(mods_depressed, mods_latched, mods_locked,
							       group);
}

void
Seat::Keyboard::repeat_info(void *data, struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay)
{
	Keyboard *self = static_cast<Keyboard *>(data);
}

Seat::Keyboard::~Keyboard() {}

Seat::Pointer::Pointer(Seat *seat) : seat_{seat}, pointer_{wl_seat_get_pointer(seat_->seat_.get())}
{
	wl_pointer_add_listener(pointer_.get(), &Pointer::iface, this);
}

void
Seat::Pointer::enter(void *data,
		     struct wl_pointer *wl_pointer,
		     uint32_t serial,
		     struct wl_surface *surface,
		     wl_fixed_t surface_x,
		     wl_fixed_t surface_y)
{
	Pointer *self = static_cast<Pointer *>(data);

	auto &output = *self->seat_->backend_->output_;
	if (output.surface() == surface) {
		self->is_output_focus_ = true;

		double x = wl_fixed_to_double(surface_x);
		double y = wl_fixed_to_double(surface_y);

		uint64_t time = util::get_monotonic_time();

		self->seat_->virtual_seat_->pointer()->set_location(time, x, y);
	}
}

void
Seat::Pointer::leave(void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface)
{
	Pointer *self = static_cast<Pointer *>(data);
	auto &output = *self->seat_->backend_->output_;
	if (output.surface() == surface) {
		self->is_output_focus_ = false;
	}
}

void
Seat::Pointer::motion(
    void *data, struct wl_pointer *wl_pointer, uint32_t time, wl_fixed_t surface_x, wl_fixed_t surface_y)
{
	Pointer *self = static_cast<Pointer *>(data);

	if (!self->is_output_focus_) {
		return;
	}

	double x = wl_fixed_to_double(surface_x);
	double y = wl_fixed_to_double(surface_y);

	self->seat_->virtual_seat_->pointer()->set_location(time, x, y);
}

void
Seat::Pointer::button(void *data,
		      struct wl_pointer *wl_pointer,
		      uint32_t serial,
		      uint32_t time,
		      uint32_t button,
		      uint32_t state)
{
	Pointer *self = static_cast<Pointer *>(data);

	orbycomp::Pointer::ButtonState button_state;
	if (state == WL_POINTER_BUTTON_STATE_PRESSED) {
		button_state = orbycomp::Pointer::ButtonState::Press;
	} else {
		button_state = orbycomp::Pointer::ButtonState::Release;
	}

	self->seat_->virtual_seat_->pointer()->feed_button_event(time, button, button_state);
}

void
Seat::Pointer::axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value)
{
	Pointer *self = static_cast<Pointer *>(data);
}

void
Seat::Pointer::frame(void *data, struct wl_pointer *wl_pointer)
{
	Pointer *self = static_cast<Pointer *>(data);
}

void
Seat::Pointer::axis_source(void *data, struct wl_pointer *wl_pointer, uint32_t axis_source)
{
	Pointer *self = static_cast<Pointer *>(data);
}

void
Seat::Pointer::axis_stop(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis)
{
	Pointer *self = static_cast<Pointer *>(data);
}

void
Seat::Pointer::axis_discrete(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t discrete)
{
	Pointer *self = static_cast<Pointer *>(data);
}

Seat::Pointer::~Pointer() {}

} // namespace wl

} // namespace orbycomp
