/*
 * backend.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <orbycomp/backend.hh>
#include <orbycomp/base/output.hh>
#include <orbycomp/base/seat.hh>

#include <orbycomp/util/idle-event-queue.hh>

#include <ev.h>
#include <keyboard-shortcuts-inhibit-unstable-v1-client-protocol.h>
#include <wayland-client.h>
#include <xdg-shell-client-protocol.h>

#include <unordered_map>

#include "renderer.hh"
#include "shm-pool.hh"

extern "C" orbycomp::BackendBase *backend_init(orbycomp::Compositor *compositor);

namespace orbycomp
{

namespace wl
{

class Output;
class Seat;

template <typename wl_type, void destruct_func(wl_type *)> class Proxy
{
	struct ProxyDestroy {
		void
		operator()(wl_type *obj)
		{
			destruct_func(obj);
		}
	};

	std::unique_ptr<wl_type, ProxyDestroy> obj_;

public:
	Proxy() : obj_{nullptr} {}

	Proxy(wl_type *&&obj) : obj_{obj} {}
	~Proxy() {}

	Proxy(const Proxy &other) = delete;
	Proxy &operator=(const Proxy &other) = delete;

	Proxy(Proxy &&other) = default;
	Proxy &operator=(Proxy &&other) = default;

	wl_type *
	get() const
	{
		return obj_.get();
	}

	Proxy &
	operator=(wl_type *&&ptr)
	{
		obj_.reset(std::move(ptr));
		return *this;
	}
};

class Backend : public BackendBase
{
	friend Output;
	friend Seat;

	using DisplayPtr = std::unique_ptr<struct wl_display, decltype(&wl_display_disconnect)>;
	using RegistryPtr = std::unique_ptr<struct wl_registry, decltype(&wl_registry_destroy)>;

	DisplayPtr display_;
	RegistryPtr registry_;

	static DisplayPtr get_display();
	RegistryPtr get_registry();

	struct ev_io wayland_io_ {
	};
	static void on_wayland_event(EV_P, struct ev_io *w, int revents);

	static void on_registry_global(void *user_data,
				       struct wl_registry *registry,
				       uint32_t id,
				       const char *interface,
				       uint32_t version);
	static void on_registry_global_remove(void *user_data, struct wl_registry *registry, uint32_t id);

	static constexpr struct wl_registry_listener registry_listener = {
	    Backend::on_registry_global,
	    Backend::on_registry_global_remove,
	};

	struct Globals {
		Proxy<struct wl_compositor, wl_compositor_destroy> compositor_;

		Proxy<struct wl_shm, wl_shm_destroy> shm_;

		Proxy<struct xdg_wm_base, xdg_wm_base_destroy> wm_base_;

		Proxy<struct wl_subcompositor, wl_subcompositor_destroy> subcomp_;

		Proxy<struct zwp_keyboard_shortcuts_inhibit_manager_v1,
		      zwp_keyboard_shortcuts_inhibit_manager_v1_destroy>
		    shortcuts_inhibit_manager_;

		std::unordered_map<uint32_t, std::unique_ptr<Seat>> seats_;
	};

	std::unique_ptr<Output> output_;
	std::unique_ptr<Renderer> renderer_;

	static void wm_base_ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial);
	static constexpr struct xdg_wm_base_listener wm_base_listener = {
	    Backend::wm_base_ping,
	};

public:
	Globals globals_;

	Backend(Compositor *compositor);
	~Backend();

	inline Compositor *
	compositor() const
	{
		return compositor_;
	}

	inline struct wl_display *
	display() const
	{
		return display_.get();
	}

	orbycomp::Renderer *get_renderer() override;

	void repaint_output(orbycomp::Output *output) override;
};

class Output : public orbycomp::Output
{
	Backend *backend_;
	struct Frame {
		Output *output_;

		Proxy<struct wl_surface, wl_surface_destroy> surface_;
		Proxy<struct xdg_surface, xdg_surface_destroy> shell_surface_;
		bool has_been_configured_{false};

		Proxy<struct xdg_toplevel, xdg_toplevel_destroy> toplevel_;

		SHMPool pool_;
	} frame_;

	Proxy<struct wl_surface, wl_surface_destroy> output_surface_;
	Proxy<struct wl_subsurface, wl_subsurface_destroy> output_subsurf_;
	Proxy<struct zwp_keyboard_shortcuts_inhibitor_v1, zwp_keyboard_shortcuts_inhibitor_v1_destroy>
	    shortcuts_inhibitor_;

	Proxy<struct zwp_keyboard_shortcuts_inhibitor_v1, zwp_keyboard_shortcuts_inhibitor_v1_destroy>
	get_inhibitor();

	Frame create_frame();

	static void frame_configure(void *data, struct xdg_surface *surface, uint32_t serial);
	static constexpr xdg_surface_listener shell_surface_listener = {
	    Output::frame_configure,
	};

	static void frame_toplevel_configure(void *data,
					     struct xdg_toplevel *xdg_toplevel,
					     int32_t width,
					     int32_t height,
					     struct wl_array *states);
	static void frame_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel);
	static constexpr xdg_toplevel_listener toplevel_listener = {
	    Output::frame_toplevel_configure,
	    Output::frame_toplevel_close,
	};

public:
	Output(Backend *backend);
	~Output();

	struct wl_surface *
	surface()
	{
		return output_surface_.get();
	}
};

class Seat
{
	friend Backend;
	friend Output;

	Backend *backend_;
	Proxy<struct wl_seat, wl_seat_destroy> seat_;

	std::string name_{};

	util::EventToken capabilities_apply_event_;
	std::optional<enum ::wl_seat_capability> capabilities_apply_;

	void apply_capabilities();

	static void capabilities(void *data, struct wl_seat *wl_seat, uint32_t capabilities);

	static void name(void *data, struct wl_seat *wl_seat, const char *name);

	static constexpr wl_seat_listener iface = {
	    Seat::capabilities,
	    Seat::name,
	};

	orbycomp::Seat *virtual_seat_;

	bool has_keyboard_{false};
	bool has_pointer_{false};
	bool has_touch_{false};

	struct Keyboard {
		Seat *seat_;
		Proxy<struct wl_keyboard, wl_keyboard_destroy> keyboard_;

		static void keymap(
		    void *data, struct wl_keyboard *wl_keyboard, uint32_t format, int32_t fd, uint32_t size);
		static void enter(void *data,
				  struct wl_keyboard *wl_keyboard,
				  uint32_t serial,
				  struct wl_surface *surface,
				  struct wl_array *keys);
		static void leave(void *data,
				  struct wl_keyboard *wl_keyboard,
				  uint32_t serial,
				  struct wl_surface *surface);
		static void key(void *data,
				struct wl_keyboard *wl_keyboard,
				uint32_t serial,
				uint32_t time,
				uint32_t key,
				uint32_t state);
		static void modifiers(void *data,
				      struct wl_keyboard *wl_keyboard,
				      uint32_t serial,
				      uint32_t mods_depressed,
				      uint32_t mods_latched,
				      uint32_t mods_locked,
				      uint32_t group);
		static void
		repeat_info(void *data, struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay);

		static constexpr wl_keyboard_listener iface = {
		    Keyboard::keymap, Keyboard::enter,     Keyboard::leave,
		    Keyboard::key,    Keyboard::modifiers, Keyboard::repeat_info,
		};

		bool is_output_focus_{false};

		Keyboard(Seat *seat);
		~Keyboard();
	};

	struct Pointer {
		Seat *seat_;
		Proxy<struct wl_pointer, wl_pointer_destroy> pointer_;

		static void enter(void *data,
				  struct wl_pointer *wl_pointer,
				  uint32_t serial,
				  struct wl_surface *surface,
				  wl_fixed_t surface_x,
				  wl_fixed_t surface_y);
		static void
		leave(void *data, struct wl_pointer *wl_pointer, uint32_t serial, struct wl_surface *surface);
		static void motion(void *data,
				   struct wl_pointer *wl_pointer,
				   uint32_t time,
				   wl_fixed_t surface_x,
				   wl_fixed_t surface_y);
		static void button(void *data,
				   struct wl_pointer *wl_pointer,
				   uint32_t serial,
				   uint32_t time,
				   uint32_t button,
				   uint32_t state);
		static void axis(void *data,
				 struct wl_pointer *wl_pointer,
				 uint32_t time,
				 uint32_t axis,
				 wl_fixed_t value);
		static void frame(void *data, struct wl_pointer *wl_pointer);
		static void axis_source(void *data, struct wl_pointer *wl_pointer, uint32_t axis_source);
		static void
		axis_stop(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis);
		static void
		axis_discrete(void *data, struct wl_pointer *wl_pointer, uint32_t axis, int32_t discrete);

		static constexpr wl_pointer_listener iface = {
		    Pointer::enter,       Pointer::leave,     Pointer::motion,
		    Pointer::button,      Pointer::axis,      Pointer::frame,
		    Pointer::axis_source, Pointer::axis_stop, Pointer::axis_discrete,
		};

		bool is_output_focus_{false};

		Pointer(Seat *seat);
		~Pointer();
	};

	std::optional<Keyboard> keyboard_;
	std::optional<Pointer> pointer_;

public:
	Seat(Backend *backend, uint32_t id, uint32_t version);
	~Seat();
};

}; // namespace wl

}; // namespace orbycomp
