/*
 * renderer.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "renderer.hh"
#include <stdexcept>

#include "gles-renderer.hh"
#include "software-renderer.hh"

namespace orbycomp
{

namespace wl
{

std::unique_ptr<Renderer>
Renderer::choose_renderer(Backend *backend)
{
	try {
		return std::make_unique<GLESRenderer>(backend);
	} catch (std::exception &ex) {
		std::fprintf(stderr, "Could not load GLES renderer: %s\n", ex.what());
		std::fprintf(stderr, "Trying next renderer\n");
	}
	try {
		return std::make_unique<SoftwareRenderer>(backend);
	} catch (std::exception &ex) {
		std::fprintf(stderr, "Could not load software renderer: %s\n", ex.what());
		std::fprintf(stderr, "Trying next renderer\n");
	}
	throw std::runtime_error("Could not find suitable renderer");
}

} // namespace wl

} // namespace orbycomp
