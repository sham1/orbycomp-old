/*
 * shm-pool.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <wayland-client.h>

#include <memory>

namespace orbycomp
{

namespace wl
{

class SHMMemRegion
{
	int fd_{-1};
	size_t capacity_{0};
	void *backing_{nullptr};

	bool valid_{false};

public:
	SHMMemRegion() {}

	SHMMemRegion(size_t capacity);
	~SHMMemRegion();

	int
	fd() const
	{
		return fd_;
	}

	size_t
	capacity() const
	{
		return capacity_;
	}

	void *
	base()
	{
		return backing_;
	}

	SHMMemRegion(const SHMMemRegion &other) = delete;
	SHMMemRegion &operator=(const SHMMemRegion &other) = delete;

	SHMMemRegion(SHMMemRegion &&other);
	SHMMemRegion &operator=(SHMMemRegion &&other);
};

class SHMPool
{
	struct wl_shm *shm_{nullptr};

	int32_t width_;
	int32_t height_;
	enum wl_shm_format format_;
	size_t stride_;

	size_t buffer_capacity_;
	SHMMemRegion mem_;

	struct WLSHMPool {
		struct wl_shm_pool *pool_{nullptr};

		WLSHMPool() {}
		WLSHMPool(struct wl_shm_pool *pool) : pool_{pool} {}

		~WLSHMPool()
		{
			if (pool_) {
				wl_shm_pool_destroy(pool_);
				pool_ = nullptr;
			}
		}

		WLSHMPool(const WLSHMPool &other) = delete;
		WLSHMPool &operator=(const WLSHMPool &other) = delete;

		WLSHMPool(WLSHMPool &&other) : pool_{std::move(other.pool_)} { other.pool_ = nullptr; }

		WLSHMPool &
		operator=(WLSHMPool &&other)
		{
			pool_ = std::move(other.pool_);
			other.pool_ = nullptr;

			return *this;
		}
	} pool_;

	struct BufferDestroyer {
		void
		operator()(struct wl_buffer *buf)
		{
			wl_buffer_destroy(buf);
		}
	};

	using WLBuffer = std::unique_ptr<struct wl_buffer, BufferDestroyer>;
	std::array<WLBuffer, 2> wl_buffers_;
	size_t active_buffer_{0};

	size_t
	get_offset() const
	{
		return buffer_capacity_ * active_buffer_;
	}

	std::array<WLBuffer, 2> create_buffers();

	size_t figure_stride();

public:
	SHMPool() {}

	SHMPool(struct wl_shm *shm, int32_t width, int32_t height, enum wl_shm_format format);
	~SHMPool();

	SHMPool(const SHMPool &other) = delete;
	SHMPool &operator=(const SHMPool &other) = delete;

	SHMPool(SHMPool &&other) = default;
	SHMPool &operator=(SHMPool &&other) = default;

	int32_t
	width() const
	{
		return width_;
	}

	int32_t
	height() const
	{
		return height_;
	}

	int32_t
	stride() const
	{
		return stride_;
	}

	class BufferAccessor
	{
		friend SHMPool;

		uint8_t *begin_;
		uint8_t *end_;

		BufferAccessor(uint8_t *begin, int32_t height, size_t stride);

	public:
		~BufferAccessor();

		uint8_t *
		begin()
		{
			return begin_;
		}

		uint8_t *
		end()
		{
			return end_;
		}

		const uint8_t *
		begin() const
		{
			return begin_;
		}

		const uint8_t *
		end() const
		{
			return end_;
		}
	};

	BufferAccessor access_current_buffer();

	uint8_t *get_buffer_base();

	struct wl_buffer *
	swap_active()
	{
		struct wl_buffer *current{wl_buffers_[active_buffer_].get()};
		active_buffer_ = (active_buffer_ + 1) % wl_buffers_.size();
		return current;
	}
};

}; // namespace wl

}; // namespace orbycomp
