/*
 * shm-pool.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "shm-pool.hh"

#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif
#define _POSIX_C_SOURCE 200112L

#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include <stdexcept>
#include <system_error>

namespace orbycomp
{

namespace wl
{

static void
randname(char *buf)
{
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	int64_t r{ts.tv_nsec};

	for (size_t i = 0; i < 6; ++i) {
		buf[i] = 'A' + (r & 15) + (r & 16) * 2;
		r >>= 5;
	}
}

static int
create_shm_file(void)
{
	int retries = 100;
	do {
		char name[]{"/wl_shm-XXXXXX"};
		randname(name + sizeof(name) - 7);
		--retries;

		int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
		if (fd >= 0) {
			shm_unlink(name);
			return fd;
		}
	} while (retries > 0 && errno == EEXIST);
	return -1;
}

SHMMemRegion::SHMMemRegion(size_t capacity) : fd_{create_shm_file()}, capacity_{capacity}
{
	if (fd_ < 0) {
		throw std::runtime_error("Could not create SHM buffer file");
	}

	int ret;
	do {
		ret = ftruncate(fd_, capacity_);
	} while (ret < 0 && errno == EINTR);
	if (ret < 0) {
		close(fd_);
		fd_ = -1;
		throw std::system_error(errno, std::generic_category(), "Could not create SHM buffer file");
	}

	backing_ = mmap(nullptr, capacity_, PROT_READ | PROT_WRITE, MAP_SHARED, fd_, 0);
	valid_ = true;
}

SHMMemRegion::~SHMMemRegion()
{
	if (valid_) {
		munmap(backing_, capacity_);
		backing_ = nullptr;

		close(fd_);
		fd_ = -1;

		valid_ = false;
	}
}

SHMMemRegion::SHMMemRegion(SHMMemRegion &&other)
    : fd_{std::move(other.fd_)}, capacity_{std::move(other.capacity_)}, backing_{std::move(other.backing_)},
      valid_{std::move(other.valid_)}
{
	other.fd_ = -1;
	other.capacity_ = 0;
	other.backing_ = nullptr;
	other.valid_ = false;
}

SHMMemRegion &
SHMMemRegion::operator=(SHMMemRegion &&other)
{
	fd_ = std::move(other.fd_);
	capacity_ = std::move(other.capacity_);
	backing_ = std::move(other.backing_);
	valid_ = std::move(other.valid_);

	other.fd_ = -1;
	other.capacity_ = 0;
	other.backing_ = nullptr;
	other.valid_ = false;

	return *this;
}

SHMPool::SHMPool(struct wl_shm *shm, int32_t width, int32_t height, enum wl_shm_format format)
    : shm_{shm}, width_{width}, height_{height}, format_{format}, stride_{figure_stride()},
      buffer_capacity_{height_ * stride_}, mem_{2 * buffer_capacity_},
      pool_{wl_shm_create_pool(shm_, mem_.fd(), mem_.capacity())}, wl_buffers_{create_buffers()}
{
}

size_t
SHMPool::figure_stride()
{
	size_t bytes_per_pixel{0};
	switch (format_) {
	case WL_SHM_FORMAT_ARGB8888:
	case WL_SHM_FORMAT_XRGB8888:
		bytes_per_pixel = 4;
		break;
	default:
		// Add more formats if need be.
		throw std::runtime_error("Unsupported SHM format");
	}

	return width_ * bytes_per_pixel;
}

std::array<SHMPool::WLBuffer, 2>
SHMPool::create_buffers()
{
	WLBuffer first{wl_shm_pool_create_buffer(pool_.pool_, 0, width_, height_, stride_, format_)};
	WLBuffer second{
	    wl_shm_pool_create_buffer(pool_.pool_, buffer_capacity_, width_, height_, stride_, format_)};
	return {std::move(first), std::move(second)};
}

SHMPool::BufferAccessor
SHMPool::access_current_buffer()
{
	size_t offset = height_ * stride_ * active_buffer_;
	return BufferAccessor{static_cast<uint8_t *>(mem_.base()) + offset, height_, stride_};
}

uint8_t *
SHMPool::get_buffer_base()
{
	size_t offset = height_ * stride_ * active_buffer_;
	return static_cast<uint8_t *>(mem_.base()) + offset;
}

SHMPool::~SHMPool() {}

SHMPool::BufferAccessor::BufferAccessor(uint8_t *begin, int32_t height, size_t stride)
    : begin_{begin}, end_{begin + height * stride}
{
}

SHMPool::BufferAccessor::~BufferAccessor() {}

} // namespace wl

}; // namespace orbycomp
