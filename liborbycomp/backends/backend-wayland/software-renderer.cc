/*
 * software-renderer.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "software-renderer.hh"
#include <orbycomp/base/compositor.hh>
#include <orbycomp/renderers/software-renderer.hh>

namespace orbycomp
{

namespace wl
{

SoftwareRenderer::SoftwareRenderer(Backend *backend)
    : Renderer{backend, SoftwareRenderer::load_renderer(backend)}
{
}

SoftwareRenderer::~SoftwareRenderer() {}

RendererHandle
SoftwareRenderer::load_renderer(Backend *backend)
{
	renderers::SoftwareRendererOptions opts{};
	opts.compositor = backend->compositor();

	return orbycomp::load_renderer("liborbycomp-software-renderer.so", opts);
}

void
SoftwareRenderer::setup_output(Output *output)
{
	auto [info, _] = outputs_.try_emplace(output, std::make_unique<OutputInfo>(this, output));

	auto *output_info = info->second.get();

	renderers::SoftwareRendererOutputOptions opts{};
	opts.get_fb = [output_info]() { return output_info->get_framebuffer(); };

	renderer_->setup_output(output, opts);
}

void
SoftwareRenderer::render_output(Output *output)
{
	outputs_[output]->render();
}

SoftwareRenderer::OutputInfo::OutputInfo(SoftwareRenderer *renderer, Output *output)
    : renderer_{renderer}, output_{output}, output_pool_{renderer_->backend_->globals_.shm_.get(), 800, 600,
							 WL_SHM_FORMAT_XRGB8888}
{
}

SoftwareRenderer::OutputInfo::~OutputInfo() {}

void
SoftwareRenderer::OutputInfo::render()
{
	renderer_->renderer_->render_output(output_);

	struct wl_buffer *new_buffer = output_pool_.swap_active();

	struct wl_surface *surface = output_->surface();
	wl_surface_attach(surface, new_buffer, 0, 0);

	struct wl_callback *frame_cb{wl_surface_frame(surface)};
	wl_callback_add_listener(frame_cb, &SoftwareRenderer::frame_cb_listener, renderer_);

	wl_surface_commit(surface);
}

rendering::software::BitmapSurface
SoftwareRenderer::OutputInfo::get_framebuffer()
{
	auto width = output_pool_.width();
	auto height = output_pool_.height();
	auto stride = size_t(output_pool_.stride());

	return {output_pool_.get_buffer_base(), stride, width, height};
}

void
SoftwareRenderer::on_frame_done(void *data, struct wl_callback *wl_callback, uint32_t callback_data)
{
	SoftwareRenderer *self = static_cast<SoftwareRenderer *>(data);

	auto &renderables = self->backend_->compositor()->renderables_;
	for (auto *r : renderables) {
		if (r->layer() == Renderable::Layer::Hidden) {
			continue;
		}

		r->render_finish(callback_data);
	}

	wl_callback_destroy(wl_callback);
}

} // namespace wl

}; // namespace orbycomp
