/*
 * gles-renderer.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gles-renderer.hh"
#include <orbycomp/base/compositor.hh>
#include <orbycomp/renderers/gles-renderer.hh>

#include <drm/drm_fourcc.h>

namespace orbycomp
{

namespace wl
{

GLESRenderer::GLESRenderer(Backend *backend) : Renderer{backend, load_renderer(backend)} {}

GLESRenderer::~GLESRenderer() {}

RendererHandle
GLESRenderer::load_renderer(Backend *backend)
{
	static constexpr uint32_t formats[] = {
	    DRM_FORMAT_XRGB8888,
	};

	renderers::GLESRendererOptions opts{};
	opts.compositor = backend->compositor();

	opts.backend_dpy = backend->display();
	opts.formats = formats;
	opts.formats_count = 1;
	opts.platform = EGL_PLATFORM_WAYLAND_EXT;

	return orbycomp::load_renderer("liborbycomp-gles-renderer.so", opts);
}

void
GLESRenderer::setup_output(Output *output)
{
	auto [info, _] = outputs_.try_emplace(output, std::make_unique<OutputInfo>(this, output));

	auto *output_info = info->second.get();

	renderers::GLESRendererOutputOptions opts{};
	opts.backend_window = output_info->egl_surface_.get();

	renderer_->setup_output(output, opts);
}

void
GLESRenderer::render_output(Output *output)
{
	outputs_[output]->render();
}

void
GLESRenderer::on_frame_done(void *data, struct wl_callback *wl_callback, uint32_t callback_data)
{
	GLESRenderer *self = static_cast<GLESRenderer *>(data);

	auto &renderables = self->backend_->compositor()->renderables_;
	for (auto *r : renderables) {
		if (r->layer() == Renderable::Layer::Hidden) {
			continue;
		}

		r->render_finish(callback_data);
	}

	wl_callback_destroy(wl_callback);
}

GLESRenderer::OutputInfo::OutputInfo(GLESRenderer *renderer, Output *output)
    : renderer_{renderer}, output_{output}, egl_surface_{wl_egl_window_create(output_->surface(), 800, 600),
							 wl_egl_window_destroy}

{
}

GLESRenderer::OutputInfo::~OutputInfo() {}

void
GLESRenderer::OutputInfo::render()
{
	auto *frame_cb = wl_surface_frame(output_->surface());
	wl_callback_add_listener(frame_cb, &GLESRenderer::frame_cb_listener, renderer_);

	renderer_->renderer_->render_output(output_);
}

} // namespace wl

} // namespace orbycomp
