/*
 * renderer.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <orbycomp/base/surface.hh>
#include <orbycomp/renderer-loader.hh>

namespace orbycomp
{

namespace wl
{

class Backend;
class Output;

class Renderer
{
protected:
	Backend *backend_;
	RendererHandle renderer_{};

	Renderer(Backend *backend, RendererHandle &&renderer)
	    : backend_{backend}, renderer_{std::move(renderer)}
	{
	}

public:
	virtual ~Renderer() {}

	static std::unique_ptr<Renderer> choose_renderer(Backend *backend);

	orbycomp::Renderer *
	get() const
	{
		return renderer_.get();
	}

	virtual void setup_output(Output *output) = 0;
	virtual void render_output(Output *output) = 0;
};

} // namespace wl

} // namespace orbycomp
