/*
 * gles2-renderer.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gles2-renderer.hh"
#include <orbycomp/affine.hh>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/render/software-rendering.hh>

#include <orbycomp/util/time.hh>

namespace orbycomp
{

namespace drm
{

static constexpr char renderable_vertex_shader[] = "attribute vec2 pos;\n"
						   "attribute vec2 texCoord;\n"
						   "\n"
						   "varying vec2 TexCoord;\n"
						   "\n"
						   "uniform mat4 modelview;\n"
						   "uniform mat4 projection;\n"
						   "\n"
						   "void main(void) {\n"
						   "	vec4 translated_pos = modelview * vec4(pos, 1, 1);\n"
						   "	gl_Position = projection * translated_pos;\n"
						   "	TexCoord = texCoord;\n"
						   "}\n";

static constexpr char renderable_fragment_shader[] = "precision mediump float;\n"
						     "\n"
						     "varying vec2 TexCoord;\n"
						     "\n"
						     "uniform sampler2D buffer;\n"
						     "\n"
						     "void main(void) {\n"
						     "	gl_FragColor = texture2D(buffer, TexCoord);\n"
						     "}\n";

struct Vertex {
	GLfloat pos[2];
	GLfloat texCoord[2];
};

constexpr Vertex renderable_rect_vertices[] = {
    {{0.0f, 0.0f}, {0.0f, 0.0f}},

    {{0.0f, 1.0f}, {0.0f, 1.0f}},

    {{1.0f, 1.0f}, {1.0f, 1.0f}},

    {{1.0f, 0.0f}, {1.0f, 0.0f}},
};

constexpr GLushort renderable_rect_indices[] = {
    0, 1, 2,

    3, 0, 2,
};

static inline bool
renderable_sort(const std::weak_ptr<Renderable> &a, const std::weak_ptr<Renderable> &b)
{
	auto a_ptr = a.lock();
	auto b_ptr = b.lock();

	// Push all "removed" renderables
	// to the end of the vector.
	if (!a_ptr) {
		return false;
	}
	if (!b_ptr) {
		return true;
	}

	return Renderable::comparator_(*a_ptr, *b_ptr);
}

GLES2Renderer::GLES2Renderer(Backend *backend)
    : Renderer{backend}, device_{backend_->drm_dev.fd()}, display_{this},
      cursor_bo_{
	  gbm_bo_create(device_.get(),
			128,
			128,
			GBM_BO_FORMAT_ARGB8888,
			GBM_BO_USE_WRITE | GBM_BO_USE_CURSOR | GBM_BO_USE_RENDERING | GBM_BO_USE_SCANOUT),
	  gbm_bo_destroy}
{
	if (!cursor_bo_) {
		throw std::runtime_error("Could not create cursor BO");
	}

	event_ctx_.version = 2;
	event_ctx_.page_flip_handler = GLES2Renderer::on_page_flip;

	backend_->compositor()->activity_change_signal.connect([this](bool active) {
		if (active) {
			for (auto &[output, info] : this->output_info_) {
				info.needs_repaint = true;
				info.set_crtc();
				this->queue_render_full(output);
			}
		} else {
			for (auto &[_, info] : this->output_info_) {
				info.needs_repaint = false;
			}
		}
	});
}

GLES2RendererOutputInfo::GLES2RendererOutputInfo(GLES2Renderer *renderer,
						 Output *output,
						 GBMSurface &&surface)
    : renderer_{renderer}, output_{output}, surface_{std::move(surface)}, egl_config_{get_config()},
      egl_context_{get_context()}, egl_surface_{create_egl_surface()}
{
	eglMakeCurrent(renderer_->display_.get(), egl_surface_, egl_surface_, egl_context_);

	eglSwapBuffers(renderer_->display_.get(), egl_surface_);

	bo_ = gbm_surface_lock_front_buffer(surface_.get());

	set_crtc();

	renderable_shader_ = opengl::ShaderProgram()
				 .attach(opengl::Shader{renderable_vertex_shader, GL_VERTEX_SHADER})
				 .attach(opengl::Shader{renderable_fragment_shader, GL_FRAGMENT_SHADER})
				 .bind_attribute_location(0, "pos")
				 .bind_attribute_location(1, "texCoord")
				 .link();
}

EGLConfig
GLES2RendererOutputInfo::get_config()
{
	EGLConfig config{};

	for (const auto &conf : renderer_->display_.configs()) {
		EGLint format;
		if (!eglGetConfigAttrib(renderer_->display_.get(), conf, EGL_NATIVE_VISUAL_ID, &format)) {
			throw std::runtime_error("Could not get EGL configuration attribute");
		}

		if (format == GBM_FORMAT_XRGB8888) {
			config = conf;
			break;
		}
	}
	return config;
}

EGLContext
GLES2RendererOutputInfo::get_context()
{
	EGLint context_attribs[] = {
	    EGL_CONTEXT_CLIENT_VERSION,
	    2,

	    EGL_NONE,
	};

	EGLContext ret =
	    eglCreateContext(renderer_->display_.get(), egl_config_, EGL_NO_CONTEXT, context_attribs);
	if (ret == EGL_NO_CONTEXT) {
		throw std::runtime_error("Could not create EGL context");
	}

	return ret;
}

EGLSurface
GLES2RendererOutputInfo::create_egl_surface()
{

	EGLSurface ret = eglCreatePlatformWindowSurfaceEXT(renderer_->display_.get(), egl_config_,
							   surface_.get(), nullptr);

	if (ret == EGL_NO_SURFACE) {
		throw std::runtime_error("Could not create EGL surface for display");
	}

	return ret;
}

GLES2RendererOutputInfo::~GLES2RendererOutputInfo() {}

GLES2Renderer::egl_display::egl_display(GLES2Renderer *renderer)
    : renderer_{renderer}, dpy{eglGetDisplay(renderer_->device_.get())}

{
}

GLES2Renderer::egl_display::~egl_display()
{
	unbind_wayland_display_wl(dpy, renderer_->backend_->compositor()->display());
}

void
GLES2Renderer::on_page_flip(
    int fd, unsigned int sequence, unsigned int tv_sec, unsigned int tv_usec, void *user_data)
{
	GLES2RendererOutputInfo *info = static_cast<GLES2RendererOutputInfo *>(user_data);

	info->is_drawing = false;

	gbm_surface_release_buffer(info->surface_.get(), info->bo_);
	info->bo_ = info->next_bo_;

	uint32_t tv_msec = tv_sec * 1000;
	tv_msec += tv_usec / 1000;

	for (auto &r : info->renderer_->backend_->compositor()->renderables_) {
		auto renderable = r.lock();
		if (!renderable) {
			continue;
		}

		if (renderable->layer() == Renderable::Layer::Hidden) {
			continue;
		}

		renderable->render_finish(tv_msec);
	}
}

std::shared_ptr<SurfaceRenderable>
GLES2Renderer::get_surface_renderable(Surface *surface)
{
	auto ret = std::make_shared<GLES2SurfaceRenderable>(surface, this);

	backend_->compositor()->renderables_.emplace_back(ret);

	return ret;
}

std::shared_ptr<BitmapRenderable>
GLES2Renderer::get_bitmap_renderable(int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha)
{
	return std::make_shared<GLES2BitmapRenderable>(x, y, width, height, has_alpha);
}

void
GLES2Renderer::setup_display(std::shared_ptr<Output> output)
{
	using GBMSurface = GLES2RendererOutputInfo::GBMSurface;

	const auto &mode = output->current_mode();
	int32_t width = mode.width_;
	int32_t height = mode.height_;

	GBMSurface surface = GBMSurface{gbm_surface_create(device_.get(), width, height, GBM_FORMAT_XRGB8888,
							   GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING),
					gbm_surface_destroy};
	if (!surface) {
		throw std::system_error(errno, std::generic_category(), "Could not setup GBM surface");
	}

	output_info_.try_emplace(output, this, output.get(), std::move(surface));
}

void
GLES2Renderer::queue_render_full(std::shared_ptr<Output> output)
{
	GLES2RendererOutputInfo &info = output_info_.at(output);
	if (info.needs_repaint && !info.is_drawing) {
		info.render_output();
	}
}

void
GLES2RendererOutputInfo::set_crtc()
{
	int fd = renderer_->backend_->drm_dev.fd();

	SurfaceFB *fb = SurfaceFB::get_from_bo(this, bo_);
	drmModeSetCrtc(fd, output_->encoder_->crtc_id, fb->fb_, 0, 0, &output_->connector_->connector_id, 1,
		       &output_->connector_->modes[output_->current_mode_]);
}

void
GLES2RendererOutputInfo::render()
{
	eglMakeCurrent(renderer_->display_.get(), egl_surface_, egl_surface_, egl_context_);

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glViewport(0, 0, output_->current_mode().width_, output_->current_mode().height_);

	renderable_shader_.activate();

	GLuint vbo, index_buf;
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &index_buf);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(renderable_rect_vertices), renderable_rect_vertices,
		     GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			      reinterpret_cast<void *>(util::offset_of(&Vertex::pos)));

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			      reinterpret_cast<void *>(util::offset_of(&Vertex::texCoord)));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buf);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(renderable_rect_indices), renderable_rect_indices,
		     GL_STATIC_DRAW);

	for (auto &r : renderer_->backend_->compositor()->renderables_) {
		auto renderable = r.lock();

		if (!renderable) {
			break;
		}

		if (renderable->layer() == Renderable::Layer::Hidden) {
			continue;
		}

		auto surface_renderable = std::dynamic_pointer_cast<GLES2SurfaceRenderable>(renderable);
		if (surface_renderable) {
			GLint buffer_uniform = renderable_shader_.uniform_location("buffer");

			surface_renderable->bind_texture_to_loc(buffer_uniform);
			if (!surface_renderable->is_opaque_) {
				glEnable(GL_BLEND);
			} else {
				glDisable(GL_BLEND);
			}
		}

		float rx = float(renderable->offset_x());
		float ry = float(renderable->offset_y());
		if (renderable->parent()) {
			auto *rp = renderable->parent();
			while (rp) {
				rx += rp->offset_x();
				ry += rp->offset_y();

				rp = rp->parent();
			}
		}

		affine::Mat4 translate_matrix = affine::create_translation_matrix({rx, ry});

		GLint modelview_uniform = renderable_shader_.uniform_location("modelview");

		affine::Mat4 scale_matrix =
		    affine::create_scaling_matrix(renderable->width(), renderable->height(), 0);

		affine::Mat4 modelview_matrix = translate_matrix * scale_matrix;

		std::array<float, 16> modelview_data = modelview_matrix;

		glUniformMatrix4fv(modelview_uniform, 1, GL_FALSE, modelview_data.data());

		GLint projection_uniform = renderable_shader_.uniform_location("projection");

		affine::Mat4 projection_matrix =
		    affine::create_ortho_matrix(0, float(output_->current_mode().width_),
						float(output_->current_mode().height_), 0, 0, 1);

		std::array<float, 16> matrix_data = projection_matrix;

		glUniformMatrix4fv(projection_uniform, 1, GL_FALSE, matrix_data.data());

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
	}

	renderable_shader_.deactivate();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDeleteBuffers(1, &index_buf);
	glDeleteBuffers(1, &vbo);
}

void
GLES2RendererOutputInfo::render_output()
{
	render();

	eglSwapBuffers(renderer_->display_.get(), egl_surface_);
	next_bo_ = gbm_surface_lock_front_buffer(surface_.get());

	if (next_bo_) {
		is_drawing = true;
		SurfaceFB *fb = SurfaceFB::get_from_bo(this, next_bo_);

		drmModePageFlip(renderer_->backend_->drm_dev.fd(), output_->encoder_->crtc_id, fb->fb_,
				DRM_MODE_PAGE_FLIP_EVENT, this);
	}
}

GLES2RendererOutputInfo::SurfaceFB::SurfaceFB(GLES2RendererOutputInfo *info, struct gbm_bo *bo)
    : info_{info}, bo_{bo}, fb_{create_fb_from_bo(bo_)}
{
}

uint32_t
GLES2RendererOutputInfo::SurfaceFB::create_fb_from_bo(struct gbm_bo *bo)
{
	int fd = info_->renderer_->backend_->drm_dev.fd();

	uint32_t fb{0};

	uint32_t width{gbm_bo_get_width(bo)};
	uint32_t height{gbm_bo_get_height(bo)};
	uint32_t format{gbm_bo_get_format(bo)};

	int ret{-1};

	uint32_t handles[4]{0};
	uint32_t strides[4]{0};
	uint32_t offsets[4]{0};
	uint32_t flags{0};

	uint64_t modifiers[4]{0};

	modifiers[0] = gbm_bo_get_modifier(bo);
	int planes{gbm_bo_get_plane_count(bo)};
	for (int i = 0; i < planes; ++i) {
		strides[i] = gbm_bo_get_stride_for_plane(bo, i);
		handles[i] = gbm_bo_get_handle(bo).u32;
		offsets[i] = gbm_bo_get_offset(bo, i);
		modifiers[i] = modifiers[0];
	}

	ret = drmModeAddFB2WithModifiers(fd, width, height, format, handles, strides, offsets, modifiers, &fb,
					 flags);

	if (ret) {
		uint32_t default_handles[4]{gbm_bo_get_handle(bo).u32, 0, 0, 0};
		uint32_t default_strides[4]{gbm_bo_get_stride(bo), 0, 0, 0};
		uint32_t default_offsets[4]{0};
		ret = drmModeAddFB2(fd, width, height, format, default_handles, default_strides,
				    default_offsets, &fb, 0);
		if (ret) {
			throw std::system_error(errno, std::generic_category(),
						"Could not create framebuffer for GBM buffer");
		}
	}

	return fb;
}

GLES2RendererOutputInfo::SurfaceFB::~SurfaceFB()
{
	if (fb_) {
		drmModeRmFB(info_->renderer_->backend_->drm_dev.fd(), fb_);
		fb_ = 0;
	}
}

void
GLES2RendererOutputInfo::SurfaceFB::on_fb_destroy_cb(struct gbm_bo *bo, void *data)
{
	SurfaceFB *self = static_cast<SurfaceFB *>(data);
	delete self;
}

GLES2RendererOutputInfo::SurfaceFB *
GLES2RendererOutputInfo::SurfaceFB::get_from_bo(GLES2RendererOutputInfo *info, struct gbm_bo *bo)
{
	SurfaceFB *self = static_cast<SurfaceFB *>(gbm_bo_get_user_data(bo));
	if (self) {
		return self;
	}

	self = new SurfaceFB(info, bo);

	gbm_bo_set_user_data(bo, self, SurfaceFB::on_fb_destroy_cb);

	return self;
}

drmEventContextPtr
GLES2Renderer::drm_event_context()
{
	return &event_ctx_;
}

bool
GLES2Renderer::render_cursor_to_bo(Renderable *renderable)
{
	if (auto *surface_renderable = dynamic_cast<GLES2SurfaceRenderable *>(renderable)) {
		surface_renderable->is_cursor_ = true;
		auto *surface = surface_renderable->surface();
		if (auto *shm_buf = wl_shm_buffer_get(surface->buffer())) {
			int width = wl_shm_buffer_get_width(shm_buf);
			int height = wl_shm_buffer_get_height(shm_buf);

			GBMDumbMap cursor_bo{cursor_bo_.get(), 0, 0, 128, 128, GBM_BO_TRANSFER_WRITE};

			rendering::software::BitmapSurface cursor_surface{
			    static_cast<uint8_t *>(cursor_bo.map()), cursor_bo.stride(), 128, 128};

			wl_shm_buffer_begin_access(shm_buf);

			rendering::software::BitmapSurface renderable_surface{
			    static_cast<uint8_t *>(wl_shm_buffer_get_data(shm_buf)),
			    static_cast<size_t>(wl_shm_buffer_get_stride(shm_buf)), width, height};

			rendering::software::BitmapSurface::BlitRegion renderable_region{0, 0, width, height};

			renderable_surface.blit_to(renderable_region, cursor_surface, 0, 0, false);

			wl_shm_buffer_end_access(shm_buf);

			surface->call_frame_cbs(util::get_monotonic_time());

			wl_buffer_send_release(surface->buffer());
		} else {
			std::fprintf(stderr, "Only SHM surface may be a cursor!\n");

			return false;
		}
	} else if (auto *bitmap_renderable = dynamic_cast<GLES2BitmapRenderable *>(renderable)) {
		GBMDumbMap cursor_bo{cursor_bo_.get(), 0, 0, 128, 128, GBM_BO_TRANSFER_WRITE};
		bitmap_renderable->is_cursor_ = true;

		rendering::software::BitmapSurface cursor_surface{static_cast<uint8_t *>(cursor_bo.map()),
								  cursor_bo.stride(), 128, 128};

		rendering::software::BitmapSurface renderable_surface{
		    bitmap_renderable->buffer_.data(), static_cast<size_t>(bitmap_renderable->width_ * 4),
		    bitmap_renderable->width_, bitmap_renderable->height_};

		rendering::software::BitmapSurface::BlitRegion renderable_region{
		    0, 0, bitmap_renderable->width_, bitmap_renderable->height_};

		renderable_surface.blit_to(renderable_region, cursor_surface, 0, 0, false);
	}

	return true;
}

void
GLES2Renderer::set_cursor(Pointer *pointer, Renderable *renderable, int32_t hotx, int32_t hoty)
{
	if (render_cursor_to_bo(renderable)) {
		GLES2Renderable *gles_renderable = (GLES2Renderable *) renderable;

		gles_renderable->is_cursor_ = true;

		cursor_.emplace();

		cursor_->pointer = pointer;
		cursor_->hotx = hotx;
		cursor_->hoty = hoty;
		cursor_->pointer_move_connection = cursor_->pointer->on_move_signal.connect(
		    [this](Pointer *, double new_x, double new_y, uint64_t) {
			    this->set_cursor_position(new_x, new_y);
		    });

		activate_cursor();
	}
}

void
GLES2Renderer::activate_cursor()
{
	if (!cursor_) {
		return;
	}

	uint32_t cursor_bo_handle = gbm_bo_get_handle(cursor_bo_.get()).u32;
	for (auto &[output, _] : this->output_info_) {
		uint32_t crtc_id = output->encoder_->crtc_id;
		drmModeSetCursor2(backend_->drm_dev.fd(), crtc_id, cursor_bo_handle, 128, 128, cursor_->hotx,
				  cursor_->hoty);

		int32_t output_x = static_cast<int32_t>(cursor_->pointer->x()) - output->x();
		int32_t output_y = static_cast<int32_t>(cursor_->pointer->y()) - output->y();
		drmModeMoveCursor(backend_->drm_dev.fd(), crtc_id, output_x - cursor_->hotx,
				  output_y - cursor_->hoty);
	}
}

void
GLES2Renderer::set_cursor_position(double x, double y)
{
	for (auto &[output, _] : this->output_info_) {
		uint32_t crtc_id = output->encoder_->crtc_id;
		int32_t output_x = static_cast<int32_t>(x) - output->x();
		int32_t output_y = static_cast<int32_t>(y) - output->y();

		drmModeMoveCursor(backend_->drm_dev.fd(), crtc_id, output_x - cursor_->hotx,
				  output_y - cursor_->hoty);
	}
}

void
GLES2Renderer::hide_cursor()
{
	for (auto &[output, _] : this->output_info_) {
		uint32_t crtc_id = output->encoder_->crtc_id;

		drmModeSetCursor2(backend_->drm_dev.fd(), crtc_id, 0, 0, 0, 0, 0);
	}
}

bool
GLES2Renderer::is_buffer_egl(struct wl_resource *buffer)
{
	EGLint format{};
	return display_.query_wayland_buffer_wl(display_.get(), buffer, EGL_TEXTURE_FORMAT, &format);
}

GLES2SurfaceRenderable::GLES2SurfaceRenderable(Surface *surface, GLES2Renderer *renderer)
    : SurfaceRenderable{surface}, renderer_{renderer}
{
	glGenTextures(1, &texture_);
}

GLES2SurfaceRenderable::~GLES2SurfaceRenderable() { glDeleteTextures(1, &texture_); }

void
GLES2SurfaceRenderable::render_finish(uint32_t msec_delta)
{
	is_rendering_ = false;
	SurfaceRenderable::render_finish(msec_delta);
}

static void
load_shm_image_to_texture(int32_t width, int32_t height, enum wl_shm_format format, void *data)
{
	if (!data) {
		return;
	}

	switch (format) {
	case WL_SHM_FORMAT_ARGB8888:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE,
			     data);
		break;
	case WL_SHM_FORMAT_XRGB8888:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE,
			     data);
		break;
	default:
		break;
	}
}

void
GLES2SurfaceRenderable::update()
{
	if (is_rendering_) {
		return;
	}

	std::sort(renderer_->backend_->compositor()->renderables_.begin(),
		  renderer_->backend_->compositor()->renderables_.end(), renderable_sort);

	if (is_cursor_) {
		renderer_->render_cursor_to_bo(this);
		return;
	}

	if (layer_ == Layer::Hidden) {
		return;
	}

	auto our_pos =
	    std::find_if(renderer_->backend_->compositor()->renderables_.begin(),
			 renderer_->backend_->compositor()->renderables_.end(), [this](const auto &a) {
				 auto a_ptr = a.lock();
				 if (!a_ptr) {
					 return false;
				 }

				 std::shared_ptr<GLES2SurfaceRenderable> check_a =
				     std::dynamic_pointer_cast<GLES2SurfaceRenderable>(a_ptr);

				 return check_a.get() == this;
			 });

	std::fprintf(stderr, "Use count: %zu\n", our_pos->use_count());

	// TODO: Deal with all possible buffer types
	if (auto *shm_buf = wl_shm_buffer_get(surface_->buffer())) {
		width_ = wl_shm_buffer_get_width(shm_buf);
		height_ = wl_shm_buffer_get_height(shm_buf);

		x_ = surface_->x();
		y_ = surface_->y();

		if (surface_->geometry_) {
			x_off_ = surface_->geometry_->x_;
			y_off_ = surface_->geometry_->y_;
		}

		enum wl_shm_format format =
		    static_cast<enum wl_shm_format>(wl_shm_buffer_get_format(shm_buf));

		is_opaque_ = format != WL_SHM_FORMAT_ARGB8888;

		glBindTexture(GL_TEXTURE_2D, texture_);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		wl_shm_buffer_begin_access(shm_buf);

		void *data = wl_shm_buffer_get_data(shm_buf);
		load_shm_image_to_texture(width_, height_, format, data);

		wl_shm_buffer_end_access(shm_buf);

		wl_buffer_send_release(surface_->buffer());
		is_rendering_ = true;
	} else if (renderer_->is_buffer_egl(surface_->buffer())) {
		struct wl_resource *buffer = surface_->buffer();

		EGLint plane_count{};
		EGLint format{};

		EGLint buffer_width{};
		EGLint buffer_height{};

		auto &display = renderer_->display_;

		display.query_wayland_buffer_wl(display.get(), buffer, EGL_TEXTURE_FORMAT, &format);

		switch (format) {
			// TODO: Deal with formats such as YUV
		case EGL_TEXTURE_Y_U_V_WL:
			plane_count = 3;
			break;
		case EGL_TEXTURE_Y_UV_WL:
		case EGL_TEXTURE_Y_XUXV_WL:
			plane_count = 2;
			break;
		case EGL_TEXTURE_EXTERNAL_WL:
		case EGL_TEXTURE_RGB:
		case EGL_TEXTURE_RGBA:
		default:
			plane_count = 1;
			break;
		}

		is_opaque_ = format != EGL_TEXTURE_RGBA;

		display.query_wayland_buffer_wl(display.get(), buffer, EGL_WIDTH, &buffer_width);

		display.query_wayland_buffer_wl(display.get(), buffer, EGL_HEIGHT, &buffer_height);

		width_ = buffer_width;
		height_ = buffer_height;

		x_ = surface_->x();
		y_ = surface_->y();

		if (surface_->geometry_) {
			x_off_ = surface_->geometry_->x_;
			y_off_ = surface_->geometry_->y_;
		}

		glBindTexture(GL_TEXTURE_2D, texture_);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// TODO: Deal with multi-planar formats properly
		std::vector<EGLImage> planes;
		for (EGLint i = 0; i < plane_count; ++i) {
			EGLint img_attrs[]{
			    EGL_WAYLAND_PLANE_WL,
			    i,

			    EGL_IMAGE_PRESERVED_KHR,
			    EGL_TRUE,

			    EGL_NONE,
			};

			EGLImage img = eglCreateImageKHR(display.get(), nullptr, EGL_WAYLAND_BUFFER_WL,
							 buffer, img_attrs);

			glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, img);

			eglDestroyImageKHR(display.get(), img);
		}

		wl_buffer_send_release(surface_->buffer());
		is_rendering_ = true;
	}

	for (auto &[output, _] : renderer_->output_info_) {
		renderer_->queue_render_full(output);
	}

	Region input_region{};
	for (auto it = our_pos + 1; it != renderer_->backend_->compositor()->renderables_.end(); ++it) {
		auto renderable = it->lock();
		if (!renderable) {
			break;
		}

		input_region += renderable->input_region();
	}

	calculate_input_region(input_region);

	for (auto it = std::reverse_iterator(our_pos);
	     it != renderer_->backend_->compositor()->renderables_.rend(); ++it) {
		auto r = it->lock();

		if (r->layer() == Layer::Hidden) {
			break;
		}

		auto renderable = std::dynamic_pointer_cast<GLES2Renderable>(r);

		renderable->calculate_input_region(input_region);
	}
}

void
GLES2SurfaceRenderable::calculate_input_region(Region &input_region)
{
	Region tmp{surface_->input_region()};
	Region clip{{0, 0, width_, height_}};

	input_region_ = tmp.intersect(clip);
	input_region_.translate(offset_x(), offset_y());

	input_region_ -= input_region;

	input_region += input_region_;
}

void
GLES2SurfaceRenderable::bind_texture_to_loc(GLint loc)
{
	glUniform1i(loc, 0);

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, texture_);
}

GLES2BitmapRenderable::GLES2BitmapRenderable(
    int32_t x, int32_t y, int32_t width, int32_t height, bool is_alpha)
    : BitmapRenderable{x, y, width, height, is_alpha}
{
	is_opaque_ = !has_alpha_;

	glGenTextures(1, &texture_);
}

GLES2BitmapRenderable::~GLES2BitmapRenderable() { glDeleteTextures(1, &texture_); }

void
GLES2BitmapRenderable::render_finish(uint32_t msec_delta)
{
}

void
GLES2BitmapRenderable::update()
{
	std::sort(renderer_->backend_->compositor()->renderables_.begin(),
		  renderer_->backend_->compositor()->renderables_.end(), renderable_sort);

	if (is_cursor_) {
		renderer_->render_cursor_to_bo(this);
		return;
	}

	if (layer_ == Layer::Hidden) {
		return;
	}

	auto our_pos =
	    std::find_if(renderer_->backend_->compositor()->renderables_.begin(),
			 renderer_->backend_->compositor()->renderables_.end(), [this](const auto &a) {
				 auto a_ptr = a.lock();
				 if (!a_ptr) {
					 return false;
				 }

				 std::shared_ptr<GLES2BitmapRenderable> check_a =
				     std::dynamic_pointer_cast<GLES2BitmapRenderable>(a_ptr);

				 return check_a.get() == this;
			 });

	glBindTexture(GL_TEXTURE_2D, texture_);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width_, height_, 0, GL_BGRA, GL_UNSIGNED_BYTE,
		     buffer_.data());

	for (auto &[output, _] : renderer_->output_info_) {
		renderer_->queue_render_full(output);
	}

	Region input_region{};
	for (auto it = our_pos + 1; it != renderer_->backend_->compositor()->renderables_.end(); ++it) {
		auto renderable = it->lock();
		if (!renderable) {
			break;
		}

		input_region += renderable->input_region();
	}

	calculate_input_region(input_region);

	for (auto it = std::reverse_iterator(our_pos);
	     it != renderer_->backend_->compositor()->renderables_.rend(); ++it) {
		auto r = it->lock();

		if (r->layer() == Layer::Hidden) {
			break;
		}

		auto renderable = std::dynamic_pointer_cast<GLES2Renderable>(r);

		renderable->calculate_input_region(input_region);
	}
}

void
GLES2BitmapRenderable::bind_texture_to_loc(GLint loc)
{
	// TODO: Implement
}

void
GLES2BitmapRenderable::calculate_input_region(Region &input_region)
{
	// TODO: Make bitmap renderables interactable
}

} // namespace drm

} // namespace orbycomp
