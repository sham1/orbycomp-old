/*
 * renderer.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gbm.h>
#include <orbycomp/base/surface.hh>
#include <orbycomp/renderer-loader.hh>
#include <system_error>
#include <xf86drm.h>

namespace orbycomp
{

namespace drm
{

class Backend;
class Output;

class GBMDevice
{
	struct gbm_device *device_;

public:
	inline struct gbm_device *
	get()
	{
		return device_;
	}

	GBMDevice(int fd) : device_{gbm_create_device(fd)}
	{
		if (!device_) {
			throw std::runtime_error("Could not setup GBM");
		}
	}

	~GBMDevice()
	{
		if (device_) {
			gbm_device_destroy(device_);
			device_ = nullptr;
		}
	}

	GBMDevice(const GBMDevice &) = delete;
	GBMDevice &operator=(const GBMDevice &) = delete;

	GBMDevice(GBMDevice &&other) : device_{std::move(other.device_)} { other.device_ = nullptr; }

	GBMDevice &
	operator=(GBMDevice &&other)
	{
		device_ = std::move(other.device_);
		other.device_ = nullptr;

		return *this;
	}
};

class SurfaceFB
{
	struct gbm_bo *bo_;
	uint32_t fb_{0};

	uint32_t create_fb();

public:
	SurfaceFB(struct gbm_bo *bo);
	~SurfaceFB();

	static void on_fb_destroy_cb(struct gbm_bo *bo, void *data);

	static SurfaceFB *get_from_bo(struct gbm_bo *bo);

	uint32_t
	fb() const
	{
		return fb_;
	}
};

class Renderer
{
protected:
	Backend *backend_;
	RendererHandle renderer_{};

	Renderer(Backend *backend, RendererHandle &&renderer)
	    : backend_{backend}, renderer_{std::move(renderer)}
	{
	}

public:
	virtual ~Renderer() {}

	static std::unique_ptr<Renderer> choose_renderer(Backend *backend);

	orbycomp::Renderer *
	get() const
	{
		return renderer_.get();
	}

	virtual void setup_output(Output *output) = 0;
	virtual SurfaceFB *render_output(Output *output) = 0;
};

class GBMDumbMap
{
	struct gbm_bo *bo_;
	void *map_;
	uint32_t stride_;

public:
	inline void *
	map()
	{
		return map_;
	}

	inline size_t
	stride()
	{
		return stride_;
	}

	inline gbm_bo *
	bo() const
	{
		return bo_;
	}

	GBMDumbMap(struct gbm_bo *bo, int32_t x, int32_t y, int32_t width, int32_t height, uint32_t flags)
	    : bo_{bo}
	{
		if (!gbm_bo_map(bo_, x, y, width, height, flags, &stride_, &map_)) {
			throw std::system_error(errno, std::generic_category());
		}
	}

	~GBMDumbMap() { gbm_bo_unmap(bo_, map_); }
};

} // namespace drm

} // namespace orbycomp
