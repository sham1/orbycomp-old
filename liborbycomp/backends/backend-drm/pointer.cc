/*
 * pointer.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pointer.hh"
#include "backend.hh"
#include <cstdio>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/seat.hh>

namespace orbycomp
{

namespace drm
{

void
Pointer::dispatch(struct libinput_event *event)
{
	enum libinput_event_type type = libinput_event_get_type(event);

	switch (type) {
	case LIBINPUT_EVENT_POINTER_MOTION_ABSOLUTE:
	case LIBINPUT_EVENT_POINTER_MOTION:
	case LIBINPUT_EVENT_POINTER_BUTTON:
	case LIBINPUT_EVENT_POINTER_AXIS:
		break;
	default:
		return;
	}

	struct libinput_event_pointer *pointer_event = libinput_event_get_pointer_event(event);

	uint64_t tv_usec = libinput_event_pointer_get_time_usec(pointer_event);

	auto &pointer = seat_->pointer().value();

	if (type == LIBINPUT_EVENT_POINTER_MOTION_ABSOLUTE) {
		// TODO: Handle
	}

	if (type == LIBINPUT_EVENT_POINTER_MOTION) {
		double dx = libinput_event_pointer_get_dx(pointer_event);
		double dy = libinput_event_pointer_get_dy(pointer_event);
		double dx_unaccel = libinput_event_pointer_get_dx_unaccelerated(pointer_event);
		double dy_unaccel = libinput_event_pointer_get_dy_unaccelerated(pointer_event);

		pointer.move(tv_usec, dx, dy, dx_unaccel, dy_unaccel);
	}

	if (type == LIBINPUT_EVENT_POINTER_BUTTON) {
		uint32_t button = libinput_event_pointer_get_button(pointer_event);
		enum libinput_button_state state = libinput_event_pointer_get_button_state(pointer_event);

		orbycomp::Pointer::ButtonState btn_state;

		if (state == LIBINPUT_BUTTON_STATE_PRESSED) {
			btn_state = orbycomp::Pointer::ButtonState::Press;
		}
		if (state == LIBINPUT_BUTTON_STATE_RELEASED) {
			btn_state = orbycomp::Pointer::ButtonState::Release;
		}

		pointer.feed_button_event(tv_usec, button, btn_state);
	}

	if (type == LIBINPUT_EVENT_POINTER_AXIS) {
		// TODO: Handle
	}
}

} // namespace drm

} // namespace orbycomp
