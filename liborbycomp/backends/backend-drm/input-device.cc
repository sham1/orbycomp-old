/*
 * input-device.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "input-device.hh"
#include "backend.hh"
#include "keyboard.hh"
#include "pointer.hh"
#include <orbycomp/base/compositor.hh>
#include <string>

namespace orbycomp
{

namespace drm
{

void
InputDevice::create_device(Backend *backend, struct libinput_device *device)
{
	if (libinput_device_has_capability(device, LIBINPUT_DEVICE_CAP_KEYBOARD)) {
		if (InputDevice *dev = static_cast<InputDevice *>(libinput_device_get_user_data(device))) {
			delete dev;
		}

		Keyboard *kb = new Keyboard(backend, device);
		libinput_device_set_user_data(device, kb);
	}

	if (libinput_device_has_capability(device, LIBINPUT_DEVICE_CAP_POINTER)) {
		if (InputDevice *dev = static_cast<InputDevice *>(libinput_device_get_user_data(device))) {
			delete dev;
		}

		Pointer *ptr = new Pointer(backend, device);
		libinput_device_set_user_data(device, ptr);
	}
}

InputDevice::InputDevice(Backend *backend, struct libinput_device *device)
    : backend_{backend}, device_{libinput_device_ref(device)}
{
	auto seat = libinput_device_get_seat(device);
	std::string seat_name{libinput_seat_get_logical_name(seat)};
	seat_ = backend_->compositor()->get_seat(seat_name);
}

} // namespace drm

} // namespace orbycomp
