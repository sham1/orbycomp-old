/*
 * software-renderer.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "backend.hh"
#include "renderer.hh"

#include <orbycomp/render/software-rendering.hh>

namespace orbycomp
{

namespace drm
{

class OutputInfo;

class SoftwareRenderer : public Renderer
{
	friend OutputInfo;

	static RendererHandle load_renderer(Backend *backend);

	std::unordered_map<Output *, std::unique_ptr<OutputInfo>> outputs_;

	GBMDevice device_;

public:
	SoftwareRenderer(Backend *backend);

	void setup_output(Output *output) override;

	SurfaceFB *render_output(Output *output) override;
};

class OutputInfo
{
	class GBMBo
	{
		struct gbm_bo *bo_;

	public:
		GBMBo(GBMDevice &dev, uint32_t width, uint32_t height, uint32_t format, uint32_t flags)
		    : bo_{gbm_bo_create(dev.get(), width, height, format, flags)}
		{
			if (!bo_) {
				throw std::system_error(errno, std::generic_category(),
							"Could not create buffer object");
			}
		}

		~GBMBo()
		{
			if (bo_) {
				gbm_bo_destroy(bo_);
				bo_ = nullptr;
			}
		}

		GBMBo(const GBMBo &other) = delete;
		GBMBo &operator=(const GBMBo &other) = delete;

		GBMBo(GBMBo &&other) : bo_{std::move(other.bo_)} { other.bo_ = nullptr; }
		GBMBo &
		operator=(GBMBo &&other)
		{
			bo_ = std::move(other.bo_);
			other.bo_ = nullptr;

			return *this;
		}

		struct gbm_bo *
		get() const
		{
			return bo_;
		}
	};

	class BoBuffer
	{
		GBMBo *bo_;

		uint32_t width_;
		uint32_t height_;

		uint32_t stride_{0};
		void *data_{nullptr};

	public:
		BoBuffer(GBMBo &bo, uint32_t x, uint32_t y, uint32_t width, uint32_t height, uint32_t flags)
		    : bo_{&bo}, width_{width}, height_{height}
		{
			auto *tmp{gbm_bo_map(bo_->get(), x, y, width_, height_, flags, &stride_, &data_)};
			if (!tmp) {
				throw std::system_error(errno, std::generic_category(), "Could not map BO");
			}
		}

		~BoBuffer()
		{
			if (data_) {
				gbm_bo_unmap(bo_->get(), data_);

				data_ = nullptr;
				stride_ = 0;
				bo_ = nullptr;
			}
		}

		BoBuffer(const BoBuffer &other) = delete;
		BoBuffer &operator=(const BoBuffer &other) = delete;

		BoBuffer(BoBuffer &&other)
		    : bo_{std::move(other.bo_)}, width_{std::move(other.width_)},
		      height_{std::move(other.height_)}, stride_{std::move(other.stride_)}, data_{std::move(
												other.data_)}
		{
			other.bo_ = nullptr;
			other.width_ = 0;
			other.height_ = 0;
			other.stride_ = 0;
			other.data_ = nullptr;
		}
		BoBuffer &
		operator=(BoBuffer &&other)
		{
			bo_ = std::move(other.bo_);
			width_ = std::move(other.width_);
			height_ = std::move(other.height_);
			stride_ = std::move(other.stride_);
			data_ = std::move(other.data_);

			other.bo_ = nullptr;
			other.width_ = 0;
			other.height_ = 0;
			other.stride_ = 0;
			other.data_ = nullptr;

			return *this;
		}

		uint32_t
		width() const
		{
			return width_;
		}

		uint32_t
		height() const
		{
			return height_;
		}

		uint32_t
		stride() const
		{
			return stride_;
		}

		uint8_t *
		data() const
		{
			return static_cast<uint8_t *>(data_);
		}
	};

	SoftwareRenderer *renderer_;

	Output *output_;

	std::array<GBMBo, 2> bos_;
	std::array<BoBuffer, 2> buffers_;
	size_t active_bo_{0};

	std::array<GBMBo, 2> create_bos();
	std::array<BoBuffer, 2> create_buffers();

public:
	OutputInfo(SoftwareRenderer *renderer, Output *output);
	~OutputInfo();

	rendering::software::BitmapSurface get_framebuffer();

	SurfaceFB *render();
};

}; // namespace drm

}; // namespace orbycomp
