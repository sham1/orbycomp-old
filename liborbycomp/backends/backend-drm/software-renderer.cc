/*
 * software-renderer.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "software-renderer.hh"
#include <orbycomp/renderers/software-renderer.hh>

namespace orbycomp
{

namespace drm
{

SoftwareRenderer::SoftwareRenderer(Backend *backend)
    : Renderer{backend, load_renderer(backend)}, device_{backend->drm_dev.fd_}
{
}

RendererHandle
SoftwareRenderer::load_renderer(Backend *backend)
{
	orbycomp::renderers::SoftwareRendererOptions opts{};
	opts.compositor = backend->compositor();

	return orbycomp::load_renderer("liborbycomp-software-renderer.so", opts);
}

void
SoftwareRenderer::setup_output(Output *output)
{
	auto [info, _] = outputs_.try_emplace(output, std::make_unique<OutputInfo>(this, output));
	auto *output_info = info->second.get();

	orbycomp::renderers::SoftwareRendererOutputOptions opts{};
	opts.get_fb = [output_info]() { return output_info->get_framebuffer(); };

	renderer_->setup_output(output, opts);
}

SurfaceFB *
SoftwareRenderer::render_output(Output *output)
{
	auto *info = outputs_[output].get();
	return info->render();
}

OutputInfo::OutputInfo(SoftwareRenderer *renderer, Output *output)
    : renderer_{renderer}, output_{output}, bos_{create_bos()}, buffers_{create_buffers()}
{
}

std::array<OutputInfo::GBMBo, 2>
OutputInfo::create_bos()
{
	uint32_t width = output_->current_mode().width_;
	uint32_t height = output_->current_mode().height_;

	uint32_t format{GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING | GBM_BO_USE_WRITE};

	GBMBo bo1{renderer_->device_, width, height, GBM_BO_FORMAT_XRGB8888, format};
	GBMBo bo2{renderer_->device_, width, height, GBM_BO_FORMAT_XRGB8888, format};

	return {std::move(bo1), std::move(bo2)};
}

std::array<OutputInfo::BoBuffer, 2>
OutputInfo::create_buffers()
{
	uint32_t width = output_->current_mode().width_;
	uint32_t height = output_->current_mode().height_;

	BoBuffer buffer1{bos_[0], 0, 0, width, height, GBM_BO_TRANSFER_READ_WRITE};
	BoBuffer buffer2{bos_[1], 0, 0, width, height, GBM_BO_TRANSFER_READ_WRITE};

	return {std::move(buffer1), std::move(buffer2)};
}

OutputInfo::~OutputInfo() {}

rendering::software::BitmapSurface
OutputInfo::get_framebuffer()
{
	auto &buffer{buffers_[active_bo_]};

	return {buffer.data(), buffer.stride(), int32_t(buffer.width()), int32_t(buffer.height())};
}

SurfaceFB *
OutputInfo::render()
{
	renderer_->renderer_->render_output(output_);

	auto *ret = SurfaceFB::get_from_bo(bos_[active_bo_].get());
	active_bo_ = (active_bo_ + 1) % bos_.size();

	return ret;
}

}; // namespace drm

}; // namespace orbycomp
