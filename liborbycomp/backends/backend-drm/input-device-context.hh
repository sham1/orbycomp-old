/*
 * input-device-context.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <ev.h>
#include <libinput.h>
#include <memory>

namespace orbycomp
{

namespace drm
{

class Backend;

class InputDeviceCtx
{
	Backend *backend_;

	struct libinput_context_closer {
		void
		operator()(struct libinput *libinput) const
		{
			libinput_unref(libinput);
		}
	};

	std::unique_ptr<struct libinput, libinput_context_closer> ctx_;

	struct ev_io libinput_dispatch {
	};

	static void on_libinput_event_cb(EV_P_ struct ev_io *w, int revents);

	void dispatch_events();
	void on_compositor_activity_change(bool active);

public:
	InputDeviceCtx(Backend *backend);
	~InputDeviceCtx();

	inline struct libinput *
	get()
	{
		return ctx_.get();
	}

	static int open_restricted(const char *path, int flags, void *user_data);

	static void close_restricted(int fd, void *user_data);
};

} // namespace drm

} // namespace orbycomp
