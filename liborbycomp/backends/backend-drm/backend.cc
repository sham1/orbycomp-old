/*
 * backend.cc
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "backend.hh"
#include <cstdio>
#include <fcntl.h>
#include <orbycomp/base/compositor.hh>
#include <stdexcept>
#include <sys/stat.h>
#include <unistd.h>

namespace orbycomp
{

namespace drm
{

// XXX: "seat0" is used as the "default" seat name in prior art,
// particularly in Weston.
//
// Assumption is that this is sane.
static std::string
get_seat()
{
	const char *xdg_seat = std::getenv("XDG_SEAT");
	if (xdg_seat != nullptr) {
		return xdg_seat;
	} else {
		return "seat0";
	}
}

extern "C" orbycomp::BackendBase *
backend_init(orbycomp::Compositor *compositor)
{
	return new orbycomp::drm::Backend(compositor);
}

Backend::Backend(Compositor *compositor)
    : BackendBase(compositor), seat_name_{get_seat()}, session_manager{SessionManager::load(this)},
      drm_dev{this}, input_ctx{this}, renderer_{Renderer::choose_renderer(this)}
{
	// Register keybinds for switching virtual terminals.
	for (size_t i = 0; i < 12; ++i) {
		xkb_keysym_t switch_sym = XKB_KEY_XF86Switch_VT_1 + i;
		Keybind &bind = vty_switch_binds.emplace_back(
		    Modifier::Control | Modifier::Alt, switch_sym,
		    [this](Modifier mods, xkb_keysym_t sym) { this->switch_vty_cb(mods, sym); });
		compositor_->keybind_ctx().get_global().add_bind(bind);
	}

	ev_io_init(&drm_event_handler_, Backend::on_drm_event_cb, drm_dev.fd_, EV_READ | EV_WRITE);
	drm_event_handler_.data = this;
	ev_io_start(compositor_->loop(), &drm_event_handler_);

	for (auto output : outputs_) {
		renderer_->setup_output(output);
	}

	modeset_outputs();

	event_ctx_.version = 2;
	event_ctx_.page_flip_handler = Backend::on_page_flip;

	on_compositor_active_connection_ = compositor_->activity_change_signal.connect([this](bool active) {
		if (active) {
			this->modeset_outputs();
		}
	});
}

void
Backend::modeset_outputs()
{
	for (auto output : outputs_) {
		SurfaceFB *fb = renderer_->render_output(output);
		output->modeset(fb->fb());
	}
}

Backend::~Backend()
{
	if (ev_is_active(&drm_event_handler_)) {
		ev_io_stop(compositor_->loop(), &drm_event_handler_);
	}

	for (auto *output : compositor_->outputs_) {
		compositor_->on_output_destroy_signal(output);
		delete output;
	}
}

void
Backend::on_drm_event_cb(EV_P_ ev_io *w, int revents)
{
	Backend *self = static_cast<Backend *>(w->data);
	drmHandleEvent(w->fd, &self->event_ctx_);
};

void
Backend::on_page_flip(
    int fd, unsigned int sequence, unsigned int tv_sec, unsigned int tv_usec, void *user_data)
{
	// TODO: Do properly

	uint32_t tv_msec = tv_sec * 1000;
	tv_msec += tv_usec / 1000;

	Backend *self = static_cast<Backend *>(user_data);
	for (auto *r : self->compositor_->renderables_) {
		if (r->layer() == Renderable::Layer::Hidden) {
			continue;
		}
		r->render_finish(tv_msec);
	}
}

Backend::DrmDevice::DrmDevice(Backend *backend) : backend_{backend}
{
	// TODO: Be actually intelligent about what primary node
	// is selected. Maybe use udev or equivalent to query what
	// device should be used. (Maybe allow the user to specify?)
	//
	// This expands to /dev/dri/card0 on Linux.
	fd_ = backend_->session_manager->open(DRM_DIR_NAME "/" DRM_PRIMARY_MINOR_NAME "0", O_CLOEXEC);

	{
		struct stat st;
		int ret = fstat(fd_, &st);
		if (ret < 0) {
			throw std::runtime_error("Couldn't access stat of device node");
		}

		major_ = ::major(st.st_rdev);
		minor_ = ::minor(st.st_rdev);
	}

	if (!drmIsMaster(fd_)) {
		if (drmSetMaster(fd_) < 0) {
			throw std::runtime_error("Couldn't set DRM master");
		}
	}

	res_ = {drmModeGetResources(fd_), drmModeFreeResources};
	if (!res_) {
		throw std::system_error(errno, std::generic_category(), "Couldn't get DRM device resources");
	}

	// Let's go through each connector, and see if it's connected.
	// If it is, we can make that into an `Output`.

	for (int i = 0; i < res_->count_connectors; ++i) {
		uint32_t connector_id = res_->connectors[i];
		ManagedDrmModeConnector connector{drmModeGetConnector(fd_, connector_id),
						  drmModeFreeConnector};
		if (!connector) {
			continue;
		}

		if (connector->connection == DRM_MODE_CONNECTED && connector->count_modes > 0) {
			create_output(std::move(connector));
		}
	}

	active_ = true;
}

Backend::DrmDevice::~DrmDevice()
{
	if (!active_) {
		return;
	}

	if (drmIsMaster(fd_)) {
		drmDropMaster(fd_);
	}

	backend_->session_manager->close(fd_);
}

void
Backend::DrmDevice::create_output(ManagedDrmModeConnector &&connector)
{
	ManagedDrmModeConnector conn{std::move(connector)};
	std::vector<orbycomp::Output::Mode> modes;
	int current_mode = -1;
	bool has_current_mode{false};
	for (int i = 0; i < conn->count_modes; ++i) {
		drmModeModeInfo mode_info = conn->modes[i];

		enum wl_output_mode flags = static_cast<wl_output_mode>(0);
		bool is_preferred = (mode_info.flags & DRM_MODE_TYPE_PREFERRED) != 0;

		// If this current mode is preferred, we will also use it
		// by default.
		//
		// TODO: if there are multiple "preferred modes", take the
		// "largest" one.
		if (is_preferred) {
			flags = static_cast<wl_output_mode>(static_cast<int>(flags) |
							    static_cast<int>(WL_OUTPUT_MODE_PREFERRED));

			if (!has_current_mode) {
				current_mode = i;
				flags = static_cast<wl_output_mode>(static_cast<int>(flags) |
								    static_cast<int>(WL_OUTPUT_MODE_CURRENT));
				has_current_mode = true;
			}
		}

		modes.emplace_back(mode_info.hdisplay, mode_info.vdisplay, mode_info.vrefresh * 1000, flags);
	}

	// If no preferred mode was found, set the current mode to the last one.
	if (current_mode == -1) {
		current_mode = conn->count_modes - 1;
		modes[current_mode].flags_ = static_cast<wl_output_mode>(
		    static_cast<int>(modes[current_mode].flags_) | static_cast<int>(WL_OUTPUT_MODE_CURRENT));
	}

	// TODO: Use EDID to figure out maker and model
	std::string make{"Sham1"};
	std::string model{"Output"};

	// TODO: Figure out proper X and Y for the output
	auto *output_ptr = new Output(backend_, std::move(conn), 0, 0, std::move(modes), 1,
				      WL_OUTPUT_TRANSFORM_NORMAL, make, model, current_mode);

	backend_->outputs_.push_back(output_ptr);
	backend_->compositor()->outputs_.emplace_back(output_ptr);
	backend_->compositor()->on_output_added_signal(output_ptr);
}

void
Backend::switch_vty_cb(Modifier mods, xkb_keysym_t sym)
{
	if (sym >= XKB_KEY_XF86Switch_VT_1 && sym <= XKB_KEY_XF86Switch_VT_12) {
		int vtnum = sym - XKB_KEY_XF86Switch_VT_1 + 1;
		session_manager->switch_vty(vtnum);
	}
}

void
Backend::on_device_pause(unsigned int major, unsigned int minor)
{
	// Let's check if the relevant device is our DRM node.
	// If it is, we have to notify the compositor about the display being
	// gone.
	if (major == drm_dev.major_ && minor == drm_dev.minor_) {
		drm_dev.active_ = false;

		if (ev_is_active(&drm_event_handler_)) {
			ev_io_stop(compositor_->loop(), &drm_event_handler_);
		}

		compositor_->set_active(false);
	}
}

void
Backend::on_device_resume(unsigned int major, unsigned int minor, int new_fd)

{
	// Let's check if the relevant device is our DRM node.
	// If it is, we have to notify the compositor about the display being
	// back.
	if (major == drm_dev.major_ && minor == drm_dev.minor_) {
		drm_dev.active_ = true;
		drm_dev.fd_ = new_fd;

		if (!drmIsMaster(drm_dev.fd_)) {
			drmSetMaster(drm_dev.fd_);
		}

		if (!ev_is_active(&drm_event_handler_)) {
			ev_io_set(&drm_event_handler_, drm_dev.fd_, EV_READ | EV_WRITE);
			ev_io_start(compositor_->loop(), &drm_event_handler_);
		}

		compositor_->set_active(true);
	}
}

orbycomp::Renderer *
Backend::get_renderer()
{
	return renderer_->get();
}

void
Backend::repaint_output(orbycomp::Output *output)
{
	Output *op;

	auto op_pos =
	    std::find_if(outputs_.begin(), outputs_.end(), [output](auto op) { return op == output; });
	if (op_pos == outputs_.end()) {
		// Shouldn't happen.
		return;
	}

	op = *op_pos;
	SurfaceFB *fb = renderer_->render_output(op);

	op->pageflip(fb->fb());
}

Output::Output(Backend *backend,
	       ManagedDrmModeConnector &&connector,
	       int32_t x,
	       int32_t y,
	       std::vector<orbycomp::Output::Mode> &&modes,
	       int32_t scale,
	       enum wl_output_transform transform,
	       const std::string &make,
	       const std::string &model,
	       int current_mode)
    : orbycomp::Output(backend->compositor(),
		       x,
		       y,
		       std::move(modes),
		       connector->mmWidth,
		       connector->mmHeight,
		       drm_subpixel_to_wl_subpixel(connector->subpixel),
		       scale,
		       transform,
		       make,
		       model),
      backend_{backend}, connector_{std::move(connector)},
      current_mode_{current_mode}, encoder_{find_encoder()}, original_crtc_info_{get_original_crtc_info()}
{
}

ManagedDrmEncoder
Output::find_encoder()
{
	drmModeResPtr res = backend_->drm_dev.res_.get();
	for (int i = 0; i < res->count_encoders; ++i) {
		ManagedDrmEncoder enc{drmModeGetEncoder(backend_->drm_dev.fd_, res->encoders[i]),
				      drmModeFreeEncoder};
		if (!enc) {
			continue;
		}

		if (enc->encoder_id == connector_->encoder_id) {
			return ManagedDrmEncoder{std::move(enc)};
		}
	}

	throw std::runtime_error("Could not find encoder for output");
}

ManagedDrmCrtcInfo
Output::get_original_crtc_info()
{
	return ManagedDrmCrtcInfo{drmModeGetCrtc(backend_->drm_dev.fd_, encoder_->crtc_id), drmModeFreeCrtc};
}

void
Output::modeset(uint32_t fb)
{
	int fd = backend_->drm_dev.fd_;
	drmModeSetCrtc(fd, encoder_->crtc_id, fb, 0, 0, &connector_->connector_id, 1,
		       &connector_->modes[current_mode_]);
}

void
Output::pageflip(uint32_t fb)
{
	int fd = backend_->drm_dev.fd_;

	drmModePageFlip(fd, encoder_->crtc_id, fb, DRM_MODE_PAGE_FLIP_EVENT, backend_);
}

Output::~Output()
{
	if (original_crtc_info_) {
		drmModeSetCrtc(backend_->drm_dev.fd_, original_crtc_info_->crtc_id,
			       original_crtc_info_->buffer_id, original_crtc_info_->x, original_crtc_info_->y,
			       &connector_->connector_id, 1, &original_crtc_info_->mode);
		original_crtc_info_.reset();
	}
}

} // namespace drm

} // namespace orbycomp
