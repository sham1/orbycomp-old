/*
 * session-manager-logind.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "session-manager.hh"
#include <dbus/dbus.h>
#include <orbycomp/ev-dbus.hh>
#include <string>

// TODO: get dev_t and such in a more cross-platform manner
#include <sys/sysmacros.h>

namespace orbycomp
{

namespace drm
{

class SessionManagerLogind : public SessionManager
{
private:
	static void check_logind_presence(DBusConnection *conn);

	void take_over_session();
	void activate_session();

	std::string session_name_{};
	std::string session_path{};
	std::string seat_path{};

	std::pair<int, bool> take_device(dev_t &dev);
	void release_device(dev_t &dev);

	static DBusHandlerResult handle_messages_cb(DBusConnection *conn, DBusMessage *msg, void *user_data);

	dbus::Context::SignalMatcher session_removed_matcher;
	dbus::Context::SignalMatcher pause_device_matcher;
	dbus::Context::SignalMatcher resume_device_matcher;
	dbus::Context::SignalMatcher props_changed_matcher;

	void on_session_remove(DBusMessage *msg);
	void on_pause_device(DBusMessage *msg);
	void on_resume_device(DBusMessage *msg);

public:
	~SessionManagerLogind() {}

	SessionManagerLogind(Backend *backend);

	int open(const char *path, int flags) override;

	void close(int fd) override;

	void switch_vty(unsigned int vtynum) noexcept override;
};

} // namespace drm

} // namespace orbycomp
