/*
 * session-manager.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>

namespace orbycomp
{

namespace drm
{

class Backend;

class SessionManager
{
protected:
	Backend *backend_;

	SessionManager(Backend *backend) : backend_{backend} {}

public:
	virtual ~SessionManager() {}

	static std::unique_ptr<SessionManager> load(Backend *backend);

	// Opens a device at a given `path` with given `flags`.
	//
	// Throws std::system_error with appropriate errno-compatible
	// number.
	//
	// May also throw std::bad_alloc.
	virtual int open(const char *path, int flags) = 0;

	// Closes a device at `fd`.
	virtual void close(int fd) = 0;

	// Switches the virtual terminal to the specified terminal.
	virtual void switch_vty(unsigned int vtynum) noexcept = 0;
};

} // namespace drm

} // namespace orbycomp
