/*
 * session-manager-logind.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "session-manager-logind.hh"
#include "backend.hh"
#include <errno.h>
#include <fcntl.h>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/ev-dbus.hh>
#include <stdexcept>
#include <sys/stat.h>
#include <system_error>
#include <systemd/sd-login.h>
#include <unistd.h>

static const char *logind_bus_name = "org.freedesktop.login1";
static const char *logind_manager_path = "/org/freedesktop/login1";
static const char *logind_manager_iface = "org.freedesktop.login1.Manager";
static const char *logind_seat_path_prefix = "/org/freedesktop/login1/seat/";
static const char *logind_seat_iface = "org.freedesktop.login1.Seat";
static const char *logind_session_path_prefix = "/org/freedesktop/login1/session/";
static const char *logind_session_iface = "org.freedesktop.login1.Session";

namespace orbycomp
{

namespace drm
{

SessionManagerLogind::SessionManagerLogind(Backend *backend)
    : SessionManager(backend), session_path{logind_session_path_prefix}, seat_path{logind_seat_path_prefix}
{
	// logind resides in the system bus.
	dbus::Context &context = backend_->compositor()->system_context;
	DBusConnection *conn = context.get_connection();

	// First let's check whether there is logind enabled on the system
	// to begin with. I.e. whether it is running.
	//
	// If this doesn't throw for whatever reason, we know that
	// the logind manager object exists.
	check_logind_presence(conn);

	// Now we need to get our seat path, and our session name
	char *session_name{};
	int ret = sd_pid_get_session(getpid(), &session_name);
	if (ret < 0) {
		throw std::system_error(-ret, std::generic_category(), "Couldn't find systemd session");
	}
	session_name_ = session_name;
	session_path += session_name;
	std::free(session_name);

	seat_path += backend_->seat_name();

	// Take control of the session
	take_over_session();

	// Set the session to be active
	activate_session();

	// Set up signal listeners
	session_removed_matcher = context.add_signal_matcher(logind_bus_name, logind_manager_iface,
							     "SessionRemoved", logind_manager_path);

	pause_device_matcher =
	    context.add_signal_matcher(logind_bus_name, logind_session_iface, "PauseDevice", session_path);

	resume_device_matcher =
	    context.add_signal_matcher(logind_bus_name, logind_session_iface, "ResumeDevice", session_path);

	props_changed_matcher = context.add_signal_matcher(logind_bus_name, DBUS_INTERFACE_PROPERTIES,
							   "PropertiesChanged", session_path);

	if (!dbus_connection_add_filter(conn, SessionManagerLogind::handle_messages_cb, this, nullptr)) {
		throw std::runtime_error("Couldn't set up logind listener");
	}
}

void
SessionManagerLogind::check_logind_presence(DBusConnection *conn)
{
	dbus::Error error;

	dbus::Message ping_msg{dbus_message_new_method_call(logind_bus_name, logind_manager_path,
							    "org.freedesktop.DBus.Peer", "Ping")};
	if (!ping_msg) {
		throw std::bad_alloc();
	}

	// If the manager object doesn't exist currently, just don't
	// bother trying to bring it up.
	dbus_message_set_auto_start(ping_msg.get(), FALSE);

	// Ping takes no arguments, so we can just send it and block.
	dbus::Message pong_msg{dbus_connection_send_with_reply_and_block(conn, ping_msg.get(),
									 DBUS_TIMEOUT_USE_DEFAULT, &error)};
	if (error) {
		throw std::runtime_error(error.message());
	}
}

int
SessionManagerLogind::open(const char *path, int flags)
{
	struct stat st {
	};
	int ret;

	ret = stat(path, &st);
	if (ret < 0) {
		throw std::system_error(errno, std::generic_category());
	}

	if (!S_ISCHR(st.st_mode)) {
		throw std::system_error(ENODEV, std::generic_category());
	}

	auto [fd, paused] = take_device(st.st_rdev);

	int fd_flags = fcntl(fd, F_GETFL);
	if (fd_flags < 0) {
		throw std::system_error(errno, std::generic_category());
	}

	// logind gives us nice defaults, however if we need other
	// things like O_NONBLOCK, we need to add them here.
	if (flags & O_NONBLOCK) {
		fd_flags |= O_NONBLOCK;
	}

	ret = fcntl(fd, F_SETFL, fd_flags);
	if (ret < 0) {
		throw std::system_error(errno, std::generic_category());
	}

	return fd;
}

std::pair<int, bool>
SessionManagerLogind::take_device(dev_t &dev)
{
	int fd;
	dbus_bool_t paused;

	dbus::Message msg{dbus_message_new_method_call(logind_bus_name, session_path.c_str(),
						       logind_session_iface, "TakeDevice")};
	if (!msg) {
		throw std::bad_alloc();
	}

	unsigned int dev_major = major(dev);
	unsigned int dev_minor = minor(dev);

	if (!dbus_message_append_args(msg.get(), DBUS_TYPE_UINT32, &dev_major, DBUS_TYPE_UINT32, &dev_minor,
				      DBUS_TYPE_INVALID)) {
		throw std::bad_alloc();
	}

	dbus::Error error;

	dbus::Message reply{dbus_connection_send_with_reply_and_block(
	    backend_->compositor()->system_context.get_connection(), msg.get(), -1, &error)};

	if (!reply) {
		throw std::bad_alloc();
	}

	if (!dbus_message_get_args(reply.get(), nullptr, DBUS_TYPE_UNIX_FD, &fd, DBUS_TYPE_BOOLEAN, &paused,
				   DBUS_TYPE_INVALID)) {
		throw std::system_error(ENODEV, std::generic_category());
	}

	return {fd, paused};
}

void
SessionManagerLogind::close(int fd)
{
	struct stat st {
	};

	int ret;
	ret = fstat(fd, &st);
	::close(fd);

	if (ret < 0) {
		return;
	}

	if (!S_ISCHR(st.st_mode)) {
		return;
	}

	release_device(st.st_rdev);
}

void
SessionManagerLogind::release_device(dev_t &dev)
{
	dbus::Message msg{dbus_message_new_method_call(logind_bus_name, session_path.c_str(),
						       logind_session_iface, "ReleaseDevice")};

	if (!msg) {
		return;
	}

	unsigned int dev_major = major(dev);
	unsigned int dev_minor = minor(dev);
	if (!dbus_message_append_args(msg.get(), DBUS_TYPE_UINT32, &dev_major, DBUS_TYPE_UINT32, &dev_minor,
				      DBUS_TYPE_INVALID)) {
		return;
	}

	dbus_connection_send(backend_->compositor()->system_context.get_connection(), msg.get(), nullptr);
}

void
SessionManagerLogind::take_over_session()
{
	dbus::Error err;
	dbus::Message msg{dbus_message_new_method_call(logind_bus_name, session_path.c_str(),
						       logind_session_iface, "TakeControl")};

	if (!msg) {
		throw std::bad_alloc();
	}

	dbus_bool_t force_takeover = FALSE;
	if (!dbus_message_append_args(msg.get(), DBUS_TYPE_BOOLEAN, &force_takeover, DBUS_TYPE_INVALID)) {
		throw std::bad_alloc();
	}

	dbus::Message reply{dbus_connection_send_with_reply_and_block(
	    backend_->compositor()->system_context.get_connection(), msg.get(), -1, &err)};
	if (!reply) {
		if (err.name() == DBUS_ERROR_UNKNOWN_METHOD) {
			throw std::runtime_error("Logind: Incompatibly old logind detected");
		} else {
			throw std::runtime_error("Logind: Could not take over session");
		}
	}
}

void
SessionManagerLogind::activate_session()
{
	dbus::Message msg{dbus_message_new_method_call(logind_bus_name, session_path.c_str(),
						       logind_session_iface, "Activate")};
	if (!msg) {
		throw std::bad_alloc();
	}

	dbus_connection_send(backend_->compositor()->system_context.get_connection(), msg.get(), nullptr);
}

void
SessionManagerLogind::switch_vty(unsigned int vtynum) noexcept
{
	dbus::Message msg{
	    dbus_message_new_method_call(logind_bus_name, seat_path.c_str(), logind_seat_iface, "SwitchTo")};

	// If there are any failures along the way, just do not care.
	if (!msg) {
		return;
	}

	if (!dbus_message_append_args(msg.get(), DBUS_TYPE_UINT32, &vtynum, DBUS_TYPE_INVALID)) {
		return;
	}

	dbus_connection_send(backend_->compositor()->system_context.get_connection(), msg.get(), nullptr);
}

DBusHandlerResult
SessionManagerLogind::handle_messages_cb(DBusConnection *conn, DBusMessage *msg, void *user_data)
{
	SessionManagerLogind *self = static_cast<SessionManagerLogind *>(user_data);

	bool match = false;
	if (dbus_message_is_signal(msg, logind_manager_iface, "SessionRemoved")) {
		self->on_session_remove(msg);
		match = true;
	}
	if (dbus_message_is_signal(msg, DBUS_INTERFACE_PROPERTIES, "PropertiesChanged")) {
		// TODO: Handle
		match = true;
	}
	if (dbus_message_is_signal(msg, logind_session_iface, "PauseDevice")) {
		self->on_pause_device(msg);
		match = true;
	}
	if (dbus_message_is_signal(msg, logind_session_iface, "ResumeDevice")) {
		self->on_resume_device(msg);
		match = true;
	}

	return match ? DBUS_HANDLER_RESULT_HANDLED : DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

void
SessionManagerLogind::on_session_remove(DBusMessage *msg)
{
	const char *name, *obj_path;

	dbus::Error error;
	if (!dbus_message_get_args(msg, &error, DBUS_TYPE_STRING, &name, DBUS_TYPE_OBJECT_PATH, &obj_path,
				   DBUS_TYPE_INVALID)) {
		std::fprintf(stderr, "Logind: Unable to parse SessionRemoved-signal: %s\n",
			     error.message().c_str());
		return;
	}

	if (session_name_ == name) {
		std::fprintf(stderr, "Logind: our session was removed!\n");
		std::fprintf(stderr, "Quitting!\n");

		ev_break(backend_->compositor()->loop(), EVBREAK_ALL);
	}
}

void
SessionManagerLogind::on_pause_device(DBusMessage *msg)
{
	const char *pause_type;
	uint32_t major, minor;

	dbus::Error error;
	if (!dbus_message_get_args(msg, &error, DBUS_TYPE_UINT32, &major, DBUS_TYPE_UINT32, &minor,
				   DBUS_TYPE_STRING, &pause_type, DBUS_TYPE_INVALID)) {
		std::fprintf(stderr, "Logind: Unable to parse PauseDevice-signal: %s\n",
			     error.message().c_str());
		return;
	}

	// PauseDevice has three "types":
	// - "pause" which we have to acknowledge with PauseDeviceComplete
	// - "force" which just means that it was paused by logind and we're
	//   just being notified
	// - "gone" which means that the device is gone for good (at least for
	//   now)
	//
	// We're just gonna unconditionally send the device pausing back to
	// logind.
	if (std::string{pause_type} == "pause") {
		dbus::Message reply_msg{dbus_message_new_method_call(
		    logind_bus_name, session_path.c_str(), logind_session_iface, "PauseDeviceComplete")};
		if (reply_msg) {
			if (dbus_message_append_args(reply_msg.get(), DBUS_TYPE_UINT32, &major,
						     DBUS_TYPE_UINT32, &minor, DBUS_TYPE_INVALID)) {
				dbus_connection_send(backend_->compositor()->system_context.get_connection(),
						     reply_msg.get(), nullptr);
			}
		}
	}

	// No matter the type of pause, we have to notify the backend
	// that the device is paused/gone one way or another.
	backend_->on_device_pause(major, minor);
}

void
SessionManagerLogind::on_resume_device(DBusMessage *msg)
{
	int new_fd;
	uint32_t major, minor;

	dbus::Error error;
	if (!dbus_message_get_args(msg, &error, DBUS_TYPE_UINT32, &major, DBUS_TYPE_UINT32, &minor,
				   DBUS_TYPE_UNIX_FD, &new_fd, DBUS_TYPE_INVALID)) {
		std::fprintf(stderr, "Logind: Unable to parse ResumeDevice-signal: %s\n",
			     error.message().c_str());
		return;
	}

	// Notify the backend that the device is back
	backend_->on_device_resume(major, minor, new_fd);
}

} // namespace drm

} // namespace orbycomp
