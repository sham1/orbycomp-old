/*
 * gles-renderer.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gles-renderer.hh"
#include <orbycomp/renderers/gles-renderer.hh>

template <typename T, size_t N> static constexpr std::size_t array_len(T (&array)[N]) { return N; }

namespace orbycomp
{

namespace drm
{

std::unique_ptr<GLESRenderer>
GLESRenderer::setup(Backend *backend)
{
	GBMDevice dev{backend->drm_dev.fd_};
	return std::unique_ptr<GLESRenderer>(new GLESRenderer(backend, std::move(dev)));
}

GLESRenderer::GLESRenderer(Backend *backend, GBMDevice &&dev)
    : Renderer{backend, load_renderer(backend, dev.get())}, gbm_dev_{std::move(dev)}
{
}

RendererHandle
GLESRenderer::load_renderer(Backend *backend, struct gbm_device *dev)
{
	uint32_t formats[] = {
	    GBM_FORMAT_XRGB8888,
	};

	orbycomp::renderers::GLESRendererOptions opts{};
	opts.compositor = backend->compositor();

	opts.platform = EGL_PLATFORM_GBM_KHR;
	opts.backend_dpy = dev;

	opts.formats = formats;
	opts.formats_count = array_len(formats);

	return orbycomp::load_renderer("liborbycomp-gles-renderer.so", opts);
}

void
GLESRenderer::setup_output(Output *output)
{
	auto &mode = output->current_mode();
	GBMSurface surface{gbm_dev_, uint32_t(mode.width_), uint32_t(mode.height_), GBM_FORMAT_XRGB8888,
			   GBM_BO_USE_RENDERING | GBM_BO_USE_SCANOUT};

	auto [output_info, _] =
	    outputs_.try_emplace(output, std::make_unique<OutputInfo>(this, output, std::move(surface)));

	orbycomp::renderers::GLESRendererOutputOptions opts;

	opts.backend_window = (*output_info).second->surface_.get();

	renderer_->setup_output(output, opts);
}

SurfaceFB *
GLESRenderer::render_output(Output *output)
{
	auto *info = outputs_[output].get();
	return info->render();
}

GLESRenderer::OutputInfo::OutputInfo(GLESRenderer *renderer, Output *output, GBMSurface &&surface)
    : renderer_{renderer}, output_{output}, surface_{std::move(surface)}
{
}

GLESRenderer::OutputInfo::~OutputInfo() {}

SurfaceFB *
GLESRenderer::OutputInfo::render()
{
	renderer_->renderer_->render_output(output_);

	struct gbm_bo *bo = gbm_surface_lock_front_buffer(surface_.get());
	if (!bo) {
		std::fprintf(stderr, "Failed to lock front buffer: %s\n", strerror(errno));
		return nullptr;
	}

	if (bo_) {
		gbm_surface_release_buffer(surface_.get(), bo_);
	}

	auto *ret = SurfaceFB::get_from_bo(bo);
	bo_ = bo;

	return ret;
}

}; // namespace drm

}; // namespace orbycomp
