/*
 * renderer.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "renderer.hh"
#include <cstdio>
#include <orbycomp/renderer-loader.hh>
#include <stdexcept>

#include "gles-renderer.hh"
#include "software-renderer.hh"

namespace orbycomp
{

namespace drm
{

std::unique_ptr<Renderer>
Renderer::choose_renderer(Backend *backend)
{
	try {
		return GLESRenderer::setup(backend);
	} catch (std::exception &ex) {
		std::fprintf(stderr, "Could not load GLES renderer: %s\n", ex.what());
	}

	try {
		return std::make_unique<SoftwareRenderer>(backend);
	} catch (std::exception &ex) {
		std::fprintf(stderr, "Could not load software renderer: %s\n", ex.what());
	}

	throw std::runtime_error("Could not load any renderer for backend");
}

SurfaceFB::SurfaceFB(struct gbm_bo *bo) : bo_{bo}, fb_{create_fb()} {}

uint32_t
SurfaceFB::create_fb()
{
	struct gbm_device *dev = gbm_bo_get_device(bo_);
	int fd = gbm_device_get_fd(dev);

	uint32_t fb{0};

	uint32_t width{gbm_bo_get_width(bo_)};
	uint32_t height{gbm_bo_get_height(bo_)};
	uint32_t format{gbm_bo_get_format(bo_)};

	int ret{-1};

	uint32_t handles[4]{0};
	uint32_t strides[4]{0};
	uint32_t offsets[4]{0};
	uint32_t flags{0};

	uint64_t modifiers[4]{0};

	modifiers[0] = gbm_bo_get_modifier(bo_);
	int planes{gbm_bo_get_plane_count(bo_)};
	for (int i = 0; i < planes; ++i) {
		strides[i] = gbm_bo_get_stride_for_plane(bo_, i);
		handles[i] = gbm_bo_get_handle(bo_).u32;
		offsets[i] = gbm_bo_get_offset(bo_, i);
		modifiers[i] = modifiers[0];
	}

	ret = drmModeAddFB2WithModifiers(fd, width, height, format, handles, strides, offsets, modifiers, &fb,
					 flags);

	if (ret < 0) {
		uint32_t default_handles[4]{gbm_bo_get_handle(bo_).u32, 0, 0, 0};
		uint32_t default_strides[4]{gbm_bo_get_stride(bo_), 0, 0, 0};
		uint32_t default_offsets[4]{0};
		ret = drmModeAddFB2(fd, width, height, format, default_handles, default_strides,
				    default_offsets, &fb, 0);
		if (ret < 0) {
			throw std::system_error(errno, std::generic_category(),
						"Could not create framebuffer for GBM buffer");
		}
	}

	return fb;
}

SurfaceFB::~SurfaceFB()
{
	if (fb_) {
		int fd = gbm_bo_get_fd(bo_);
		drmModeRmFB(fd, fb_);
	}
}

SurfaceFB *
SurfaceFB::get_from_bo(struct gbm_bo *bo)
{
	SurfaceFB *self = static_cast<SurfaceFB *>(gbm_bo_get_user_data(bo));
	if (self) {
		return self;
	}

	self = new SurfaceFB(bo);
	gbm_bo_set_user_data(bo, self, SurfaceFB::on_fb_destroy_cb);

	return self;
}

void
SurfaceFB::on_fb_destroy_cb(struct gbm_bo *bo, void *data)
{
	SurfaceFB *self = static_cast<SurfaceFB *>(data);
	delete self;
}

} // namespace drm

} // namespace orbycomp
