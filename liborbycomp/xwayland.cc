/*
 * xwayland.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <cstring>
#include <fcntl.h>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/xwayland.hh>
#include <orbycomp/util/format-string.hh>
#include <sstream>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

namespace orbycomp
{

void
XWaylandCtx::on_connect_event(EV_P_ struct ev_io *w, int revents)
{
	XWaylandCtx *self = static_cast<XWaylandCtx *>(w->data);

	std::string display_name = util::format_str(":%d", self->dpynum_);
	if (!self->spawn_x_server(display_name)) {
		// If we failed to spawn the X server, do not remove
		// the connection listeners.
		std::fprintf(stderr, "Failed to spawn X server\n");
		return;
	}

	// We don't know if the event was triggered by a connection
	// to the abstract socket or the unix socket.
	//
	// Either way, we have to stop listening to both.
	ev_io_stop(EV_A_ & self->abstract_sock_listener_);
	ev_io_stop(EV_A_ & self->unix_sock_listener_);
}

void
XWaylandCtx::on_xserver_ready(EV_P_ struct ev_signal *w, int revents)
{
	XWaylandCtx *self = static_cast<XWaylandCtx *>(w->data);

	try {
		self->xwm_.emplace(self);
	} catch (std::exception &ex) {
		std::fprintf(stderr, "Could not create XWM: %s\n", ex.what());
		ev_break(EV_A, EVBREAK_ALL);
	}

	ev_signal_stop(EV_A_ w);
}

void
XWaylandCtx::create_x_lock_file(std::string &lockfile_path)
{
	struct FdWrapper {
		int fd_;
		FdWrapper(int fd) : fd_{fd} {}
		~FdWrapper()
		{
			if (fd_ != -1) {
				::close(fd_);
				fd_ = -1;
			}
		}

		void
		close()
		{
			if (fd_ != -1) {
				::close(fd_);
				fd_ = -1;
			}
		}
	} lock_fd{-1};

	lockfile_path = util::format_str("/tmp/.X%d-lock", dpynum_);
	lock_fd.fd_ = open(lockfile_path.c_str(), O_WRONLY | O_CLOEXEC | O_CREAT | O_EXCL, 0444);
	if (lock_fd.fd_ < 0 && errno == EEXIST) {
		// Some other process has this lock. Let's try to dislodge it.
		lock_fd.fd_ = open(lockfile_path.c_str(), O_CLOEXEC | O_RDONLY);
		// 10 digits plus trailing line-feed or NUL byte.
		char pid[11]{0};
		if (lock_fd.fd_ < 0 || read(lock_fd.fd_, pid, 11) != 11) {
			throw std::system_error(EEXIST, std::generic_category(), "Could not read X lockfile");
		}

		// The lock file doesn't contain a NUL,
		// so we must add it to the end
		pid[10] = '\0';
		char *end_ptr{nullptr};

		// errno can be set even if there has not been an error
		// therefore we have to clear it for strtol.
		errno = 0;
		pid_t other_pid{static_cast<pid_t>(std::strtol(pid, &end_ptr, 10))};
		if (errno == ERANGE) {
			throw std::system_error(EEXIST, std::generic_category(),
						"Could not parse X lockfile");
		}

		// Now we'll check whether the process that has created this
		// lockfile still exists. If not, we'll try to remove it and
		// try again.
		if (kill(other_pid, 0) < 0 && errno == ESRCH) {
			// It doesn't exist. Let's try to unlink it.
			lock_fd.close();
			if (unlink(lockfile_path.c_str()) != 0) {
				// Could not unlink the file.
				throw std::system_error(EEXIST, std::generic_category());
			} else {
				// Unlinked the file. Try to lock again.
				throw std::system_error(EAGAIN, std::generic_category());
			}
		}

		// The other process exists. We should just try a different
		// display.
		throw std::system_error(EEXIST, std::generic_category());

	} else if (lock_fd.fd_ < 0) {
		throw std::system_error(errno, std::generic_category(), "Could not create X lockfile");
	}

	// We've established that the other lockfile does not exist.
	// Now is the time to create our own for this display.
	//
	// We use our own PID for the lockfile, since we control the
	// X server. For more prior art, see Weston.
	std::string lock_content = util::format_str("%10d\n", getpid());
	::write(lock_fd.fd_, lock_content.data(), lock_content.size());
}

int
XWaylandCtx::bind_abstract_sock()
{
	struct sockaddr_un addr;
	socklen_t size, name_size;
	int fd;

	fd = socket(PF_LOCAL, SOCK_STREAM | SOCK_CLOEXEC, 0);
	if (fd < 0) {
		return -1;
	}

	addr.sun_family = AF_LOCAL;
	name_size = std::snprintf(addr.sun_path, sizeof(addr.sun_path), "%c/tmp/.X11-unix/%d", 0, dpynum_);
	size = offsetof(struct sockaddr_un, sun_path) + name_size;
	if (bind(fd, reinterpret_cast<struct sockaddr *>(&addr), size) < 0) {
		// Could not bind to the abstract socket
		std::fprintf(stderr, "Could not bind to @%s: %s\n", addr.sun_path + 1, std::strerror(errno));
		close(fd);
		return -1;
	}
	if (listen(fd, 1) < 0) {
		close(fd);
		return -1;
	}

	return fd;
}

int
XWaylandCtx::bind_unix_sock()
{
	struct sockaddr_un addr;
	socklen_t size, name_size;
	int fd;

	fd = socket(PF_LOCAL, SOCK_STREAM | SOCK_CLOEXEC, 0);
	if (fd < 0) {
		return -1;
	}

	addr.sun_family = AF_LOCAL;
	name_size = snprintf(addr.sun_path, sizeof(addr.sun_path), "/tmp/.X11-unix/X%d", dpynum_) + 1;
	size = offsetof(struct sockaddr_un, sun_path) + name_size;
	unlink(addr.sun_path);
	if (bind(fd, reinterpret_cast<struct sockaddr *>(&addr), size) < 0) {
		// Could not bind to the abstract socket
		std::fprintf(stderr, "Could not bind to %s: %s\n", addr.sun_path, std::strerror(errno));
		close(fd);
		return -1;
	}

	if (listen(fd, 1) < 0) {
		unlink(addr.sun_path);
		close(fd);
		return -1;
	}

	return fd;
}

XWaylandCtx::XWaylandCtx(Compositor *compositor) : compositor_{compositor}
{
	char dpyname[13]{0};
	std::string lockfile_path{};
	while (true) {
		// Let's try to lock down a display number for ourselves.
		// Exceptions for control flow!
		try {
			create_x_lock_file(lockfile_path);
		} catch (std::system_error &sys_ex) {
			int err_code = sys_ex.code().value();
			if (err_code != EEXIST && err_code != EAGAIN) {
				// If it's not one of the aforementioned
				// errors, rethrow.
				throw;
			}
			// If this is EEXIST, we have to increment the
			// dpynum.
			if (err_code == EEXIST) {
				++dpynum_;
				continue;
			}

			// If this is EAGAIN, we just want to try again...
			if (err_code == EAGAIN) {
				continue;
			}
		}

		// We now have our lock file. Let's try to create an abstract
		// UNIX socket for our XWayland connection.
		abstract_fd_ = bind_abstract_sock();
		if (abstract_fd_ < 0 && errno == EADDRINUSE) {
			// If we cannot get this socket, retry with a new
			// dpynum.
			++dpynum_;
			unlink(lockfile_path.c_str());
			continue;
		}

		// We now have our abstract socket, so we now have to
		// obtain a normal UNIX socket and make sure that succeeds.
		break;
	}

	unix_fd_ = bind_unix_sock();
	if (unix_fd_ < 0) {
		unlink(lockfile_path.c_str());
		close(abstract_fd_);
		throw std::system_error(errno, std::generic_category(), "Could not create X11 connection");
	}

	std::snprintf(dpyname, sizeof(dpyname), ":%d", dpynum_);
	std::fprintf(stderr, "X server launched on display %s\n", dpyname);
	setenv("DISPLAY", dpyname, 1);

	// We wish to be lazy about initializing the X server, so we have to
	// listen to the proper events on the sockets.
	ev_io_init(&abstract_sock_listener_, &XWaylandCtx::on_connect_event, abstract_fd_, EV_READ);
	ev_io_init(&unix_sock_listener_, &XWaylandCtx::on_connect_event, unix_fd_, EV_READ);

	abstract_sock_listener_.data = this;
	unix_sock_listener_.data = this;

	ev_io_start(compositor_->loop(), &abstract_sock_listener_);
	ev_io_start(compositor_->loop(), &unix_sock_listener_);
}

XWaylandCtx::~XWaylandCtx()
{
	if (ev_is_active(&abstract_sock_listener_)) {
		ev_io_stop(compositor_->loop(), &abstract_sock_listener_);
	}

	if (ev_is_active(&unix_sock_listener_)) {
		ev_io_stop(compositor_->loop(), &unix_sock_listener_);
	}

	if (ev_is_active(&on_xserver_ready_listener_)) {
		ev_signal_stop(compositor_->loop(), &on_xserver_ready_listener_);
	}

	char path[256];
	snprintf(path, sizeof(path), "/tmp/.X%d-lock", dpynum_);
	unlink(path);
	snprintf(path, sizeof(path), "/tmp/.X11-unix/X%d", dpynum_);
	unlink(path);

	close(abstract_fd_);
	close(unix_fd_);
}

bool
XWaylandCtx::spawn_x_server(std::string &display_name)
{
	int fd{-1};
	// Some socketpairs for the purpose of letting XWayland
	// communicate as a Wayland client, and for the compositor
	// to have an X connection of its own to the X serevr.
	int wl_socks[2]{-1};
	int x_socks[2]{-1};

	if (socketpair(AF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0, wl_socks) < 0) {
		std::fprintf(stderr, "Failed to create Wayland connection to X server\n");
		return false;
	}

	if (socketpair(AF_UNIX, SOCK_STREAM | SOCK_CLOEXEC, 0, x_socks) < 0) {
		std::fprintf(stderr, "Failed to create X connection to X server\n");
		close(wl_socks[0]);
		close(wl_socks[1]);
		return false;
	}

	enum socketpair_side {
		PAIR_PARENT,
		PAIR_CHILD,
	};

	ev_signal_init(&on_xserver_ready_listener_, &XWaylandCtx::on_xserver_ready, SIGUSR1);
	on_xserver_ready_listener_.data = this;

	pid_t pid = fork();
	switch (pid) {
	case 0:
		// We are in the child, going to spawn X server.
		try {
			fd = dup(wl_socks[PAIR_CHILD]);
			if (fd < 0) {
				throw std::system_error(errno, std::generic_category());
			}

			std::string socket_str = util::format_str("%d", fd);
			setenv("WAYLAND_SOCKET", socket_str.c_str(), 1);

			fd = dup(abstract_fd_);
			if (fd < 0) {
				throw std::system_error(errno, std::generic_category());
			}
			std::string abstract_str = util::format_str("%d", fd);

			fd = dup(unix_fd_);
			if (fd < 0) {
				throw std::system_error(errno, std::generic_category());
			}
			std::string unix_str = util::format_str("%d", fd);

			fd = dup(x_socks[PAIR_CHILD]);
			if (fd < 0) {
				throw std::system_error(errno, std::generic_category());
			}
			std::string wm_str = util::format_str("%d", fd);

			// Ignore SIGUSR so that the X server's SIGUSR1 gets
			// sent to the compositor properly. This is used as
			// indication about the X server being ready.
			signal(SIGUSR1, SIG_IGN);

			if (execlp("Xwayland", "Xwayland", display_name.c_str(), "-rootless", "-listen",
				   abstract_str.c_str(), "-listen", unix_str.c_str(), "-wm", wm_str.c_str(),
				   "-terminate", nullptr) < 0) {
				throw std::system_error(errno, std::generic_category(),
							"exec(\"Xwayland\") failed");
			}
		} catch (std::exception &err) {
			std::fprintf(stderr, "Could not spawn X server: %s\n", err.what());
			_exit(EXIT_FAILURE);
		}
		break;
	case -1:
		// Something went wrong.
		break;
	default:
		// We are in the compositor.
		ev_signal_start(compositor_->loop(), &on_xserver_ready_listener_);

		close(wl_socks[PAIR_CHILD]);
		xwayland_client_ = wl_client_create(compositor_->display(), wl_socks[PAIR_PARENT]);
		close(x_socks[PAIR_CHILD]);
		wm_fd_ = x_socks[PAIR_PARENT];

		x_server_pid_ = pid;
		break;
	}

	return true;
}

}; // namespace orbycomp
