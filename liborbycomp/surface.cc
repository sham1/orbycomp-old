/*
 * surface.cc
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <new>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/surface.hh>

namespace orbycomp
{

const Region Surface::infinite_region{
    {std::numeric_limits<int32_t>::min(), std::numeric_limits<int32_t>::min(),
     std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()}};

Surface::Surface(Compositor *compositor, struct wl_resource *resource)
    : compositor_{compositor}, resource_{resource}, input_region_{infinite_region}
{
	wl_list_init(&this->frames);
	wl_list_init(&this->on_attached_buffer_destroy_.link);
}

Surface::~Surface()
{
	// We may have frame callbacks lying around, both pending for
	// commit and already commited.

	struct wl_resource *res, *tmp;
	wl_resource_for_each_safe(res, tmp, &this->frames) { wl_resource_destroy(res); }

	if (pending_state.frame_cbs) {
		auto &frame_cbs = pending_state.frame_cbs.value();
		wl_resource_for_each_safe(res, tmp, &frame_cbs) { wl_resource_destroy(res); }
	}

	if (buffer_) {
		wl_list_remove(&on_attached_buffer_destroy_.link);
	}
}

void
Surface::attach_buffer(struct wl_resource *buffer, int32_t xoff, int32_t yoff)
{
	pending_state.attach = {buffer, xoff, yoff};
}

void
Surface::add_to_surface_damage(Region::Rect rect)
{
	if (!pending_state.surface_damage) {
		pending_state.surface_damage.emplace(rect);
	} else {
		*(pending_state.surface_damage) += rect;
	}
}

void
Surface::add_to_buffer_damage(Region::Rect rect)
{
	if (!pending_state.buffer_damage) {
		pending_state.buffer_damage.emplace(rect);
	} else {
		*(pending_state.buffer_damage) += rect;
	}
}

static void
remove_resource_from_list(struct wl_resource *res)
{
	wl_list_remove(wl_resource_get_link(res));
}

void
Surface::add_frame(struct wl_client *client, uint32_t callback)
{
	auto res = wl_resource_create(client, &wl_callback_interface, 1, callback);
	if (res == nullptr) {
		throw std::bad_alloc();
	}

	wl_resource_set_implementation(res, nullptr, nullptr, remove_resource_from_list);

	if (!pending_state.frame_cbs) {
		pending_state.frame_cbs.emplace();
		wl_list_init(&*pending_state.frame_cbs);
	}

	wl_list_insert(pending_state.frame_cbs->prev, wl_resource_get_link(res));
}

void
Surface::commit_state(PendingState &pending_state)
{
	bool was_update = false;
	bool attach = false;
	// First we will handle the buffer attachment
	if (pending_state.attach) {
		auto &state = pending_state.attach.value();

		attach = true;

		if (!state.new_buffer) {
			buffer_ = nullptr;
			renderable_.reset();
		} else {
			was_update = buffer_ != nullptr;

			if (buffer_) {
				wl_list_remove(&on_attached_buffer_destroy_.link);
			}

			buffer_ = state.new_buffer;

			on_attached_buffer_destroy_.notify = Surface::on_attached_buffer_destroy_cb;
			wl_resource_add_destroy_listener(buffer_, &on_attached_buffer_destroy_);

			x_ += state.xoff;
			y_ += state.yoff;
			if (!renderable_) {
				renderable_ =
				    compositor_->backend().get_renderer()->get_surface_renderable(this);
			}
		}
	}

	// Then we will handle the possible changes to buffer_transform
	// and buffer_scale, since those affect how surface- and buffer-local
	// damage and other regions are handled.
	if (pending_state.transform || pending_state.scale) {
		if (pending_state.transform) {
			transform = pending_state.transform.value();
		}

		if (pending_state.scale) {
			scale = pending_state.scale.value();
		}

		calculate_transformation_matrices();
		was_update = true;
	}

	// Now we can transform the surface damage into buffer-space
	// so that it can be united.
	if (pending_state.surface_damage) {
		auto &damage = pending_state.surface_damage.value();

		// TODO: Transform by `surface_to_buffer_transform`
		Region tmp(damage);

		damage_region += tmp;
		was_update = true;
	}

	// Buffer damage doesn't need to be transformed.
	if (pending_state.buffer_damage) {
		auto &damage = pending_state.buffer_damage.value();

		damage_region += damage;
		was_update = true;
	}

	if (pending_state.opaque_region) {
		opaque_region_ = pending_state.opaque_region.value();
	}
	if (pending_state.input_region) {
		input_region_ = pending_state.input_region.value();
	}

	if (pending_state.frame_cbs) {
		auto &frame_cbs = pending_state.frame_cbs.value();
		wl_list_insert_list(&this->frames, &frame_cbs);
	}

	reset_pending(pending_state);

	on_commit_signal_(this);

	if (renderable_ && was_update) {
		renderable_->upload_texture(attach);
		renderable_->update();
	}
}

void
Surface::on_attached_buffer_destroy_cb(struct wl_listener *listener, void *data)
{
	Surface *self = util::container_of(listener, &Surface::on_attached_buffer_destroy_);

	self->buffer_ = nullptr;
}

static void destroy_surface(struct wl_resource *resource);

void
Surface::create_surface(struct wl_client *client, uint32_t id, Compositor *compositor)
{
	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> res{
	    wl_resource_create(client, &wl_surface_interface, 4, id), wl_resource_destroy};
	if (res == nullptr) {
		throw std::bad_alloc();
	}

	Surface *self = new Surface(compositor, res.get());
	wl_resource_set_implementation(res.get(), &surface_iface, self, destroy_surface);

	res.release();

	compositor->on_surface_create_signal(*self);
}

void
Surface::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Surface::attach(struct wl_client *client,
		struct wl_resource *resource,
		struct wl_resource *buffer,
		int32_t xoff,
		int32_t yoff)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));
	self->attach_buffer(buffer, xoff, yoff);
}

void
Surface::damage(struct wl_client *client,
		struct wl_resource *resource,
		int32_t x,
		int32_t y,
		int32_t width,
		int32_t height)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));
	try {
		self->add_to_surface_damage(Region::Rect::from_xy_width_height(x, y, width, height));
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Surface::frame(struct wl_client *client, struct wl_resource *resource, uint32_t callback)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	try {
		self->add_frame(client, callback);
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Surface::set_opaque_region(struct wl_client *client, struct wl_resource *resource, struct wl_resource *region)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	if (region == nullptr) {
		self->pending_state.opaque_region.emplace();
		return;
	}

	try {
		Region *region_data = static_cast<Region *>(wl_resource_get_user_data(region));

		self->pending_state.opaque_region.emplace(*region_data);
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Surface::set_input_region(struct wl_client *client, struct wl_resource *resource, struct wl_resource *region)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	if (region == nullptr) {
		self->pending_state.input_region.emplace(infinite_region);
		return;
	}

	try {
		Region *region_data = static_cast<Region *>(wl_resource_get_user_data(region));

		self->pending_state.input_region.emplace(*region_data);
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Surface::commit(struct wl_client *client, struct wl_resource *resource)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	try {
		if (self->subsurf_ && self->subsurf_->is_synchronized()) {
			self->subsurf_->commit_to_cache(self->pending_state);
		} else {
			self->commit_state(self->pending_state);
		}
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
Surface::set_buffer_transform(struct wl_client *client, struct wl_resource *resource, int32_t transform)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	if ((transform < 0) || (transform > 7)) {
		wl_resource_post_error(resource, WL_SURFACE_ERROR_INVALID_TRANSFORM,
				       "Invalid buffer transform: %d", transform);
		return;
	}

	self->pending_state.transform = static_cast<enum wl_output_transform>(transform);
}

void
Surface::set_buffer_scale(struct wl_client *client, struct wl_resource *resource, int32_t scale)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	if (scale <= 0) {
		wl_resource_post_error(resource, WL_SURFACE_ERROR_INVALID_SCALE, "Invalid buffer scale: %d",
				       scale);
		return;
	}

	self->pending_state.scale = scale;
}

void
Surface::buffer_damage(struct wl_client *client,
		       struct wl_resource *resource,
		       int32_t x,
		       int32_t y,
		       int32_t width,
		       int32_t height)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));

	try {
		self->add_to_buffer_damage(Region::Rect::from_xy_width_height(x, y, width, height));
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

static void
destroy_surface(struct wl_resource *resource)
{
	Surface *self = static_cast<Surface *>(wl_resource_get_user_data(resource));
	delete self;
}

void
Surface::reset_pending(PendingState &pending_state)
{
	pending_state.attach.reset();
	pending_state.surface_damage.reset();
	pending_state.frame_cbs.reset();
	pending_state.opaque_region.reset();
	pending_state.input_region.reset();
	pending_state.transform.reset();
	pending_state.scale.reset();
	pending_state.buffer_damage.reset();
}

void
Surface::reset_damage()
{
	damage_region = Region{};
}

void
Surface::calculate_transformation_matrices()
{
	using Mat4 = affine::Mat4;

	// TODO: Implement properly
	surface_to_buffer_transform = Mat4::get_identity();
	buffer_to_surface_transform = Mat4::get_identity();
}

void
Surface::call_frame_cbs(uint32_t render_msec)
{
	struct wl_resource *resource, *tmp;
	wl_resource_for_each_safe(resource, tmp, &frames)
	{
		wl_callback_send_done(resource, render_msec);
		wl_resource_destroy(resource);
	}
}

SubSurface::SubSurface(struct wl_client *client, Surface *surface, Surface *parent, uint32_t id)
    : resource_{wl_resource_create(client, &wl_subsurface_interface, 1, id)}, surface_{surface}
{
	if (!resource_) {
		throw std::bad_alloc();
	}

	wl_resource_set_implementation(resource_, &SubSurface::iface, this, SubSurface::on_resource_destroy);

	surface_->parent_ = parent;
	surface_->subsurf_ = this;

	surface_->parent_->children_above.push_back(surface_);

	on_parent_commit_ = surface_->parent_->on_commit_signal_.connect(
	    [this](Surface *) { this->parent_commit_handler(); });

	on_surface_commit_ =
	    surface_->on_commit_signal_.connect([this](Surface *) { this->surface_commit_handler(); });

	wl_list_init(&on_surface_destroy_.link);
	on_surface_destroy_.notify = SubSurface::on_surface_destroy_cb;
	wl_resource_add_destroy_listener(surface_->resource_, &on_surface_destroy_);

	wl_list_init(&on_parent_destroy_.link);
	on_parent_destroy_.notify = SubSurface::on_parent_destroy_cb;
	wl_resource_add_destroy_listener(surface_->parent_->resource_, &on_parent_destroy_);
}

void
SubSurface::parent_commit_handler()
{
	if (pending_placement) {
		auto &order = pending_placement.value();
		auto *parent = surface_->parent_;

		parent->children_above.remove(surface_);
		parent->children_below.remove(surface_);

		if (order.surface_ == parent) {
			switch (order.relation_) {
			case Placing::PlaceAbove:
				parent->children_above.emplace_front(surface_);
				break;
			case Placing::PlaceBelow:
				parent->children_below.emplace_front(surface_);
				break;
			}
		} else {
			bool found_sibling = false;
			std::list<Surface *> *stack_half = nullptr;
			auto sibling = std::find(parent->children_above.begin(), parent->children_above.end(),
						 order.surface_);
			if (sibling != parent->children_above.end()) {
				stack_half = &parent->children_above;
				found_sibling = true;
			}
			if (!found_sibling) {
				sibling = std::find(parent->children_below.begin(),
						    parent->children_below.end(), order.surface_);

				if (sibling != parent->children_below.end()) {
					stack_half = &parent->children_below;
					found_sibling = true;
				}
			}

			if (found_sibling) {
				switch (order.relation_) {
				case Placing::PlaceAbove:
					++sibling;
					stack_half->insert(sibling, surface_);
					break;
				case Placing::PlaceBelow:
					stack_half->insert(sibling, surface_);
					break;
				}
			}
		}

		pending_placement.reset();
	}

	if (has_set_position) {
		surface_->x_ = x_pos_;
		surface_->y_ = y_pos_;
		has_set_position = false;
		if (surface_->renderable_) {
			surface_->renderable_->update();
		}
	}

	synchronize();
}

bool
SubSurface::is_synchronized() const
{
	Surface *s = surface_;
	while (s) {
		if (s->subsurf_) {
			if (s->subsurf_->synchronized_) {
				return true;
			}
		} else {
			return false;
		}
		s = s->parent_;
	}
	return false;
}

void
SubSurface::commit_to_cache(Surface::PendingState &pending_state)
{
	if (state_cache.attach) {
		state_cache.attach->new_buffer = pending_state.attach->new_buffer;
		state_cache.attach->xoff += pending_state.attach->xoff;
		state_cache.attach->yoff += pending_state.attach->yoff;
	} else {
		state_cache.attach = pending_state.attach;
	}

	if (state_cache.surface_damage) {
		state_cache.surface_damage.value() += pending_state.surface_damage.value_or(Region{});
	} else {
		state_cache.surface_damage = pending_state.surface_damage;
	}

	if (pending_state.frame_cbs) {
		if (state_cache.frame_cbs) {
			auto &frame_cbs = pending_state.frame_cbs.value();
			wl_list_insert_list(&state_cache.frame_cbs.value(), &frame_cbs);
		} else {
			state_cache.frame_cbs = std::move(pending_state.frame_cbs);
		}
	}

	if (state_cache.opaque_region) {
		state_cache.opaque_region.value() += pending_state.opaque_region.value_or(Region{});
	} else {
		state_cache.opaque_region = pending_state.opaque_region;
	}

	if (state_cache.input_region) {
		state_cache.input_region.value() += pending_state.input_region.value_or(Region{});
	} else {
		state_cache.input_region = pending_state.input_region;
	}

	state_cache.transform = pending_state.transform;
	state_cache.scale = pending_state.scale;

	if (state_cache.buffer_damage) {
		state_cache.buffer_damage.value() += pending_state.buffer_damage.value_or(Region{});
	} else {
		state_cache.buffer_damage = pending_state.buffer_damage;
	}

	Surface::reset_pending(pending_state);
}

void
SubSurface::synchronize()
{
	surface_->commit_state(state_cache);

	for (auto *child : surface_->children_below) {
		child->subsurf_->synchronize();
	}

	for (auto *child : surface_->children_above) {
		child->subsurf_->synchronize();
	}
}

void
SubSurface::surface_commit_handler()
{
	auto *parent = surface_->parent_;
	if (surface_->renderable_) {
		auto *r = surface_->renderable_.get();
		if (parent->renderable_) {
			auto *pr = parent->renderable_.get();

			r->set_parent(pr);
			r->layer() = pr->layer();
		}

		auto surface_above_iter =
		    std::find(parent->children_above.begin(), parent->children_above.end(), surface_);
		if (surface_above_iter != parent->children_above.end()) {
			r->relative_pos() =
			    std::distance(parent->children_above.begin(), surface_above_iter) + 1;
		}

		auto surface_below_iter =
		    std::find(parent->children_below.begin(), parent->children_below.end(), surface_);
		if (surface_below_iter != parent->children_below.end()) {
			r->relative_pos() =
			    -std::distance(parent->children_below.begin(), surface_below_iter) - 1;
		}

		r->update();
	}
}

SubSurface::~SubSurface()
{
	if (surface_) {
		surface_->parent_->children_above.remove(surface_);
		surface_->parent_->children_below.remove(surface_);

		surface_->parent_ = nullptr;
		surface_->subsurf_ = nullptr;
		surface_ = nullptr;

		wl_list_remove(&on_surface_destroy_.link);
		wl_list_remove(&on_parent_destroy_.link);
		on_parent_commit_.disconnect();
	}
}

void
SubSurface::on_parent_destroy_cb(struct wl_listener *listener, void *data)
{
	std::fprintf(stderr, "SubSurface::on_parent_destroy_cb\n");
	SubSurface *self = util::container_of(listener, &SubSurface::on_parent_destroy_);

	std::fprintf(stderr, "Surface: %p\n", self->surface_);
	std::fprintf(stderr, "Parent: %p\n", self->surface_->parent_);

	self->on_parent_commit_.disconnect();

	self->surface_->parent_->children_above.remove(self->surface_);
	self->surface_->parent_->children_below.remove(self->surface_);

	self->surface_->parent_ = nullptr;
	self->surface_->subsurf_ = nullptr;

	self->surface_ = nullptr;
	wl_list_remove(&self->on_surface_destroy_.link);
}

void
SubSurface::on_surface_destroy_cb(struct wl_listener *listener, void *data)
{
	std::fprintf(stderr, "SubSurface::on_surface_destroy_cb\n");
	SubSurface *self = util::container_of(listener, &SubSurface::on_surface_destroy_);

	self->on_parent_commit_.disconnect();

	self->surface_->parent_->children_above.remove(self->surface_);
	self->surface_->parent_->children_below.remove(self->surface_);

	self->surface_->parent_ = nullptr;
	self->surface_->subsurf_ = nullptr;

	self->surface_ = nullptr;
	wl_list_remove(&self->on_parent_destroy_.link);
}

void
SubSurface::on_resource_destroy(struct wl_resource *resource)
{
	std::fprintf(stderr, "SubSurface::on_resource_destroy\n");
	SubSurface *self = static_cast<SubSurface *>(wl_resource_get_user_data(resource));

	delete self;
}

void
SubSurface::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
SubSurface::set_position(struct wl_client *client, struct wl_resource *resource, int32_t x, int32_t y)
{
	SubSurface *self = static_cast<SubSurface *>(wl_resource_get_user_data(resource));

	self->has_set_position = true;
	self->x_pos_ = x;
	self->y_pos_ = y;
}

void
SubSurface::place_above(struct wl_client *client, struct wl_resource *resource, struct wl_resource *sibling)
{
	SubSurface *self = static_cast<SubSurface *>(wl_resource_get_user_data(resource));
	Surface *sibl = static_cast<Surface *>(wl_resource_get_user_data(sibling));
	if (sibl == self->surface_) {
		wl_resource_post_error(resource, WL_SUBSURFACE_ERROR_BAD_SURFACE,
				       "cannot use self as reference surface");
		return;
	}

	if (sibl == self->surface_->parent_) {
		self->pending_placement.emplace(Placing::PlaceAbove, sibl);
	} else if (sibl->parent_ == self->surface_->parent_) {
		self->pending_placement.emplace(Placing::PlaceAbove, sibl);
	} else {
		wl_resource_post_error(resource, WL_SUBSURFACE_ERROR_BAD_SURFACE,
				       "cannot use non-sibling or non-parent "
				       "as reference surface");
	}
}

void
SubSurface::place_below(struct wl_client *client, struct wl_resource *resource, struct wl_resource *sibling)
{
	SubSurface *self = static_cast<SubSurface *>(wl_resource_get_user_data(resource));
	Surface *sibl = static_cast<Surface *>(wl_resource_get_user_data(sibling));
	if (sibl == self->surface_) {
		wl_resource_post_error(resource, WL_SUBSURFACE_ERROR_BAD_SURFACE,
				       "cannot use self as reference surface");
		return;
	}

	if (sibl == self->surface_->parent_) {
		self->pending_placement.emplace(Placing::PlaceBelow, sibl);
	} else if (sibl->parent_ == self->surface_->parent_) {
		self->pending_placement.emplace(Placing::PlaceBelow, sibl);
	} else {
		wl_resource_post_error(resource, WL_SUBSURFACE_ERROR_BAD_SURFACE,
				       "cannot use non-sibling or non-parent "
				       "as reference surface");
	}
}

void
SubSurface::set_sync(struct wl_client *client, struct wl_resource *resource)
{
	SubSurface *self = static_cast<SubSurface *>(wl_resource_get_user_data(resource));
	self->synchronized_ = true;
}

void
SubSurface::set_desync(struct wl_client *client, struct wl_resource *resource)
{
	SubSurface *self = static_cast<SubSurface *>(wl_resource_get_user_data(resource));
	self->synchronized_ = false;
}

} // namespace orbycomp
