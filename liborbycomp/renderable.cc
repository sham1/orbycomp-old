/*
 * renderable.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/base/renderable.hh>
#include <orbycomp/base/surface.hh>

namespace orbycomp
{

SurfaceRenderable::SurfaceRenderable(Surface *surface)
    : Renderable{surface->x(), surface->y(), 0, 0}, surface_{surface}
{
}

void
SurfaceRenderable::render_finish(uint32_t tv_msec)
{
	surface_->call_frame_cbs(tv_msec);
}

// TODO: Change to `operator<=>` when C++20 becomes "official".
static int
layer_compare(const Renderable::Layer &a, const Renderable::Layer &b)
{
	using Layer_Underlying = std::underlying_type<Renderable::Layer>::type;

	Layer_Underlying a_repr = static_cast<Layer_Underlying>(a);
	Layer_Underlying b_repr = static_cast<Layer_Underlying>(b);

	if (a_repr < b_repr) {
		return -1;
	}
	if (a_repr > b_repr) {
		return 1;
	}
	return 0;
}

bool
Renderable::Comparator::operator()(const Renderable &a, const Renderable &b) const
{
	if (&b == a.parent_) {
		return a.relative_pos_ < 0;
	}

	if (&a == b.parent_) {
		return b.relative_pos_ > 0;
	}

	if (a.parent_ && b.parent_) {
		if (a.parent_ == b.parent_) {
			return a.relative_pos_ < b.relative_pos_;
		} else {
			return Renderable::comparator_(*a.parent_, *b.parent_);
		}
	}

	if (a.parent_) {
		return Renderable::comparator_(*a.parent_, b);
	}

	if (b.parent_) {
		return Renderable::comparator_(a, *b.parent_);
	}

	auto layer_comparison = layer_compare(a.layer_, b.layer_);

	if (layer_comparison < 0) {
		return true;
	}
	if (layer_comparison > 0) {
		return false;
	}

	if (a.layer_ == Layer::Hidden) {
		// Since the comparison is `0`, we also know that
		// the layer of `b` is also `Hidden`.
		//
		// If both of the renderables are hidden, their relative
		// positioning doesn't matter.
		return false;
	}

	return a.relative_pos_ < b.relative_pos_;
}

}; // namespace orbycomp
