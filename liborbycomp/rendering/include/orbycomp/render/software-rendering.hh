/*
 * software-rendering.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <cstddef>
#include <cstdint>

namespace orbycomp
{

namespace rendering
{

namespace software
{

class BitmapSurface
{

public:
	struct BlitRegion {
		int32_t x;
		int32_t y;
		int32_t w;
		int32_t h;
	};

	BitmapSurface(std::uint8_t *source, std::size_t stride, int32_t width, int32_t height);

	~BitmapSurface() = default;

	void blit_to(
	    const BlitRegion &region, BitmapSurface &dest, int32_t x, int32_t y, bool has_alpha = true) const;

	int32_t
	width() const
	{
		return width_;
	}

	int32_t
	height() const
	{
		return height_;
	}

private:
	std::uint8_t *source_;
	std::size_t stride_;

	int32_t width_;
	int32_t height_;

	void blit_no_alpha(const BlitRegion &region, BitmapSurface &dest, int32_t x, int32_t y) const;
	void blit_alpha(const BlitRegion &region, BitmapSurface &dest, int32_t x, int32_t y) const;
};

} // namespace software

} // namespace rendering

} // namespace orbycomp
