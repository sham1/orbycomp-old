/*
 * shader.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>

#define EGL_NO_X11
#include <epoxy/egl.h>

namespace orbycomp
{

namespace opengl
{

class ShaderProgram;
class LinkedShaderProgram;

class Shader
{
	friend ShaderProgram;

	GLuint id_{0};

public:
	Shader(const std::string &source, GLenum type);
	~Shader();

	Shader(const Shader &) = delete;
	Shader &operator=(const Shader &) = delete;

	Shader(Shader &&other) = default;
	Shader &operator=(Shader &&) = default;
};

class ShaderProgram
{
	GLuint id_{0};

	std::vector<Shader> attached_shaders_{};

public:
	ShaderProgram();
	~ShaderProgram();

	ShaderProgram(const ShaderProgram &) = delete;
	ShaderProgram &operator=(const ShaderProgram &) = delete;

	ShaderProgram(ShaderProgram &&other) : id_{other.id_} { other.id_ = 0; }

	ShaderProgram &
	operator=(ShaderProgram &&other)
	{
		id_ = other.id_;
		other.id_ = 0;

		return *this;
	}

	ShaderProgram bind_attribute_location(GLuint index, const std::string &name) &&;

	ShaderProgram attach(Shader &&shader) &&;

	LinkedShaderProgram link() &&;
};

class LinkedShaderProgram
{
	friend ShaderProgram;

	GLuint id_{0};

	LinkedShaderProgram(GLuint &id) : id_{id} { id = 0; }

public:
	LinkedShaderProgram() = default;
	~LinkedShaderProgram();

	LinkedShaderProgram(const LinkedShaderProgram &) = delete;
	LinkedShaderProgram &operator=(const LinkedShaderProgram &) = delete;

	LinkedShaderProgram(LinkedShaderProgram &&other) : id_{other.id_} { other.id_ = 0; }

	LinkedShaderProgram &
	operator=(LinkedShaderProgram &&other)
	{
		id_ = other.id_;
		other.id_ = 0;

		return *this;
	}

	void activate();

	void deactivate();

	GLint uniform_location(const std::string &name);
};

} // namespace opengl

} // namespace orbycomp
