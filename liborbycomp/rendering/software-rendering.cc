/*
 * software-rendering.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <algorithm>
#include <cstdio>
#include <orbycomp/render/software-rendering.hh>

namespace orbycomp
{

namespace rendering
{

namespace software
{

BitmapSurface::BitmapSurface(std::uint8_t *source, std::size_t stride, int32_t width, int32_t height)
    : source_{source}, stride_{stride}, width_{width}, height_{height}
{
}

constexpr int32_t TILE_SIZE = 64;

void
BitmapSurface::blit_to(
    const BlitRegion &region, BitmapSurface &dest, int32_t x, int32_t y, bool has_alpha) const
{
	if (!has_alpha) {
		blit_no_alpha(region, dest, x, y);
	} else {
		blit_alpha(region, dest, x, y);
	}
}

void
BitmapSurface::blit_no_alpha(const BlitRegion &region, BitmapSurface &dest, int32_t x, int32_t y) const
{
	int32_t render_width = region.w;
	int32_t render_height = region.h;

	for (int32_t yprime = 0; yprime < render_height; yprime += TILE_SIZE) {
		for (int32_t xprime = 0; xprime < render_width; xprime += TILE_SIZE) {
			int32_t end_x = std::min(render_width, xprime + TILE_SIZE);
			int32_t end_y = std::min(render_height, yprime + TILE_SIZE);

			for (int32_t yoff = yprime; yoff < end_y; ++yoff) {
				int32_t src_begin_offset =
				    (region.y + yoff) * stride_ + (region.x + xprime) * 4;
				int32_t src_end_offset = (region.y + yoff) * stride_ + (region.x + end_x) * 4;
				int32_t dest_offset = (y + yoff) * dest.stride_ + (x + xprime) * 4;

				const std::uint8_t *src_begin = source_ + src_begin_offset;
				const std::uint8_t *src_end = source_ + src_end_offset;
				std::uint8_t *dest_base = dest.source_ + dest_offset;

				std::copy(src_begin, src_end, dest_base);
			}
		}
	}
}

void
BitmapSurface::blit_alpha(const BlitRegion &region, BitmapSurface &dest, int32_t x, int32_t y) const
{
	int32_t render_width = region.w;
	int32_t render_height = region.h;

	for (int32_t yprime = 0; yprime < render_height; yprime += TILE_SIZE) {
		for (int32_t xprime = 0; xprime < render_width; xprime += TILE_SIZE) {
			int32_t end_x = std::min(render_width, xprime + TILE_SIZE);
			int32_t end_y = std::min(render_height, yprime + TILE_SIZE);

			for (int32_t yoff = yprime; yoff < end_y; ++yoff) {
				for (int32_t xoff = xprime; xoff < end_x; ++xoff) {
					int32_t src_offset =
					    (region.y + yoff) * stride_ + (region.x + xoff) * 4;
					int32_t dest_offset = (y + yoff) * dest.stride_ + (x + xoff) * 4;

					const std::uint8_t *src_base = source_ + src_offset;
					std::uint8_t *dest_base = dest.source_ + dest_offset;

					float src_b = float(src_base[0]) / 255.0f;
					float src_g = float(src_base[1]) / 255.0f;
					float src_r = float(src_base[2]) / 255.0f;
					float src_a = float(src_base[3]) / 255.0f;

					float dest_b = float(dest_base[0]) / 255.0f;
					float dest_g = float(dest_base[1]) / 255.0f;
					float dest_r = float(dest_base[2]) / 255.0f;
					float dest_a = float(dest_base[3]) / 255.0f;

					float out_a = src_a + dest_a * (1 - src_a);
					float out_r = src_r + dest_r * (1 - src_a);
					float out_g = src_g + dest_g * (1 - src_a);
					float out_b = src_b + dest_b * (1 - src_a);

					dest_base[0] = uint8_t(out_b * 255);
					dest_base[1] = uint8_t(out_g * 255);
					dest_base[2] = uint8_t(out_r * 255);
					dest_base[3] = uint8_t(out_a * 255);
				}
			}
		}
	}
}

} // namespace software

} // namespace rendering

} // namespace orbycomp
