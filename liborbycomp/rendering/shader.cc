/*
 * shader.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/render/shader.hh>
#include <sstream>
#include <stdexcept>

namespace orbycomp
{

namespace opengl
{

Shader::Shader(const std::string &source, GLenum type) : id_{glCreateShader(type)}
{
	const char *source_ptr = source.c_str();
	glShaderSource(id_, 1, &source_ptr, nullptr);

	glCompileShader(id_);

	GLint compiled{GL_FALSE};
	glGetShaderiv(id_, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE) {
		GLint max_len{0};

		glGetShaderiv(id_, GL_INFO_LOG_LENGTH, &max_len);

		std::string error_str;
		error_str.resize(max_len);

		glGetShaderInfoLog(id_, max_len, &max_len, error_str.data());

		glDeleteShader(id_);
		id_ = 0;

		std::stringstream ss{};
		ss << "Shader compilation failed: " << error_str;

		throw std::runtime_error(ss.str());
	}
}

Shader::~Shader()
{
	if (id_ != 0) {
		glDeleteShader(id_);
	}
}

ShaderProgram::ShaderProgram() : id_{glCreateProgram()} {}

ShaderProgram
ShaderProgram::bind_attribute_location(GLuint index, const std::string &name) &&
{
	glBindAttribLocation(id_, index, name.c_str());

	return {std::move(*this)};
}

ShaderProgram
ShaderProgram::attach(Shader &&shader) &&
{
	glAttachShader(id_, shader.id_);
	attached_shaders_.push_back(std::move(shader));

	return {std::move(*this)};
}

LinkedShaderProgram
ShaderProgram::link() &&
{
	glLinkProgram(id_);

	GLint success{GL_FALSE};
	glGetProgramiv(id_, GL_LINK_STATUS, &success);
	if (success == GL_FALSE) {
		GLint max_len{0};

		glGetProgramiv(id_, GL_INFO_LOG_LENGTH, &max_len);

		std::string error_str;
		error_str.resize(max_len);

		glGetProgramInfoLog(id_, max_len, &max_len, error_str.data());

		std::stringstream ss{};
		ss << "Shader program linking failed: " << error_str;

		throw std::runtime_error(ss.str());
	}

	return LinkedShaderProgram(id_);
}

ShaderProgram::~ShaderProgram()
{
	for (auto &attached_shader : attached_shaders_) {
		glDetachShader(id_, attached_shader.id_);
	}

	if (id_ != 0) {
		glDeleteProgram(id_);
	}
}

LinkedShaderProgram::~LinkedShaderProgram()
{
	if (id_ != 0) {
		glDeleteProgram(id_);
	}
}

void
LinkedShaderProgram::activate()
{
	glUseProgram(id_);
}

void
LinkedShaderProgram::deactivate()
{
	glUseProgram(0);
}

GLint
LinkedShaderProgram::uniform_location(const std::string &name)
{
	return glGetUniformLocation(id_, name.c_str());
}

} // namespace opengl

} // namespace orbycomp
