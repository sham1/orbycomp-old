/*
 * xwm.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <array>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/xwayland.hh>
#include <orbycomp/xcursor.hh>
#include <orbycomp/xwm.hh>
#include <stdexcept>
#include <xcb/composite.h>

template <typename T, size_t N> static constexpr std::size_t array_len(T (&array)[N]) { return N; }

namespace orbycomp
{

static constexpr uint32_t frame_top_height = 32;
static constexpr uint32_t frame_edge_length = 5;

void
XWindowManager::on_xcb_event(EV_P_ struct ev_io *w, int revents)
{
	XWindowManager *self = static_cast<XWindowManager *>(w->data);
	int handle_count = 0;
	xcb_generic_event_t *event;
	while ((event = xcb_poll_for_event(self->x_conn_.get())) != nullptr) {
		switch (event->response_type & ~0x80) {
		case XCB_CREATE_NOTIFY:
			self->handle_create_notify(reinterpret_cast<xcb_create_notify_event_t *>(event));
			break;
		case XCB_DESTROY_NOTIFY:
			self->handle_destroy_notify(reinterpret_cast<xcb_destroy_notify_event_t *>(event));
			break;
		case XCB_MAP_REQUEST:
			self->handle_map_request(reinterpret_cast<xcb_map_request_event_t *>(event));
			break;
		case XCB_UNMAP_NOTIFY:
			self->handle_unmap_notify(reinterpret_cast<xcb_unmap_notify_event_t *>(event));
			break;
		case XCB_MAPPING_NOTIFY:
			break;
		case XCB_CLIENT_MESSAGE:
			self->handle_client_message(reinterpret_cast<xcb_client_message_event_t *>(event));
			break;
		case XCB_PROPERTY_NOTIFY:
			self->handle_property_notify(reinterpret_cast<xcb_property_notify_event_t *>(event));
			break;
		case XCB_CONFIGURE_REQUEST:
			self->handle_configure_request(
			    reinterpret_cast<xcb_configure_request_event_t *>(event));
			break;
		case XCB_CONFIGURE_NOTIFY:
			self->handle_configure_notify(
			    reinterpret_cast<xcb_configure_notify_event_t *>(event));
			break;
		case XCB_EXPOSE:
			self->handle_expose(reinterpret_cast<xcb_expose_event_t *>(event));
			break;
		default:
			std::fprintf(stderr, "Unknown event (%d)\n", event->response_type);
		}
		free(event);
		++handle_count;
	}

	if (handle_count > 0) {
		xcb_flush(self->x_conn_.get());
	}
}

XWindowManager::XWindowManager(XWaylandCtx *x_ctx)
    : x_ctx_{x_ctx}, x_conn_{xcb_connect_to_fd(x_ctx_->wm_fd_, nullptr)}, shell_{
									      &x_ctx_->compositor_->shell()}
{
	// We have created our X connection. We still need to check if
	// we've succeeded in connecting.
	if (xcb_connection_has_error(x_conn_.get())) {
		throw std::runtime_error("X server connection failed");
	}

	ev_io_init(&on_xcb_event_listener_, &XWindowManager::on_xcb_event, x_ctx_->wm_fd_, EV_READ);
	on_xcb_event_listener_.data = this;
	ev_io_start(x_ctx_->compositor_->loop(), &on_xcb_event_listener_);

	xcb_screen_iterator_t s = xcb_setup_roots_iterator(xcb_get_setup(x_conn_.get()));
	screen_ = s.data;

	uint32_t root_event_mask = XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT |
				   XCB_EVENT_MASK_PROPERTY_CHANGE;
	xcb_change_window_attributes(x_conn_.get(), screen_->root, XCB_CW_EVENT_MASK, &root_event_mask);
	xcb_flush(x_conn_.get());

	// Get our colormap
	create_color_map();
	xcb_flush(x_conn_.get());

	// Get all the resources we need.
	get_resources();

	xcb_flush(x_conn_.get());

	// Tell Xwayland that we will handle the subwindows properly.
	xcb_composite_redirect_subwindows(x_conn_.get(), screen_->root, XCB_COMPOSITE_REDIRECT_MANUAL);

	// After the ICCCM window manager selection S0 is created,
	// XWayland knows that we're done, and starts doing its thing.
	create_wm_window();

	std::fprintf(stderr, "Orbycomp X11 WM created\n");

	// Connect to the compositor's "surface created" signal so we may
	// check whether a new surface is related to any of our managed
	// X windows.
	on_surface_signal_conn_ = x_ctx->compositor_->on_surface_create_signal.connect(
	    [this](Surface &s) { this->on_surface_created(s); });
}

void
XWindowManager::create_color_map()
{
	xcb_depth_iterator_t depth_iter;
	xcb_visualtype_iterator_t visualtype_iter;
	xcb_visualtype_t *visualtype{nullptr};

	depth_iter = xcb_screen_allowed_depths_iterator(screen_);
	while (depth_iter.rem > 0) {
		if (depth_iter.data->depth == 32) {
			visualtype_iter = xcb_depth_visuals_iterator(depth_iter.data);
			visualtype = visualtype_iter.data;
			break;
		}

		xcb_depth_next(&depth_iter);
	}

	if (!visualtype) {
		std::fprintf(stderr, "No 32 bit visualtype\n");
		return;
	}

	visual_id_ = visualtype->visual_id;
	colormap_ = xcb_generate_id(x_conn_.get());
	xcb_create_colormap(x_conn_.get(), XCB_COLORMAP_ALLOC_NONE, colormap_, screen_->root, visual_id_);
}

void
XWindowManager::get_resources()
{
	xcb_connection_t *conn = x_conn_.get();

	const char *atom_names[] = {
	    "WM_PROTOCOLS",
	    "WM_S0",
	    "WM_TRANSIENT_FOR",
	    "WM_STATE",
	    "WM_DELETE_WINDOW",

	    "WL_SURFACE_ID",
	    "UTF8_STRING",

	    "_NET_WM_CM_S0",
	    "_NET_SUPPORTING_WM_CHECK",
	    "_NET_WM_NAME",
	    "_NET_WM_PID",

	    "_NET_WM_WINDOW_TYPE",
	    "_NET_WM_WINDOW_TYPE_DESKTOP",
	    "_NET_WM_WINDOW_TYPE_DOCK",
	    "_NET_WM_WINDOW_TYPE_TOOLBAR",
	    "_NET_WM_WINDOW_TYPE_MENU",
	    "_NET_WM_WINDOW_TYPE_UTILITY",
	    "_NET_WM_WINDOW_TYPE_SPLASH",
	    "_NET_WM_WINDOW_TYPE_DIALOG",
	    "_NET_WM_WINDOW_TYPE_DROPDOWN_MENU",
	    "_NET_WM_WINDOW_TYPE_POPUP_MENU",
	    "_NET_WM_WINDOW_TYPE_TOOLTIP",
	    "_NET_WM_WINDOW_TYPE_NOTIFICATION",
	    "_NET_WM_WINDOW_TYPE_COMBO",
	    "_NET_WM_WINDOW_TYPE_DND",
	    "_NET_WM_WINDOW_TYPE_NORMAL",

	    "_NET_WM_STATE",
	    "_NET_WM_STATE_MODAL",
	    "_NET_WM_STATE_STICKY",
	    "_NET_WM_STATE_MAXIMIZED_VERT",
	    "_NET_WM_STATE_MAXIMIZED_HORZ",
	    "_NET_WM_STATE_SHARED",
	    "_NET_WM_STATE_SKIP_TASKBAR",
	    "_NET_WM_STATE_SKIP_PAGER",
	    "_NET_WM_STATE_HIDDEN",
	    "_NET_WM_STATE_FULLSCREEN",
	    "_NET_WM_STATE_ABOVE",
	    "_NET_WM_STATE_BELOW",
	    "_NET_WM_STATE_DEMANDS_ATTENTION",
	    "_NET_WM_STATE_FOCUSED",

	    "_NET_WM_ALLOWED_ACTIONS",
	    "_NET_WM_ACTION_MOVE",
	    "_NET_WM_ACTION_RESIZE",
	    "_NET_WM_ACTION_MINIMIZE",
	    "_NET_WM_ACTION_SHADE",
	    "_NET_WM_ACTION_STICK",
	    "_NET_WM_ACTION_MAXIMIZE_HORZ",
	    "_NET_WM_ACTION_MAXIMIZE_VERT",
	    "_NET_WM_ACTION_FULLSCREEN",
	    "_NET_WM_ACTION_CHANGE_DESKTOP",
	    "_NET_WM_ACTION_CLOSE",
	    "_NET_WM_ACTION_ABOVE",
	    "_NET_WM_ACTION_BELOW",

	    "_XWAYLAND_ALLOW_COMMITS",
	};

	std::vector<std::pair<const char *, xcb_intern_atom_cookie_t>> cookies{};
	for (size_t i = 0; i < array_len(atom_names); ++i) {
		const char *name = atom_names[i];
		cookies.emplace_back(name, xcb_intern_atom(conn, false, strlen(name), name));
	}

	for (auto &[name, cookie] : cookies) {
		auto *reply = xcb_intern_atom_reply(conn, cookie, nullptr);
		atoms_.emplace(name, reply->atom);
		free(reply);
	}

	xcb_prefetch_extension_data(conn, &xcb_composite_id);

	xcb_render_query_pict_formats_cookie_t formats_cookie = xcb_render_query_pict_formats(conn);

	xcb_render_query_pict_formats_reply_t *formats_reply =
	    xcb_render_query_pict_formats_reply(conn, formats_cookie, nullptr);

	if (formats_reply) {
		xcb_render_pictforminfo_t *formats = xcb_render_query_pict_formats_formats(formats_reply);
		for (size_t i = 0; i < formats_reply->num_formats; ++i) {
			auto format = formats[i];
			if (format.direct.red_mask != 0xFF && format.direct.red_shift != 16) {
				continue;
			}
			if (format.type == XCB_RENDER_PICT_TYPE_DIRECT && format.depth == 32 &&
			    format.direct.alpha_mask == 0xFF && format.direct.alpha_shift == 24) {
				format_rgba_ = format;
			}
		}
	}

	free(formats_reply);

	get_default_cursor();
}

void
XWindowManager::get_default_cursor()
{
	xcb_connection_t *conn = x_conn_.get();

	XcursorImage *cimg = shell_->compositor()->cursor_ctx().get_cursor("left_ptr");

	uint32_t stride = cimg->width * 4;

	xcb_pixmap_t cursor_pix = xcb_generate_id(conn);
	xcb_create_pixmap(conn, 32, cursor_pix, screen_->root, cimg->width, cimg->height);

	xcb_render_picture_t pic = xcb_generate_id(conn);
	xcb_render_create_picture(conn, pic, cursor_pix, format_rgba_.id, 0, 0);

	xcb_gcontext_t gc = xcb_generate_id(conn);
	xcb_create_gc(conn, gc, cursor_pix, 0, 0);

	xcb_put_image(conn, XCB_IMAGE_FORMAT_Z_PIXMAP, cursor_pix, gc, cimg->width, cimg->height, 0, 0, 0, 32,
		      stride * cimg->height, reinterpret_cast<uint8_t *>(cimg->pixels));
	xcb_free_gc(conn, gc);

	xcb_cursor_t cursor = xcb_generate_id(conn);
	xcb_render_create_cursor(conn, cursor, pic, cimg->xhot, cimg->yhot);

	xcb_render_free_picture(conn, pic);
	xcb_free_pixmap(conn, cursor_pix);

	xcb_change_window_attributes(conn, screen_->root, XCB_CW_CURSOR, &cursor);
	xcb_flush(conn);
}

void
XWindowManager::create_wm_window()
{
	xcb_connection_t *conn = x_conn_.get();
	static constexpr char name[] = "orbycomp";

	wm_window_ = xcb_generate_id(conn);
	xcb_create_window(conn, XCB_COPY_FROM_PARENT, wm_window_, screen_->root, 0, 0, 1, 1, 0,
			  XCB_WINDOW_CLASS_INPUT_OUTPUT, screen_->root_visual, 0, nullptr);

	xcb_change_property(conn, XCB_PROP_MODE_REPLACE, wm_window_, atoms_["_NET_SUPPORTING_WM_CHECK"],
			    XCB_ATOM_WINDOW, 32, 1, &wm_window_);
	xcb_change_property(conn, XCB_PROP_MODE_REPLACE, wm_window_, atoms_["_NET_WM_NAME"],
			    atoms_["UTF8_STRING"], 8, strlen(name), name);

	xcb_change_property(conn, XCB_PROP_MODE_REPLACE, screen_->root, atoms_["_NET_SUPPORTING_WM_CHECK"],
			    XCB_ATOM_WINDOW, 32, 1, &wm_window_);

	// Claim WM_S0 so XWayland knows that we're ready.
	xcb_set_selection_owner(conn, wm_window_, atoms_["WM_S0"], XCB_TIME_CURRENT_TIME);
	xcb_set_selection_owner(conn, wm_window_, atoms_["_NET_WM_CM_S0"], XCB_TIME_CURRENT_TIME);

	xcb_flush(conn);
}

XWindowManager::~XWindowManager()
{
	if (ev_is_active(&on_xcb_event_listener_)) {
		ev_io_stop(x_ctx_->compositor_->loop(), &on_xcb_event_listener_);
	}
}

bool
XWindowManager::is_our_resource(uint32_t id)
{
	const xcb_setup_t *setup = xcb_get_setup(x_conn_.get());
	return (id & (~setup->resource_id_mask)) == setup->resource_id_base;
}

void
XWindowManager::handle_create_notify(struct xcb_create_notify_event_t *event)
{
	if (is_our_resource(event->window)) {
		// We don't need to care about our own resources (like frames)
		return;
	}

	create_window(event->window, event->width, event->height, event->x, event->y,
		      event->override_redirect);
}

void
XWindowManager::handle_destroy_notify(struct xcb_destroy_notify_event_t *event)
{
	if (is_our_resource(event->window)) {
		// We don't care about our own resources (like frames)
		return;
	}

	if (find_window(event->window) == windows_.end()) {
		// If we cannot find this window from the managed windows
		// it's probably not important.
		return;
	}

	std::fprintf(stderr, "XWindow 0x%x destroyed!\n", event->window);

	destroy_window(event->window);
}

void
XWindowManager::create_window(
    xcb_window_t window, int width, int height, int x, int y, bool override_redirect)
{
	xcb_connection_t *conn = x_conn_.get();

	fprintf(stderr, "Window with id 0x%x created\n", window);

	XWindow &wnd = windows_.try_emplace(window, this, window).first->second;

	// We want to check for whether this window has any alpha in it by
	// checking its geometry and thus depth.
	xcb_get_geometry_cookie_t geom_cookie = xcb_get_geometry(conn, window);

	uint32_t event_mask = XCB_EVENT_MASK_PROPERTY_CHANGE | XCB_EVENT_MASK_FOCUS_CHANGE;
	xcb_change_window_attributes(conn, window, XCB_CW_EVENT_MASK, &event_mask);

	wnd.props_dirty = true;
	wnd.override_redirect = override_redirect;
	wnd.width = width;
	wnd.height = height;
	wnd.x = x;
	wnd.y = y;

	xcb_get_geometry_reply_t *geom_reply = xcb_get_geometry_reply(conn, geom_cookie, nullptr);
	// To check whether we have alpha, we really ought to check XRender,
	// but for all known cases, checking the geometry and the depth
	// ought to work.
	if (geom_reply) {
		wnd.has_alpha = geom_reply->depth == 32;
	}
	free(geom_reply);
}

void
XWindowManager::destroy_window(xcb_window_t window)
{
	xcb_connection_t *conn = x_conn_.get();
	XWindow &wnd = find_window(window)->second;

	if (wnd.frame_id != XCB_WINDOW_NONE) {
		// If we had a frame, we must remove the inner window from the
		// frame and destroy the frame.
		xcb_reparent_window(conn, wnd.id, wm_window_, 0, 0);
		wnd.frame_id = XCB_WINDOW_NONE;

		wnd.send_wm_state(XWindow::ICCCMState::Withdrawn);
	}

	windows_.erase(window);
}

void
XWindowManager::handle_map_request(struct xcb_map_request_event_t *event)
{
	xcb_connection_t *conn = x_conn_.get();

	if (is_our_resource(event->window)) {
		return;
	}

	auto wnd_pos = find_window(event->window);
	if (wnd_pos == windows_.end()) {
		return;
	}
	XWindow &wnd = wnd_pos->second;

	std::fprintf(stderr, "MapRequest\n");

	wnd.read_properties();

	wnd.send_wm_state(XWindow::ICCCMState::Normal);
	wnd.set_net_wm_state();

	// If we already have a frame, don't create one again.
	if (wnd.frame_id != XCB_WINDOW_NONE) {
		return;
	}

	uint32_t values[3];
	values[0] = screen_->black_pixel;
	values[1] = XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_ENTER_WINDOW |
		    XCB_EVENT_MASK_LEAVE_WINDOW | XCB_EVENT_MASK_EXPOSURE |
		    XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT;
	values[2] = colormap_;

	wnd.frame_id = xcb_generate_id(conn);
	xcb_create_window(conn, 32, wnd.frame_id, screen_->root, 0, 0, wnd.width + frame_edge_length * 2,
			  wnd.height + frame_top_height + frame_edge_length, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT,
			  visual_id_, XCB_CW_BORDER_PIXEL | XCB_CW_EVENT_MASK | XCB_CW_COLORMAP, values);

	xcb_reparent_window(conn, wnd.id, wnd.frame_id, frame_edge_length, frame_top_height);

	xcb_map_window(conn, wnd.id);
	xcb_map_window(conn, wnd.frame_id);
}

void
XWindowManager::handle_unmap_notify(struct xcb_unmap_notify_event_t *event)
{
	std::fprintf(stderr, "UnmapNotify\n");

	xcb_connection_t *conn = x_conn_.get();

	if (is_our_resource(event->window)) {
		return;
	}

	auto wnd_pos = find_window(event->window);
	if (wnd_pos == windows_.end()) {
		return;
	}
	XWindow &wnd = wnd_pos->second;
	std::fprintf(stderr, "XWindow 0x%x unmapped\n", event->window);

	if (wnd.frame_id != XCB_WINDOW_NONE) {
		xcb_unmap_window(conn, wnd.frame_id);
		xcb_destroy_window(conn, wnd.frame_id);
		wnd.frame_id = XCB_WINDOW_NONE;
	}

	wnd.send_wm_state(XWindow::ICCCMState::Withdrawn);
	wnd.mapped_ = false;

	xcb_unmap_window(conn, wnd.id);
}

static const char *
get_atom_name(xcb_connection_t *conn, xcb_atom_t atom)
{
	xcb_get_atom_name_cookie_t cookie;
	xcb_get_atom_name_reply_t *reply;
	xcb_generic_error_t *e;
	static char buffer[64];

	if (atom == XCB_ATOM_NONE) {
		return "None";
	}

	cookie = xcb_get_atom_name(conn, atom);
	reply = xcb_get_atom_name_reply(conn, cookie, &e);

	if (reply) {
		snprintf(buffer, sizeof(buffer), "%.*s", xcb_get_atom_name_name_length(reply),
			 xcb_get_atom_name_name(reply));
	} else {
		snprintf(buffer, sizeof(buffer), "(atom %u)", atom);
	}

	free(reply);
	return buffer;
}

void
XWindowManager::handle_client_message(struct xcb_client_message_event_t *event)
{
	xcb_connection_t *conn = x_conn_.get();
	std::fprintf(stderr, "ClientMessage (%s %d %d %d %d %d win 0x%x)\n", get_atom_name(conn, event->type),
		     event->data.data32[0], event->data.data32[1], event->data.data32[2],
		     event->data.data32[3], event->data.data32[4], event->window);

	auto wnd_pos = find_window(event->window);
	if (wnd_pos == windows_.end()) {
		return;
	}

	XWindow &wnd = wnd_pos->second;

	// TODO: Add all other possible message types
	if (event->type == atoms_["WL_SURFACE_ID"]) {
		std::fprintf(stderr, "WL_SURFACE_ID received!\n");
		handle_surface_id(wnd, event);
	}
	if (event->type == atoms_["_NET_WM_STATE"]) {
		handle_net_wm_state(wnd, event);
	}
}

void
XWindowManager::handle_surface_id(XWindow &wnd, struct xcb_client_message_event_t *event)
{
	uint32_t surface_id = event->data.data32[0];
	// XWayland will send the request for creating the surface
	// before sending this, but we might still end up handling this
	// message before that is processed by the rest of the compositor.
	//
	// If this happens, we postpone the process of mapping the surface
	// and wait until both parts are done.
	struct wl_resource *res = wl_client_get_object(x_ctx_->xwayland_client_, surface_id);
	if (res) {
		// We have our surface. Display the renderable, if it exists.
		Surface *surface = static_cast<Surface *>(wl_resource_get_user_data(res));
		wnd.associate_surface(surface);
	} else {
		// The surface doesn't exist yet. When it becomes a thing,
		// set it and map it.
		fprintf(stderr, "XWindow %x does not have a surface, yet!\n", wnd.id);
		unassociated_surfaces_.try_emplace(surface_id, &wnd);
	}
}

void
XWindowManager::handle_net_wm_state(XWindow &wnd, struct xcb_client_message_event_t *event)
{
	std::fprintf(stderr, "_NET_WM_STATE received!\n");
	std::fprintf(stderr, "\tWith action: ");
	switch (event->data.data32[0]) {
	case 0:
		std::fprintf(stderr, "Remove");
		break;
	case 1:
		std::fprintf(stderr, "Add");
		break;
	case 2:
		std::fprintf(stderr, "Toggle");
		break;
	default:
		std::fprintf(stderr, "Uknown action");
		break;
	}
	std::fprintf(stderr, "\n");

	std::fprintf(stderr, "Property 1: %s\n", get_atom_name(x_conn_.get(), event->data.data32[1]));
	std::fprintf(stderr, "Property 2: %s\n", get_atom_name(x_conn_.get(), event->data.data32[2]));

	// TODO: Deal with all possible _NET_WM_STATE atoms that may be set
	// by clients.
	wnd.set_net_wm_state();
}

void
XWindowManager::handle_property_notify(struct xcb_property_notify_event_t *event)
{
	auto wnd_pos = find_window(event->window);
	if (wnd_pos == windows_.end()) {
		return;
	}

	XWindow &wnd = wnd_pos->second;
	wnd.props_dirty = true;
}

void
XWindowManager::on_surface_created(Surface &surface)
{
	auto *surface_res = surface.resource();
	struct wl_client *surface_client = wl_resource_get_client(surface_res);
	if (surface_client == x_ctx_->xwayland_client_) {
		uint32_t sid = wl_resource_get_id(surface_res);
		// We now know that the surface is from XWayland.
		// Now check if it's any of the unassociated surfaces.
		auto surface_pos = unassociated_surfaces_.find(sid);
		if (surface_pos == unassociated_surfaces_.end()) {
			// It's not. Bail.
			return;
		}
		XWindow *wnd = surface_pos->second;
		unassociated_surfaces_.erase(surface_pos);

		wnd->associate_surface(&surface);
	}
}

void
XWindowManager::XWindow::read_properties()
{
	if (!props_dirty) {
		return;
	}
	props_dirty = false;

	auto *conn = manager->x_conn_.get();

	const struct {
		const char *atom_name;
		std::function<void(xcb_get_property_reply_t *)> func;
		std::function<void(void)> no_atom_func;
	} read_props[] = {
	    {"WM_TRANSIENT_FOR",
	     [this](xcb_get_property_reply_t *reply) {
		     auto *manager = this->manager;
		     xcb_window_t wid = *static_cast<xcb_window_t *>(xcb_get_property_value(reply));
		     auto wnd_pos = manager->find_window(wid);
		     if (wnd_pos == manager->windows_.end()) {
			     std::fprintf(stderr,
					  "Window 0x%x is transient to an "
					  "unknown window 0x%x!\n",
					  this->id, wid);
			     this->transient_for_ = nullptr;
			     return;
		     }
		     XWindow &wnd = wnd_pos->second;
		     this->transient_for_ = &wnd;
	     },
	     [this]() { this->transient_for_ = nullptr; }},
	    {"WM_PROTOCOLS",
	     [this](xcb_get_property_reply_t *reply) {
		     auto *manager = this->manager;
		     xcb_atom_t *atoms = static_cast<xcb_atom_t *>(xcb_get_property_value(reply));
		     size_t len = xcb_get_property_value_length(reply) / sizeof(*atoms);
		     for (size_t i = 0; i < len; ++i) {
			     xcb_atom_t atom = atoms[i];
			     if (atom == manager->atoms_["WM_DELETE_WINDOW"]) {
				     this->has_wm_delete_window_ = true;
			     }
		     }
	     },
	     [this]() { this->has_wm_delete_window_ = false; }},
	    {"_NET_WM_WINDOW_TYPE",
	     [this](xcb_get_property_reply_t *reply) {
		     auto *manager = this->manager;
		     xcb_atom_t type_atom = *static_cast<xcb_atom_t *>(xcb_get_property_value(reply));
		     if (type_atom == manager->atoms_["_NET_WM_WINDOW_TYPE_NORMAL"] &&
			 this->state_.index() != 1) {
			     this->state_.emplace<1>(this);
		     }
		     if (type_atom == manager->atoms_["_NET_WM_WINDOW_TYPE_DIALOG"]) {
			     if (this->state_.index() != 2) {
				     auto &p = this->state_.emplace<2>(this, this->transient_for_);
				     p.is_dialog_ = true;
			     }
		     } else if (this->manager->is_popup(type_atom)) {
			     if (this->state_.index() != 2) {
				     auto &p = this->state_.emplace<2>(this, this->transient_for_);
				     p.is_dialog_ = false;
			     }
		     }
	     },
	     [this]() {
		     // From EWMH:
		     //
		     // _NET_WM_TYPE_NORMAL:
		     // Managed windows with neither _NET_WM_WINDOW_TYPE nor
		     // WM_TRANSIENT_FOR set MUST be taken as this type.
		     // Override-redirect windows without _NET_WM_WINDOW_TYPE,
		     // must be taken as this type whether or not they have
		     // WM_TRANSIENT_FOR set.
		     //
		     // _NET_WM_TYPE_DIALOG:
		     // If _NET_WM_WINDOW_TYPE is not set, then managed windows
		     // with WM_TRANSIENT_FOR set MUST be taken as this type.
		     if (this->override_redirect) {
			     if (this->state_.index() != 1) {
				     this->state_.emplace<1>(this);
			     }
		     } else {
			     if (!this->transient_for_) {
				     if (this->state_.index() != 1) {
					     this->state_.emplace<1>(this);
				     }
			     } else {
				     if (this->state_.index() != 2) {
					     this->state_.emplace<2>(this, this->transient_for_);
				     }
			     }
		     }
	     }},
	    {"_NET_WM_STATE",
	     [this](xcb_get_property_reply_t *reply) {
		     auto *manager = this->manager;
		     xcb_atom_t *atoms = static_cast<xcb_atom_t *>(xcb_get_property_value(reply));
		     size_t len = reply->value_len;
		     for (size_t i = 0; i < len; ++i) {
			     xcb_atom_t atom = atoms[i];
			     if (atom == manager->atoms_["_NET_WM_STATE_MAXIMIZED_VERT"]) {
				     this->maximized_vert_ = true;
			     }

			     if (atom == manager->atoms_["_NET_WM_STATE_MAXIMIZED_HORZ"]) {
				     this->maximized_horz_ = true;
			     }
		     }

		     this->set_net_wm_state();
	     },
	     [this]() { this->set_net_wm_state(); }},
	};
	std::array<xcb_get_property_cookie_t, array_len(read_props)> cookies{};
	for (size_t i = 0; i < array_len(read_props); ++i) {
		xcb_atom_t atom = manager->atoms_[read_props[i].atom_name];
		cookies[i] = xcb_get_property(conn, 0, id, atom, XCB_ATOM_ANY, 0, 2048);
	}

	std::vector<const std::function<void(void)> *> not_found_prop_cbs{};
	for (size_t i = 0; i < array_len(read_props); ++i) {
		xcb_get_property_reply_t *reply = xcb_get_property_reply(conn, cookies[i], nullptr);
		if (!reply) {
			// A bad window, usually.
			continue;
		}
		if (reply->type == XCB_ATOM_NONE) {
			// Property not found, using default callback
			not_found_prop_cbs.emplace_back(&read_props[i].no_atom_func);
			free(reply);
			continue;
		}

		// Property found. Calling necessary callback.
		read_props[i].func(reply);
		free(reply);
	}

	for (const auto cb : not_found_prop_cbs) {
		(*cb)();
	}
}

void
XWindowManager::handle_configure_request(struct xcb_configure_request_event_t *event)
{
	fprintf(stderr, "ConfigureRequest for window 0x%x\n", event->window);

	auto wnd_pos = find_window(event->window);
	if (wnd_pos == windows_.end()) {
		return;
	}

	xcb_connection_t *conn = x_conn_.get();

	XWindow &wnd = wnd_pos->second;

	bool size_changed{false};
	if (!wnd.is_maximized()) {
		if (event->value_mask & XCB_CONFIG_WINDOW_WIDTH) {
			wnd.width = event->width;
			size_changed = true;
		}
		if (event->value_mask & XCB_CONFIG_WINDOW_HEIGHT) {
			wnd.height = event->height;
			size_changed = true;
		}

		if (size_changed && wnd.frame_id != XCB_WINDOW_NONE) {
			wnd.set_commit_allowance(false);

			uint32_t frame_vals[2];
			frame_vals[0] = wnd.width + frame_edge_length * 2;
			frame_vals[1] = wnd.height + frame_edge_length + frame_top_height;
			xcb_configure_window(conn, wnd.frame_id,
					     XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, frame_vals);
		}
	}

	size_t i = 0;

	uint32_t values[16];
	uint16_t mask;

	values[i++] = wnd.x;
	values[i++] = wnd.y;
	values[i++] = wnd.width;
	values[i++] = wnd.height;
	values[i++] = 0;

	if (wnd.frame_id != XCB_WINDOW_NONE) {
		values[0] = frame_edge_length;
		values[1] = frame_top_height;
	}

	mask = XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH |
	       XCB_CONFIG_WINDOW_HEIGHT | XCB_CONFIG_WINDOW_BORDER_WIDTH;
	if (event->value_mask & XCB_CONFIG_WINDOW_SIBLING) {
		values[i++] = event->sibling;
		mask |= XCB_CONFIG_WINDOW_SIBLING;
	}
	if (event->value_mask & XCB_CONFIG_WINDOW_STACK_MODE) {
		values[i++] = event->stack_mode;
		mask |= XCB_CONFIG_WINDOW_STACK_MODE;
	}

	xcb_configure_window(conn, wnd.id, mask, values);
	wnd.set_commit_allowance(true);
}

void
XWindowManager::handle_configure_notify(struct xcb_configure_notify_event_t *event)
{
	auto wnd_pos = find_window(event->window);
	if (wnd_pos == windows_.end()) {
		return;
	}

	std::fprintf(stderr, "ConfigureNotify for XWindow 0x%x\n", event->window);

	XWindow &wnd = wnd_pos->second;
	std::fprintf(stderr, "Window frame ID: 0x%x\n", wnd.frame_id);

	if (wnd.frame_id != XCB_WINDOW_NONE && event->window != wnd.frame_id) {
		return;
	}

	wnd.x = event->x;
	wnd.y = event->y;
	if (wnd.surface_) {
		wnd.surface_->x() = wnd.x;
		wnd.surface_->y() = wnd.y;
		if (wnd.surface_->renderable()) {
			wnd.surface_->renderable()->update();
		}
	}
}

void
XWindowManager::handle_expose(struct xcb_expose_event_t *event)
{
	// Check if we are a frame.
	auto wnd_pos = std::find_if(windows_.begin(), windows_.end(), [event](const auto &p) {
		const XWindow &wnd = p.second;
		return wnd.frame_id == event->window;
	});

	if (wnd_pos == windows_.end()) {
		// Exposure was not for the frame window.
		return;
	}

	std::fprintf(stderr, "Expose\n");

	const xcb_window_t frame_id = wnd_pos->second.frame_id;
	// TODO: Draw
	(void) frame_id;
}

bool
XWindowManager::is_popup(xcb_atom_t atom)
{
	return atom == atoms_["_NET_WM_WINDOW_TYPE_TOOLTIP"] ||
	       atom == atoms_["_NET_WM_WINDOW_TYPE_DROPDOWN_MENU"] ||
	       atom == atoms_["_NET_WM_WINDOW_TYPE_DND"] || atom == atoms_["_NET_WM_WINDOW_TYPE_COMBO"] ||
	       atom == atoms_["_NET_WM_WINDOW_TYPE_POPUP_MENU"] ||
	       atom == atoms_["_NET_WM_WINDOW_TYPE_UTILITY"];
}

void
XWindowManager::XWindow::map()
{
}

void
XWindowManager::XWindow::unmap()
{
}

void
XWindowManager::XWindow::send_wm_state(ICCCMState state)
{
	xcb_connection_t *conn = manager->x_conn_.get();

	uint32_t props[2];
	props[0] = static_cast<uint32_t>(state);
	props[1] = XCB_WINDOW_NONE;

	xcb_change_property(conn, XCB_PROP_MODE_REPLACE, id, manager->atoms_["WM_STATE"],
			    manager->atoms_["WM_STATE"], 32, 2, props);
}

orbycomp::shell::Toplevel *
XWindowManager::XWindow::toplevel()
{
	return std::get_if<Toplevel>(&state_);
}

orbycomp::shell::Popup *
XWindowManager::XWindow::popup()
{
	return std::get_if<Popup>(&state_);
}

void
XWindowManager::XWindow::on_surface_destroy_cb(struct wl_listener *listener, void *data)
{
	XWindow *self = util::container_of(listener, &XWindow::on_surface_destroy_);

	self->state_.emplace<0>();

	wl_list_remove(&self->on_surface_destroy_.link);
	self->surface_ = nullptr;
	self->surface_resource_ = nullptr;
}

void
XWindowManager::XWindow::associate_surface(orbycomp::Surface *surface)
{
	if (surface_) {
		wl_list_remove(&on_surface_destroy_.link);
		surface_ = nullptr;
		surface_resource_ = nullptr;
	}

	surface_ = surface;
	surface_resource_ = surface_->resource();

	on_surface_destroy_.notify = XWindow::on_surface_destroy_cb;
	wl_resource_add_destroy_listener(surface_resource_, &on_surface_destroy_);

	// If we have an override-redirect window, we might have not had
	// a chance to read these before. So do it here.
	read_properties();

	fprintf(stderr, "XWindow %x has a surface!\n", id);

	if (transient_for_ && transient_for_->surface_) {
		if (auto *popup = std::get_if<Popup>(&state_)) {
			if (!popup->is_dialog_) {
				surface_->x() = x;
				surface_->y() = y;

				// TODO: Add ability to "parent" surfaces.

				std::fprintf(stderr,
					     "Relative offset in transient "
					     "window: %d, %d\n",
					     x - transient_for_->x, y - transient_for_->y);
			}
		}
	} else {
		surface_->x() = x;
		surface_->y() = y;
	}

	if (surface_->renderable()) {
		if (this->override_redirect) {
			// If we are override redirect, we should just
			// render as-is.
			auto *r = surface_->renderable();
			r->relative_pos() = std::numeric_limits<std::int32_t>::max();
			r->layer() = Renderable::Layer::Window;
			r->update();
			this->mapped_ = true;
		} else {
			if (this->state_.index() == 1) {
				auto *r = surface_->renderable();
				r->layer() = Renderable::Layer::Window;
				r->update();

				Toplevel &t = std::get<1>(this->state_);
				this->manager->shell_->add_window(&t);

				this->mapped_ = true;
			} else if (this->state_.index() == 2) {
				auto *r = surface_->renderable();
				r->layer() = Renderable::Layer::Window;
				r->update();

				Popup &t = std::get<2>(this->state_);
				this->manager->shell_->add_popup(&t);

				this->mapped_ = true;
			}
		}
	}

	surface_->on_commit_signal_.connect([this](orbycomp::Surface *s) {
		if (!this->mapped_) {
			if (s->renderable()) {
				if (this->override_redirect) {
					// If we are override redirect, we
					// should just render as-is.
					auto *r = surface_->renderable();
					r->layer() = Renderable::Layer::Window;
					r->relative_pos() = std::numeric_limits<std::int32_t>::max();
					r->update();
					this->mapped_ = true;
				} else if (this->state_.index() == 1) {
					auto *r = s->renderable();
					r->layer() = Renderable::Layer::Window;
					r->update();

					Toplevel &t = std::get<1>(this->state_);
					this->manager->shell_->add_window(&t);

					this->mapped_ = true;
				} else if (this->state_.index() == 2) {
					auto *r = s->renderable();
					r->layer() = Renderable::Layer::Window;
					r->update();

					Popup &t = std::get<2>(this->state_);
					this->manager->shell_->add_popup(&t);

					this->mapped_ = true;
				}
			}
			std::fprintf(stderr, "Is window 0x%x mapped: %s\n", this->id,
				     this->mapped_ ? "TRUE" : "FALSE");
		}
	});
}

void
XWindowManager::XWindow::set_commit_allowance(bool allow)
{
	if (frame_id != XCB_WINDOW_NONE) {
		uint32_t allow_int = allow ? 1 : 0;

		xcb_change_property(manager->x_conn_.get(), XCB_PROP_MODE_REPLACE, frame_id,
				    manager->atoms_["_XWAYLAND_ALLOW_COMMITS"], XCB_ATOM_CARDINAL, 32, 1,
				    &allow_int);
		xcb_flush(manager->x_conn_.get());
	}
}

void
XWindowManager::XWindow::set_net_wm_state()
{
	xcb_connection_t *conn = manager->x_conn_.get();
	auto &atoms = manager->atoms_;
	uint32_t props[3]{0};

	int i = 0;
	if (maximized_horz_) {
		props[i++] = atoms["_NET_WM_STATE_MAXIMIZED_HORZ"];
	}
	if (maximized_vert_) {
		props[i++] = atoms["_NET_WM_STATE_MAXIMIZED_VERT"];
	}

	xcb_change_property(conn, XCB_PROP_MODE_REPLACE, id, atoms["_NET_WM_STATE"], XCB_ATOM_ATOM, 32, i,
			    props);
}

void
XWindowManager::XWindow::Toplevel::set_size(int32_t width, int32_t height)
{
	if (!pending_config_) {
		pending_config_.emplace();

		pending_config_->maximized = wnd_->maximized_vert_ && wnd_->maximized_horz_;
		pending_config_->active = active_;

		pending_config_send_token_ = wnd_->manager->x_ctx_->compositor_->add_idle_task(
		    [this]() { this->send_pending_config(); });
	}

	pending_config_->width = width;
	pending_config_->height = height;
}

XWindowManager::XWindow::~XWindow()
{
	// Just so everything gets destructed properly.
	state_.emplace<0>();

	if (surface_) {
		wl_list_remove(&on_surface_destroy_.link);
		surface_ = nullptr;
		surface_resource_ = nullptr;
	}
}

void
XWindowManager::XWindow::Toplevel::set_activity(bool activity)
{
	if (!pending_config_) {
		pending_config_.emplace();

		pending_config_->maximized = wnd_->is_maximized();
		pending_config_->width = wnd_->width;
		pending_config_->height = wnd_->height;

		pending_config_send_token_ = wnd_->manager->x_ctx_->compositor_->add_idle_task(
		    [this]() { this->send_pending_config(); });
	}

	pending_config_->active = activity;
}

void
XWindowManager::XWindow::Toplevel::set_maximized(bool maximize)
{
	if (!pending_config_) {
		pending_config_.emplace();

		pending_config_->active = active_;
		pending_config_->width = wnd_->width;
		pending_config_->height = wnd_->height;

		pending_config_send_token_ = wnd_->manager->x_ctx_->compositor_->add_idle_task(
		    [this]() { this->send_pending_config(); });
	}

	pending_config_->maximized = maximize;
}

void
XWindowManager::XWindow::Toplevel::send_pending_config()
{
	xcb_connection_t *conn = wnd_->manager->x_conn_.get();

	wnd_->set_commit_allowance(false);
	wnd_->maximized_horz_ = pending_config_->maximized;
	wnd_->maximized_vert_ = pending_config_->maximized;

	wnd_->width = pending_config_->width;
	wnd_->height = pending_config_->height;

	if (wnd_->frame_id != XCB_WINDOW_NONE) {
		int32_t frame_dims[2] = {pending_config_->width, pending_config_->height};
		xcb_configure_window(conn, wnd_->frame_id, XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT,
				     frame_dims);
		pending_config_->width -= 2 * frame_edge_length;
		pending_config_->height -= frame_edge_length + frame_top_height;
	}

	int32_t window_dims[2] = {pending_config_->width, pending_config_->height};
	xcb_configure_window(conn, wnd_->id, XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, window_dims);

	bool old_active = active_;
	active_ = pending_config_->active;

	if (active_ != old_active) {
		if (active_) {
			xcb_set_input_focus(conn, XCB_INPUT_FOCUS_POINTER_ROOT, wnd_->id, XCB_CURRENT_TIME);
		} else {
			xcb_set_input_focus(conn, XCB_INPUT_FOCUS_POINTER_ROOT, XCB_WINDOW_NONE,
					    XCB_CURRENT_TIME);
		}
	}

	pending_config_.reset();

	wnd_->set_net_wm_state();
	wnd_->set_commit_allowance(true);

	xcb_flush(conn);
}

void
XWindowManager::XWindow::Toplevel::close()
{
	xcb_connection_t *conn = wnd_->manager->x_conn_.get();
	if (wnd_->has_wm_delete_window_) {
		xcb_client_message_event_t msg;

		msg.response_type = XCB_CLIENT_MESSAGE;
		msg.format = 32;
		msg.window = wnd_->id;
		msg.type = wnd_->manager->atoms_["WM_PROTOCOLS"];
		msg.data.data32[0] = wnd_->manager->atoms_["WM_DELETE_WINDOW"];
		msg.data.data32[1] = XCB_TIME_CURRENT_TIME;

		xcb_send_event(conn, 0, wnd_->id, XCB_EVENT_MASK_NO_EVENT, (char *) &msg);
	} else {
		xcb_kill_client(conn, wnd_->id);
	}
	xcb_flush(conn);
}

XWindowManager::XWindow::Toplevel::~Toplevel() { wnd_->manager->shell_->remove_window(this); }

XWindowManager::XWindow::Popup::~Popup() { wnd_->manager->shell_->remove_popup(this); }

orbycomp::shell::Surface *
XWindowManager::XWindow::Popup::transient_for()
{
	return parent_;
}

void
XWindowManager::XWindow::Popup::set_activity(bool active)
{
	xcb_connection_t *conn = wnd_->manager->x_conn_.get();
	if (active) {
		xcb_set_input_focus(conn, XCB_INPUT_FOCUS_POINTER_ROOT, wnd_->id, XCB_TIME_CURRENT_TIME);
	} else {
		xcb_set_input_focus(conn, XCB_INPUT_FOCUS_POINTER_ROOT, XCB_WINDOW_NONE,
				    XCB_TIME_CURRENT_TIME);
	}
	xcb_flush(conn);
}

}; // namespace orbycomp
