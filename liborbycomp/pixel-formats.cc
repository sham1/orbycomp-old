/*
 * pixel-formats.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <drm/drm_fourcc.h>
#include <orbycomp/base/pixel-formats.hh>

template <typename T, size_t N> static constexpr std::size_t array_len(T (&array)[N]) { return N; }

namespace orbycomp
{

static constexpr PixelFormat formats[] = {
    {
	DRM_FORMAT_XRGB8888,

	8,
	8,
	8,
	0,

	32,
	1,
    },
    {
	DRM_FORMAT_ARGB8888,

	8,
	8,
	8,
	8,

	32,
	1,
    },
};

static constexpr size_t format_count{array_len(formats)};

const PixelFormat *
PixelFormat::get_by_shm(enum wl_shm_format format)
{
	switch (format) {
	case WL_SHM_FORMAT_XRGB8888:
		return get_by_fourcc(DRM_FORMAT_XRGB8888);
	case WL_SHM_FORMAT_ARGB8888:
		return get_by_fourcc(DRM_FORMAT_ARGB8888);
	default:
		return get_by_fourcc(format);
	}
}

const PixelFormat *
PixelFormat::get_by_fourcc(uint32_t format)
{
	for (std::size_t i = 0; i < format_count; ++i) {
		auto &f{formats[i]};
		if (f.fourcc_format == format) {
			return &f;
		}
	}
	return nullptr;
}

}; // namespace orbycomp
