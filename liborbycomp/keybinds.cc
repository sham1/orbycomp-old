/*
 * keybinds.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <algorithm>
#include <iostream>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/keybinds.hh>

namespace orbycomp
{

KeybindContext::KeybindContext(Compositor *compositor)
    : compositor_{compositor}, keymaps_ref_{create_keymaps_ref()}, global_{"global", this}
{
	lua::Context &L = compositor_->lua().ctx();

	L.new_metatable("orbycomp::Keymap");
	L.register_lib(Keymap::keymap_reg);

	L.push_value(-1);
	L.set_table_field("__index", -2);

	L.pop(1);

	L.new_metatable("orbycomp::Keybind");
	L.register_lib(LuaKeybind::keybind_reg);

	L.push_value(-1);
	L.set_table_field("__index", -2);

	L.pop(1);

	create_keymap("default");
	current_keymap_ = &get_default();
}

lua::Context::Ref
KeybindContext::create_keymaps_ref()
{
	auto &L = compositor_->lua().ctx();

	L.new_table();

	return lua::Context::Ref(L);
}

KeybindContext::Keymap &
KeybindContext::create_keymap(const std::string &name)
{
	auto *ctx = keymaps_ref_.ctx();
	auto keymap = std::unique_ptr<Keymap>(new Keymap(name, this));
	auto [iter, _] = maps_.insert_or_assign(name, std::move(keymap));

	auto *map = iter->second.get();

	keymaps_ref_.get_value();
	map->userdata_ref_.get_value();
	ctx->set_table_field(name, -2);

	ctx->pop(1);

	return *map;
}

KeybindContext::Keymap &
KeybindContext::get(const std::string &name)
{
	return *maps_.at(name).get();
}

KeybindContext::Keymap::Keymap(const std::string &name, KeybindContext *ctx)
    : name_{name}, userdata_ref_{create_userdata(ctx)}, bind_table_ref_{create_bind_table(ctx)}, ctx_{ctx}
{
}

lua::Context::Ref
KeybindContext::Keymap::create_userdata(KeybindContext *ctx)
{
	lua::Context &L = ctx->compositor_->lua().ctx();

	L.create_unowned_userdata(this);
	L.get_metatable("orbycomp::Keymap");

	L.set_metatable(-2);

	return lua::Context::Ref(L);
}

lua::Context::Ref
KeybindContext::Keymap::create_bind_table(KeybindContext *ctx)
{
	lua::Context &L = ctx->compositor_->lua().ctx();

	L.new_table();

	return lua::Context::Ref(L);
}

struct KeybindSpecParseResult {
	bool success;
	Modifier mod;
	xkb_keysym_t sym;
};

static std::vector<std::string>
split_keybind_spec(const std::string &spec)
{
	std::vector<std::string> ret;

	size_t prev = 0;
	size_t pos = 0;
	do {
		pos = spec.find("-", prev);
		if (pos == std::string::npos) {
			pos = spec.length();
		}
		ret.emplace_back(spec.substr(prev, pos - prev));
		prev = pos + 1;
	} while (pos < spec.length() && prev < spec.length());

	return ret;
}

static bool
parse_mods(const std::vector<std::string> &splits, Modifier &mods)
{
	for (auto iter = splits.rbegin() + 1; iter != splits.rend(); ++iter) {
		bool was_proper = false;
		const std::string &mod_str = *iter;

		// Control
		if (mod_str == "C") {
			mods |= Modifier::Control;
			was_proper = true;
		}

		// Meta (Alt)
		if (mod_str == "M") {
			mods |= Modifier::Alt;
			was_proper = true;
		}

		// Shift
		if (mod_str == "S") {
			mods |= Modifier::Shift;
			was_proper = true;
		}

		// Super
		if (mod_str == "s") {
			mods |= Modifier::Super;
			was_proper = true;
		}

		if (!was_proper) {
			return false;
		}
	}
	return true;
}

static KeybindSpecParseResult
parse_keybind_spec(const std::string &spec)
{
	auto splits = split_keybind_spec(spec);

	if (splits.size() == 0) {
		return {false, Modifier::NoModifier, XKB_KEY_NoSymbol};
	}

	Modifier mods = Modifier::NoModifier;
	if (!parse_mods(splits, mods)) {
		return {false, Modifier::NoModifier, XKB_KEY_NoSymbol};
	}

	std::string &sym_name = *splits.rbegin();

	xkb_keysym_t sym = xkb_keysym_from_name(sym_name.c_str(), XKB_KEYSYM_NO_FLAGS);
	if (sym == XKB_KEY_NoSymbol) {
		return {false, Modifier::NoModifier, XKB_KEY_NoSymbol};
	}

	return {true, mods, sym};
}

int
KeybindContext::Keymap::lua_add_keybind(struct lua_State *L)
{
	return lua::Context::wrap_function(L, [](lua::Context::LuaWrapper &L) {
		auto self = L.get_userdata_with_meta<Keymap>("orbycomp::Keymap", 1);

		auto keybind_spec = L.get<std::string>(2);
		L.ensure_type(3, LUA_TFUNCTION);

		auto [success, mods, sym] = parse_keybind_spec(keybind_spec);
		if (!success) {
			lua::impl::throw_argument_error(L.L(), 2, "invalid keybind string");
		}

		auto keybind = L.create_userdata<LuaKeybind>(
		    mods, sym, lua::Context::Ref(self->ctx_->compositor_->lua().ctx()));

		L.get_metatable("orbycomp::Keybind");
		L.set_metatable(-2);

		self->bind_table_ref_.get_value();
		L.push_light_userdata(keybind.get());
		L.push_value(-3);
		L.set_table(-3);

		L.pop(-1);

		self->add_bind(*keybind->bind_.get());

		return 1;
	});
}

void
KeybindContext::Keymap::add_bind(Keybind &bind)
{
	binds_.insert_back(bind);
}

LuaKeybind::LuaKeybind(Modifier mods, xkb_keysym_t sym, lua::Context::Ref cb_ref)
    : callback_ref_{std::move(cb_ref)}, bind_{std::make_unique<Keybind>(
					    mods, sym, [this](Modifier mods, xkb_keysym_t sym) {
						    this->execute_function(mods, sym);
					    })}
{
}

void
LuaKeybind::execute_function(Modifier mods, xkb_keysym_t sym)
{
	auto *ctx = callback_ref_.ctx();

	callback_ref_.get_value();
	ctx->push(static_cast<lua_Unsigned>(mods));
	ctx->push(static_cast<lua_Unsigned>(sym));

	if (ctx->pcall(2, 0) != 0) {
		auto err_msg = ctx->get<std::string>(-1);
		ctx->pop(1);
		std::fprintf(stderr, "Error while calling keybind: %s\n", err_msg.c_str());
	}
}

int
LuaKeybind::on_gc(struct lua_State *L)
{
	return lua::Context::wrap_function(L, [](lua::Context::LuaWrapper &L) {
		auto udata = L.get_userdata_with_meta<LuaKeybind>("orbycomp::Keybind");

		udata.clear();

		return 0;
	});
}

LuaKeybind::~LuaKeybind() {}

Keybind::Keybind(Modifier mods, xkb_keysym_t sym, KeyCallback cb) : callback_{cb}, mods_{mods}, sym_{sym} {}

bool
Keybind::matches(Modifier mods, xkb_keysym_t sym) const
{
	// XXX: Maybe introduce logic for consumed modifiers.
	return mods == mods_ && sym == sym_;
}

std::ostream &
operator<<(std::ostream &os, const Modifier &mods)
{
	if (mods == Modifier::NoModifier) {
		return os << "No modifier";
	}

	bool has_prefix = false;
	if (uint8_t(mods & Modifier::Control)) {
		if (has_prefix) {
			os << " | ";
		}
		os << "Control";
		has_prefix = true;
	}

	if (uint8_t(mods & Modifier::Shift)) {
		if (has_prefix) {
			os << " | ";
		}
		os << "Shift";
		has_prefix = true;
	}

	if (uint8_t(mods & Modifier::Alt)) {
		if (has_prefix) {
			os << " | ";
		}
		os << "Alt";
		has_prefix = true;
	}

	if (uint8_t(mods & Modifier::Super)) {
		if (has_prefix) {
			os << " | ";
		}
		os << "Super";
		has_prefix = true;
	}
	return os;
}

} // namespace orbycomp
