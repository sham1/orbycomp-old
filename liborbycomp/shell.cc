/*
 * shell.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <new>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/output.hh>
#include <orbycomp/base/shell.hh>
#include <orbycomp/base/surface.hh>
#include <stdexcept>

namespace orbycomp
{

namespace shell
{

Shell::Shell(Compositor *compositor) : compositor_{compositor}, shell_ref_{create_shell_ref()}
{
	for (auto &[_, seat] : compositor_->seats()) {
		on_add_seat(seat);
	}

	compositor_->seat_added_signal.connect([this](Seat *seat) { this->on_add_seat(seat); });

	compositor_->on_seat_destroy_signal.connect([this](Seat *seat) { this->on_destroy_seat(seat); });
}

lua::Context::Ref
Shell::create_shell_ref()
{
	lua::Context &L = compositor_->lua().ctx();

	L.create_unowned_userdata(this);

	L.new_metatable("orbycomp::Shell");
	L.register_lib(Shell::shell_reg);

	L.push_value(-1);
	L.set_table_field("__index", -2);

	L.set_metatable(-2);

	return lua::Context::Ref(L);
}

void
Shell::push_lua_global()
{
	shell_ref_.get_value();
}

int
Shell::lua_maximize_top(lua_State *L)
{
	return lua::Context::wrap_function(L, [](lua::Context::LuaWrapper &L) {
		auto self = L.get_userdata_with_meta<Shell>("orbycomp::Shell", 1);

		self->maximize_focus_window();

		return 0;
	});
}

int
Shell::lua_close_top(lua_State *L)
{
	return lua::Context::wrap_function(L, [](lua::Context::LuaWrapper &L) {
		auto self = L.get_userdata_with_meta<Shell>("orbycomp::Shell", 1);

		self->close_focus_window();

		return 0;
	});
}

void
Shell::on_add_seat(Seat *seat)
{
	seats_.try_emplace(seat, std::make_unique<ShellSeat>(this, seat));
}

void
Shell::on_destroy_seat(Seat *seat)
{
	seats_.erase(seat);
}

void
Shell::maximize_focus_window()
{
	auto top_surface_pos = stack_.rbegin();
	if (top_surface_pos == stack_.rend()) {
		return;
	}

	auto *toplevel = (*top_surface_pos)->toplevel();
	if (!toplevel) {
		return;
	}

	// TODO: Figure out which monitor the window ought to be maximized to
	auto *monitor = compositor_->outputs_[0];
	auto &mode = monitor->current_mode();
	toplevel->set_size(mode.width_, mode.height_);
	toplevel->set_maximized(true);

	auto *s = toplevel->surface()->surface();
	if (s) {
		s->x() = monitor->x();
		s->y() = monitor->y();
	}
}

void
Shell::close_focus_window()
{
	auto top_surface_pos = stack_.rbegin();
	if (top_surface_pos == stack_.rend()) {
		return;
	}

	auto *toplevel = (*top_surface_pos)->toplevel();
	if (!toplevel) {
		return;
	}

	toplevel->close();
}

void
Shell::add_window(Toplevel *toplevel)
{
	auto current_focus = stack_.rbegin();
	if (current_focus != stack_.rend() && (*current_focus)->toplevel()) {
		(*current_focus)->toplevel()->set_activity(false);
	}
	stack_.emplace_back(toplevel->surface());
	toplevel->set_activity(true);
	auto *surface = toplevel->surface()->surface();
	auto *r = surface->renderable();
	r->relative_pos() = stack_.size() - 1;
	r->update();

	for (auto &[s, _] : seats_) {
		if (s->keyboard()) {
			s->keyboard()->make_surface_focus(surface->resource());
		}
	}
}

void
Shell::remove_window(Toplevel *toplevel)
{
	auto it = stack_.begin();
	while (it != stack_.end()) {
		if ((*it)->toplevel() == toplevel) {
			auto *surface = toplevel->surface()->surface();
			auto *r = surface->renderable();
			r->layer() = Renderable::Layer::Hidden;
			r->update();

			for (auto &[s, _] : seats_) {
				if (s->keyboard()) {
					s->keyboard()->remove_surface_focus(surface->resource());
				}
			}

			toplevel->set_activity(false);

			it = stack_.erase(it);
			break;
		}
		++it;
	}

	for (; it != stack_.end(); ++it) {
		auto *r = (*it)->surface()->renderable();
		if (r) {
			r->relative_pos() = std::distance(stack_.begin(), it);
			r->update();
		}
	}

	auto last_surface = stack_.rbegin();
	if (last_surface != stack_.rend()) {
		auto *last_top = *last_surface;
		auto *surface = last_top->surface();
		if (last_top->toplevel()) {
			last_top->toplevel()->set_activity(true);
		}

		for (auto &[s, _] : seats_) {
			if (s->keyboard()) {
				s->keyboard()->make_surface_focus(surface->resource());
			}
		}
	}
}

void
Shell::add_popup(Popup *popup)
{
	auto current_focus = stack_.rbegin();
	if (current_focus != stack_.rend() && (*current_focus)->toplevel()) {
		(*current_focus)->toplevel()->set_activity(false);
	}
	stack_.emplace_back(popup->surface());
	auto *surface = popup->surface()->surface();
	auto *r = surface->renderable();
	r->relative_pos() = stack_.size() - 1;
	r->update();

	popup->set_activity(true);

	for (auto &[s, _] : seats_) {
		if (s->keyboard()) {
			s->keyboard()->make_surface_focus(surface->resource());
		}
	}
}

void
Shell::remove_popup(Popup *popup)
{
	auto it = stack_.begin();
	while (it != stack_.end()) {
		if ((*it)->popup() == popup) {
			auto *r = popup->surface()->surface()->renderable();
			r->layer() = Renderable::Layer::Hidden;
			r->update();

			it = stack_.erase(it);
			break;
		}
		++it;
	}

	popup->set_activity(false);

	for (; it != stack_.end(); ++it) {
		auto *r = (*it)->surface()->renderable();
		if (r) {
			r->relative_pos() = std::distance(stack_.begin(), it);
			r->update();
		}
	}

	auto last_surface = stack_.rbegin();
	if (last_surface != stack_.rend()) {
		auto *last_top = *last_surface;
		auto *surface = last_top->surface();
		if (last_top->toplevel()) {
			last_top->toplevel()->set_activity(true);
		}

		for (auto &[s, _] : seats_) {
			if (s->keyboard()) {
				s->keyboard()->make_surface_focus(surface->resource());
			}
		}
	}
}

ShellSeat::ShellSeat(Shell *shell, Seat *seat) : shell_{shell}, seat_{seat}
{
	if (seat_->keyboard()) {
		auto top_window = shell_->stack_.rbegin();
		if (top_window != shell_->stack_.rend()) {
			auto *resource = (*top_window)->surface()->resource();
			seat_->keyboard()->make_surface_focus(resource);
		}
	}

	on_seat_caps_change_ = seat_->on_caps_changed_signal_.connect(
	    [this](auto &keyboard, auto &pointer) { this->on_caps_changed(keyboard, pointer); });
}

ShellSeat::~ShellSeat() {}

void
ShellSeat::on_caps_changed(std::optional<Keyboard> &keyboard, std::optional<Pointer> &pointer)
{
	if (keyboard) {
		auto top_window = shell_->stack_.rbegin();
		if (top_window != shell_->stack_.rend()) {
			auto *resource = (*top_window)->surface()->resource();
			keyboard->make_surface_focus(resource);
		}
	}
}

} // namespace shell

} // namespace orbycomp
