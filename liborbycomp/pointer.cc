/*
 * keyboard.cc
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/pointer.hh>
#include <orbycomp/base/seat.hh>
#include <orbycomp/xcursor.hh>

// Pointer button events require Linux-specific event codes.
// TODO: Make this somehow portable and compatible with Linux.
#include <linux/input-event-codes.h>

namespace orbycomp
{

Pointer::Pointer(Seat *seat) : seat_{seat}
{
	wl_list_init(&resources_);

	// Load a default cursor with the theme specified by user.
	default_cursor_ =
	    seat_->compositor()->backend().get_renderer()->get_bitmap_renderable(0, 0, 128, 128);
	uint8_t *cursor_data = default_cursor_->data();
	XcursorImage *cursor_img = seat_->compositor()->cursor_ctx().get_cursor("left_ptr");
	for (uint32_t y = 0; y < cursor_img->height; ++y) {
		size_t cursor_img_offset = y * cursor_img->width;
		size_t cursor_data_offset = y * default_cursor_->stride();

		const uint32_t *cursor_img_data = &cursor_img->pixels[cursor_img_offset];

		uint32_t *cursor_data_ptr = reinterpret_cast<uint32_t *>(&cursor_data[cursor_data_offset]);

		std::copy_n(cursor_img_data, cursor_img->width, cursor_data_ptr);
	}

	seat_->compositor()->backend().get_renderer()->set_cursor(this, default_cursor_.get(),
								  cursor_img->xhot, cursor_img->yhot);

	mouse_move_connection_ = on_move_signal.connect(std::mem_fn(&Pointer::on_move_cb));
}

Pointer::~Pointer()
{
	struct wl_resource *resource, *tmp;
	wl_resource_for_each_safe(resource, tmp, &resources_) { wl_resource_destroy(resource); }
}

void
Pointer::create_resource(struct wl_client *client, uint32_t id, int seat_version)
{
	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> resource{
	    wl_resource_create(client, &wl_pointer_interface, seat_version, id), wl_resource_destroy};
	if (!resource) {
		throw std::bad_alloc();
	}

	wl_resource_set_implementation(resource.get(), &Pointer::iface, this, Pointer::on_resource_destroy);

	wl_list_insert(&resources_, wl_resource_get_link(resource.get()));

	resource.release();
}

void
Pointer::on_resource_destroy(struct wl_resource *resource)
{
	wl_list_remove(wl_resource_get_link(resource));
}

void
Pointer::release(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
Pointer::set_cursor(struct wl_client *client,
		    struct wl_resource *resource,
		    uint32_t serial,
		    struct wl_resource *surface,
		    int32_t hotspot_x,
		    int32_t hotspot_y)
{
	Pointer *self = static_cast<Pointer *>(wl_resource_get_user_data(resource));

	if (surface) {
		Surface *surface_ = static_cast<Surface *>(wl_resource_get_user_data(surface));
		if (surface_->renderable()) {
			self->seat_->compositor()->backend().get_renderer()->set_cursor(
			    self, surface_->renderable(), hotspot_x - surface_->x(),
			    hotspot_y - surface_->y());
			self->surface_cursor_realized_ = true;
		}

		self->on_cursor_surface_commit_ =
		    surface_->on_commit_signal_.connect([self, hotspot_x, hotspot_y](Surface *s) {
			    if (!self->surface_cursor_realized_) {
				    if (s->renderable()) {
					    self->seat_->compositor()->backend().get_renderer()->set_cursor(
						self, s->renderable(), hotspot_x - s->x(),
						hotspot_y - s->y());
					    self->surface_cursor_realized_ = true;
				    }
			    }
		    });

	} else {
		self->seat_->compositor()->backend().get_renderer()->hide_cursor();

		self->surface_cursor_realized_ = false;
		self->on_cursor_surface_commit_.disconnect();
	}
}

void
Pointer::set_location(uint64_t tv_usec, double x, double y)
{
	x_ = x;
	y_ = y;
	on_move_signal(this, x_, y_, tv_usec);
}

void
Pointer::move(uint64_t tv_usec, double dx, double dy, double dx_unaccel, double dy_unaccel)
{
	double new_x = x_ + dx;
	double new_y = y_ + dy;

	if (new_x < 0) {
		new_x = 0;
	}

	if (new_y < 0) {
		new_y = 0;
	}

	x_ = new_x;
	y_ = new_y;

	if (grab_) {
		grab_->on_move_(x_, y_, dx, dy);
	}

	on_move_signal(this, x_, y_, tv_usec);
}

void
Pointer::feed_button_event(uint64_t tv_usec, uint32_t button, ButtonState state)
{
	if (grab_ && state == ButtonState::Release) {
		grab_.reset();
	}

	if (auto *surface_focus = std::get_if<SurfaceFocus>(&focus_)) {
		// TODO: Handle input_region correctly.
		struct wl_resource *surface = surface_focus->surface_resource_;
		struct wl_resource *pointer = surface_focus->pointer_resource_;

		if (surface && pointer) {
			uint32_t wl_state;
			if (state == ButtonState::Press) {
				wl_state = uint32_t{WL_POINTER_BUTTON_STATE_PRESSED};
			}
			if (state == ButtonState::Release) {
				wl_state = uint32_t{WL_POINTER_BUTTON_STATE_RELEASED};
			}

			uint32_t time = uint32_t(tv_usec / 1000);

			uint32_t serial = wl_display_next_serial(seat_->compositor()->display());

			wl_pointer_send_button(pointer, serial, time, button, wl_state);

			if (wl_resource_get_version(pointer) >= WL_POINTER_FRAME_SINCE_VERSION) {
				wl_pointer_send_frame(pointer);
			}
		}
	}
}

void
Pointer::on_move_cb(double x, double y, uint64_t tv_usec)
{
	Compositor *compositor = seat_->compositor();
	auto top_renderable = compositor->renderables_.rbegin();
	if (top_renderable == compositor->renderables_.rend()) {
		return;
	}

	// If we are in a grab, we shouldn't set focus.
	if (grab_) {
		return;
	}

	auto hover_renderable =
	    std::find_if(top_renderable, compositor->renderables_.rend(), [x, y](const auto *r) {
		    if (r->layer() == Renderable::Layer::Hidden) {
			    return false;
		    }

		    const auto &area = r->input_region();

		    return area.contains_point(x, y);
	    });

	if (hover_renderable == compositor->renderables_.rend()) {
		focus_.emplace<0>();
		return;
	}

	auto *renderable = *hover_renderable;

	if (auto surface_renderable = dynamic_cast<SurfaceRenderable *>(renderable)) {

		Surface *surface = surface_renderable->surface();
		struct wl_resource *surface_resource = surface->resource();
		struct wl_client *surface_client = wl_resource_get_client(surface_resource);

		if (auto *surface_focus = std::get_if<SurfaceFocus>(&focus_)) {
			if (surface_focus->surface_resource_ == surface_resource) {
				update_surface_move(surface_focus, surface_renderable, x, y, tv_usec);
			} else {
				focus_on_surface(surface_client, surface_renderable, surface_resource, x, y);
			}
		} else {
			focus_on_surface(surface_client, surface_renderable, surface_resource, x, y);
		}

	} else {
		if (std::get_if<SurfaceFocus>(&focus_)) {
			focus_.emplace<0>();
		}
	}
}

void
Pointer::update_surface_move(
    SurfaceFocus *surface_focus, SurfaceRenderable *surface_renderable, double x, double y, uint64_t tv_usec)
{
	if (surface_focus->pointer_resource_) {
		uint32_t time{uint32_t(tv_usec / 1000)};

		double surface_offx = surface_renderable->offset_x();
		double surface_offy = surface_renderable->offset_y();

		wl_fixed_t surface_x = wl_fixed_from_double(x - surface_offx);
		wl_fixed_t surface_y = wl_fixed_from_double(y - surface_offy);

		wl_pointer_send_motion(surface_focus->pointer_resource_, time, surface_x, surface_y);
		if (wl_resource_get_version(surface_focus->pointer_resource_) >=
		    WL_POINTER_FRAME_SINCE_VERSION) {
			wl_pointer_send_frame(surface_focus->pointer_resource_);
		}
	}
}

void
Pointer::focus_on_surface(struct wl_client *surface_client,
			  SurfaceRenderable *surface_renderable,
			  struct wl_resource *surface_resource,
			  double x,
			  double y)
{
	bool found_pointer = false;
	struct wl_resource *pointer_resource{nullptr};
	wl_resource_for_each(pointer_resource, &resources_)
	{
		struct wl_client *client = wl_resource_get_client(pointer_resource);
		if (surface_client == client) {
			found_pointer = true;
			break;
		}
	}

	if (focus_.index() != 1) {
		focus_.emplace<0>();
	}

	if (!found_pointer) {
		pointer_resource = nullptr;
	}
	focus_.emplace<1>(this, surface_resource, pointer_resource);

	if (surface_resource && pointer_resource) {
		double surface_offx = surface_renderable->offset_x();
		double surface_offy = surface_renderable->offset_y();

		wl_fixed_t surface_x = wl_fixed_from_double(x - surface_offx);
		wl_fixed_t surface_y = wl_fixed_from_double(y - surface_offy);

		uint32_t serial = wl_display_next_serial(seat_->compositor()->display());

		wl_pointer_send_enter(pointer_resource, serial, surface_resource, surface_x, surface_y);
	}
}

void
Pointer::set_grab(struct wl_client *client,
		  Pointer::GrabCB &&on_move,
		  std::optional<std::function<void()>> &&on_release)
{
	struct wl_resource *pointer_res;
	bool found{false};

	wl_resource_for_each(pointer_res, &resources_)
	{
		struct wl_client *pointer_client = wl_resource_get_client(pointer_res);
		if (pointer_client == client) {
			found = true;
			break;
		}
	}

	if (!found) {
		return;
	}

	grab_.emplace(this, pointer_res, std::move(on_move), std::move(on_release));
	focus_.emplace<0>();
}

Pointer::Grab::Grab(Pointer *pointer,
		    struct wl_resource *pointer_resource,
		    GrabCB &&on_move,
		    std::optional<std::function<void()>> &&on_release)
    : pointer_{pointer}, pointer_resource_{pointer_resource}, on_move_{std::move(on_move)},
      on_release_{std::move(on_release)}
{
	on_pointer_destroy_.notify = Grab::on_pointer_destroy_cb;
	if (pointer_resource_) {
		wl_resource_add_destroy_listener(pointer_resource_, &on_pointer_destroy_);
	}
}

Pointer::Grab::~Grab()
{
	wl_list_remove(&on_pointer_destroy_.link);

	if (on_release_) {
		(*on_release_)();
	}
}

void
Pointer::Grab::on_pointer_destroy_cb(struct wl_listener *listener, void *user_data)
{
	Grab *self = util::container_of(listener, &Grab::on_pointer_destroy_);

	self->pointer_resource_ = nullptr;
	self->pointer_->grab_.reset();
}

Pointer::SurfaceFocus::SurfaceFocus(Pointer *pointer,
				    struct wl_resource *surface_resource,
				    struct wl_resource *pointer_resource)
    : pointer_{pointer}, surface_resource_{surface_resource}, pointer_resource_{pointer_resource}
{
	on_surface_destroy_.notify = SurfaceFocus::on_surface_destroy_cb;
	on_pointer_destroy_.notify = SurfaceFocus::on_pointer_destroy_cb;

	if (surface_resource_) {
		wl_resource_add_destroy_listener(surface_resource_, &on_surface_destroy_);
	}

	if (pointer_resource_) {
		wl_resource_add_destroy_listener(pointer_resource_, &on_pointer_destroy_);
	}
}

Pointer::SurfaceFocus::~SurfaceFocus()
{
	leave_focus();

	wl_list_remove(&on_surface_destroy_.link);
	wl_list_remove(&on_pointer_destroy_.link);
}

void
Pointer::SurfaceFocus::on_surface_destroy_cb(struct wl_listener *listener, void *user_data)
{
	SurfaceFocus *self = util::container_of(listener, &SurfaceFocus::on_surface_destroy_);

	self->surface_resource_ = nullptr;
	self->pointer_resource_ = nullptr;
	self->pointer_->focus_.emplace<0>();
}

void
Pointer::SurfaceFocus::on_pointer_destroy_cb(struct wl_listener *listener, void *user_data)
{
	SurfaceFocus *self = util::container_of(listener, &SurfaceFocus::on_pointer_destroy_);

	self->surface_resource_ = nullptr;
	self->pointer_resource_ = nullptr;
	self->pointer_->focus_.emplace<0>();
}

void
Pointer::SurfaceFocus::leave_focus()
{
	if (surface_resource_ && pointer_resource_) {
		uint32_t serial = wl_display_next_serial(pointer_->seat_->compositor()->display());

		wl_pointer_send_leave(pointer_resource_, serial, surface_resource_);
		if (wl_resource_get_version(pointer_resource_) >= WL_POINTER_FRAME_SINCE_VERSION) {
			wl_pointer_send_frame(pointer_resource_);
		}
	}
}

}; // namespace orbycomp
