/*
 * renderer.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <orbycomp/render/shader.hh>
#include <orbycomp/renderer.hh>
#include <orbycomp/renderers/gles-renderer.hh>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include <wayland-server.h>

extern "C" orbycomp::Renderer *renderer_init(orbycomp::RendererOptions &opts);

namespace
{
typedef EGLBoolean(EGLAPIENTRYP PFNEGLBINDWAYLANDDISPLAYWL)(EGLDisplay dpy, struct wl_display *display);
typedef EGLBoolean(EGLAPIENTRYP PFNEGLUNBINDWAYLANDDISPLAYWL)(EGLDisplay dpy, struct wl_display *display);
typedef EGLBoolean(EGLAPIENTRYP PFNEGLQUERYWAYLANDBUFFERWL)(EGLDisplay dpy,
							    struct wl_resource *buffer,
							    EGLint attribute,
							    EGLint *value);
}; // namespace

#ifndef EGL_WAYLAND_BUFFER_WL
#define EGL_WAYLAND_BUFFER_WL 0x31D5
#define EGL_WAYLAND_PLANE_WL 0x31D6

#define EGL_TEXTURE_Y_U_V_WL 0x31D7
#define EGL_TEXTURE_Y_UV_WL 0x31D8
#define EGL_TEXTURE_Y_XUXV_WL 0x31D9
#define EGL_TEXTURE_EXTERNAL_WL 0x31DA
#endif

namespace orbycomp
{

namespace gles
{

using Opts = orbycomp::renderers::GLESRendererOptions;
using OutputOpts = orbycomp::renderers::GLESRendererOutputOptions;

class Renderer;
class OutputInfo;

struct egl_display {
	egl_display(Renderer *renderer, NativeDisplayType backend_dpy);
	~egl_display();

	Renderer *renderer_;
	EGLDisplay dpy;
	std::vector<EGLConfig> configs_{};

	inline const std::vector<EGLConfig> &
	configs() const
	{
		return configs_;
	}

	std::unordered_set<std::string> client_exts_;
	std::unordered_set<std::string> display_exts_;

	PFNEGLBINDWAYLANDDISPLAYWL bind_wayland_display_wl;
	PFNEGLUNBINDWAYLANDDISPLAYWL unbind_wayland_display_wl;
	PFNEGLQUERYWAYLANDBUFFERWL query_wayland_buffer_wl;
};

struct egl_surface {
	struct egl_display *dpy_{nullptr};

	EGLSurface surface_{EGL_NO_SURFACE};

	egl_surface() {}
	egl_surface(struct egl_display *dpy, EGLSurface surface) : dpy_{dpy}, surface_{surface} {}

	~egl_surface()
	{
		if (surface_ != EGL_NO_SURFACE) {
			eglDestroySurface(dpy_->dpy, surface_);
			surface_ = EGL_NO_SURFACE;
		}
	}
};

struct egl_context {
	struct egl_display *dpy_{nullptr};

	EGLContext context_{EGL_NO_CONTEXT};

	egl_context() {}
	egl_context(struct egl_display *dpy, EGLContext context) : dpy_{dpy}, context_{context} {}

	~egl_context()
	{
		if (context_ != EGL_NO_CONTEXT) {
			eglDestroyContext(dpy_->dpy, context_);
			context_ = EGL_NO_CONTEXT;
		}
	}
};

class Texture
{
	struct Inner {
		GLuint tid_{0};

		Inner() { glGenTextures(1, &tid_); }

		~Inner() { glDeleteTextures(1, &tid_); }
	};
	std::shared_ptr<Inner> inner_;

public:
	Texture() : inner_{std::make_shared<Inner>()} {}

	Texture(const Texture &other) = default;
	Texture &operator=(const Texture &other) = default;

	Texture(Texture &&other) = default;
	Texture &operator=(Texture &&other) = default;

	~Texture() = default;

	inline GLuint
	id() const
	{
		return inner_->tid_;
	}
};

class GLESRenderable
{
public:
	virtual void calculate_input_region(Region &region) = 0;

	virtual ~GLESRenderable() {}
};

class SurfaceRenderable : public orbycomp::SurfaceRenderable, public GLESRenderable
{
	Texture texture_;

	Renderer *renderer_;

public:
	bool is_opaque_{false};

	SurfaceRenderable(Renderer *renderer, Surface *surface);
	~SurfaceRenderable();

	void update() override;
	void upload_texture(bool attach) override;

	void render_finish(uint32_t msec_delta) override;

	void bind_texture_to_loc(GLint uniform_loc);

	void calculate_input_region(Region &region) override;
};

class BitmapRenderable : public orbycomp::BitmapRenderable, public GLESRenderable
{
	Texture texture_;

	Renderer *renderer_;

public:
	BitmapRenderable(
	    Renderer *renderer, int32_t x, int32_t y, int32_t width, int32_t height, bool is_alpha);
	~BitmapRenderable();

	void update() override;
	void upload_texture(bool attach) override;

	void render_finish(uint32_t msec_delta) override;

	void calculate_input_region(Region &region) override;
};

class Renderer : public orbycomp::Renderer
{
	friend orbycomp::Renderer * ::renderer_init(orbycomp::RendererOptions &opts);

	friend OutputInfo;

	friend egl_display;

	friend SurfaceRenderable;
	friend BitmapRenderable;

	Renderer(orbycomp::renderers::GLESRendererOptions &opts);

	Compositor *compositor_;

	EGLenum platform_;

	egl_display dpy_;

	struct ExtInfo {
		bool has_context_priority{false};
		bool has_configless_context{false};
		bool has_surfaceless_context{false};
	} exts_;
	ExtInfo setup_exts();

	EGLConfig config_;
	egl_surface dummy_surface_;

	EGLConfig find_config_for_opts(Opts &opts);
	egl_surface get_dummy_surface();

	egl_context context_;
	egl_context get_context();

	opengl::LinkedShaderProgram renderable_shader_;

	std::unordered_map<Output *, std::unique_ptr<OutputInfo>> output_infos_;

public:
	~Renderer();

	std::shared_ptr<orbycomp::SurfaceRenderable> get_surface_renderable(Surface *surface) override;

	std::shared_ptr<orbycomp::BitmapRenderable> get_bitmap_renderable(
	    int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha = true) override;

	void set_cursor(Pointer *pointer, Renderable *renderable, int32_t hotx, int32_t hoty) override;

	void hide_cursor() override;

	void setup_output(Output *output, RendererOutputOptions &opts) override;

	void render_output(Output *output) override;

	bool is_buffer_egl(struct wl_resource *resource);
};

class OutputInfo
{
	friend Renderer;

	Renderer *renderer_;
	Output *output_;

	egl_surface surface_;

public:
	OutputInfo(Renderer *renderer, Output *output, OutputOpts &opts);

	egl_surface create_surface(OutputOpts &opts);

	~OutputInfo();

	void render();
};

}; // namespace gles

}; // namespace orbycomp
