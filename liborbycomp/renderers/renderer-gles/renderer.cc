/*
 * renderer.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "renderer.hh"
#include <orbycomp/renderers/gles-renderer.hh>

#include <cstdio>
#include <sstream>
#include <stdexcept>

#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/pixel-formats.hh>

extern "C" orbycomp::Renderer *
renderer_init(orbycomp::RendererOptions &opts)
{
	return new orbycomp::gles::Renderer(static_cast<orbycomp::gles::Opts &>(opts));
}

namespace orbycomp
{

namespace gles
{

static constexpr char renderable_vertex_shader[] = "attribute vec2 pos;\n"
						   "attribute vec2 texCoord;\n"
						   "\n"
						   "varying vec2 TexCoord;\n"
						   "\n"
						   "uniform mat4 modelview;\n"
						   "uniform mat4 projection;\n"
						   "\n"
						   "void main(void) {\n"
						   "	vec4 translated_pos = modelview * vec4(pos, 1, 1);\n"
						   "	gl_Position = projection * translated_pos;\n"
						   "	TexCoord = texCoord;\n"
						   "}\n";

static constexpr char renderable_fragment_shader[] = "precision mediump float;\n"
						     "\n"
						     "varying vec2 TexCoord;\n"
						     "\n"
						     "uniform sampler2D buffer;\n"
						     "\n"
						     "void main(void) {\n"
						     "	gl_FragColor = texture2D(buffer, TexCoord);\n"
						     "}\n";

struct Vertex {
	GLfloat pos[2];
	GLfloat texCoord[2];
};

constexpr Vertex renderable_rect_vertices[] = {
    {{0.0f, 0.0f}, {0.0f, 0.0f}},

    {{0.0f, 1.0f}, {0.0f, 1.0f}},

    {{1.0f, 1.0f}, {1.0f, 1.0f}},

    {{1.0f, 0.0f}, {1.0f, 0.0f}},
};

constexpr GLushort renderable_rect_indices[] = {
    0, 1, 2,

    3, 0, 2,
};

Renderer::Renderer(Opts &opts)
    : compositor_{opts.compositor}, platform_{opts.platform}, dpy_{this, opts.backend_dpy},
      exts_{setup_exts()}, config_{find_config_for_opts(opts)},
      dummy_surface_{get_dummy_surface()}, context_{get_context()}
{
	eglMakeCurrent(dpy_.dpy, dummy_surface_.surface_, dummy_surface_.surface_, context_.context_);

	renderable_shader_ = opengl::ShaderProgram()
				 .attach(opengl::Shader{renderable_vertex_shader, GL_VERTEX_SHADER})
				 .attach(opengl::Shader{renderable_fragment_shader, GL_FRAGMENT_SHADER})
				 .bind_attribute_location(0, "pos")
				 .bind_attribute_location(1, "texCoord")
				 .link();
}

Renderer::ExtInfo
Renderer::setup_exts()
{
	auto &extensions = dpy_.display_exts_;
	auto has_ext = [&extensions](const std::string &name) {
		return extensions.find(name) != extensions.end();
	};

	ExtInfo exts;

	exts.has_context_priority = has_ext("EGL_IMG_context_priority");

	exts.has_configless_context =
	    has_ext("EGL_KHR_no_config_context") || has_ext("EGL_MESA_configless_context");

	exts.has_surfaceless_context = has_ext("EGL_KHR_surfaceless_context");

	return exts;
}

EGLConfig
Renderer::find_config_for_opts(Opts &opts)
{
	for (size_t i = 0; i < opts.formats_count; ++i) {
		uint32_t format = opts.formats[i];
		for (const auto config : dpy_.configs_) {
			auto *pixel_format = PixelFormat::get_by_fourcc(format);
			if (!pixel_format) {
				continue;
			}

			if (platform_ == EGL_PLATFORM_GBM_KHR) {
				EGLint config_format;
				if (!eglGetConfigAttrib(dpy_.dpy, config, EGL_NATIVE_VISUAL_ID,
							&config_format)) {
					continue;
				}
				if (uint32_t(config_format) == format) {
					return config;
				}
			} else {
				EGLint red_size;
				EGLint green_size;
				EGLint blue_size;
				EGLint alpha_size;

				if (!eglGetConfigAttrib(dpy_.dpy, config, EGL_RED_SIZE, &red_size)) {
					continue;
				}
				if (!eglGetConfigAttrib(dpy_.dpy, config, EGL_GREEN_SIZE, &green_size)) {
					continue;
				}
				if (!eglGetConfigAttrib(dpy_.dpy, config, EGL_BLUE_SIZE, &blue_size)) {
					continue;
				}
				if (!eglGetConfigAttrib(dpy_.dpy, config, EGL_ALPHA_SIZE, &alpha_size)) {
					continue;
				}

				if (red_size == pixel_format->red_depth &&
				    green_size == pixel_format->green_depth &&
				    blue_size == pixel_format->blue_depth &&
				    alpha_size == pixel_format->alpha_depth) {
					return config;
				}
			}
		}
	}

	throw std::runtime_error("Could not find EGL config for our display");
}

egl_surface
Renderer::get_dummy_surface()
{
	if (exts_.has_surfaceless_context) {
		return {};
	}

	// TODO: Add ability to get pbuffer as a dummy surface.
	throw std::runtime_error("Could not create dummy surface");
}

egl_context
Renderer::get_context()
{
	EGLint context_attribs[16] = {
	    EGL_CONTEXT_CLIENT_VERSION,
	    0,
	};
	size_t attrib_count = 2;

	if (!eglBindAPI(EGL_OPENGL_ES_API)) {
		throw std::runtime_error("Failed to bind EGL_OPENGL_ES_API");
	}

	if (exts_.has_context_priority) {
		context_attribs[attrib_count++] = EGL_CONTEXT_PRIORITY_LEVEL_IMG;
		context_attribs[attrib_count++] = EGL_CONTEXT_PRIORITY_HIGH_IMG;
	}

	context_attribs[attrib_count] = EGL_NONE;

	// Try OpenGL ES 3 first
	context_attribs[1] = 3;
	EGLContext ctx = eglCreateContext(dpy_.dpy, config_, EGL_NO_CONTEXT, context_attribs);
	if (ctx == nullptr) {
		// Couldn't do OpenGL ES 3, try ES 2 instead
		context_attribs[1] = 2;
		ctx = eglCreateContext(dpy_.dpy, config_, EGL_NO_CONTEXT, context_attribs);

		if (ctx == nullptr) {
			std::runtime_error("Could not create OpenGL ES context");
		}
	}

	if (exts_.has_context_priority) {
		EGLint value = EGL_CONTEXT_PRIORITY_MEDIUM_IMG;

		eglQueryContext(dpy_.dpy, ctx, EGL_CONTEXT_PRIORITY_LEVEL_IMG, &value);
		if (value != EGL_CONTEXT_PRIORITY_HIGH_IMG) {
			std::fprintf(stderr, "Could not get high priority GLES context\n");
		}
	}

	return {&dpy_, ctx};
}

Renderer::~Renderer() {}

static inline bool
renderable_sort(Renderable *a, Renderable *b)
{
	return Renderable::comparator_(*a, *b);
}

std::shared_ptr<orbycomp::SurfaceRenderable>
Renderer::get_surface_renderable(Surface *surface)
{
	auto ret = std::make_shared<SurfaceRenderable>(this, surface);

	compositor_->renderables_.push_back(ret.get());
	std::sort(compositor_->renderables_.begin(), compositor_->renderables_.end(), renderable_sort);

	return ret;
}

std::shared_ptr<orbycomp::BitmapRenderable>
Renderer::get_bitmap_renderable(int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha)
{
	auto ret = std::make_shared<BitmapRenderable>(this, x, y, width, height, has_alpha);

	compositor_->renderables_.push_back(ret.get());
	std::sort(compositor_->renderables_.begin(), compositor_->renderables_.end(), renderable_sort);

	return ret;
}

void
Renderer::set_cursor(Pointer *pointer, Renderable *renderable, int32_t hotx, int32_t hoty)
{
}

void
Renderer::hide_cursor()
{
}

void
Renderer::setup_output(Output *output, RendererOutputOptions &opts)
{
	OutputOpts &gles_opts = static_cast<OutputOpts &>(opts);
	output_infos_.try_emplace(output, std::make_unique<OutputInfo>(this, output, gles_opts));
}

void
Renderer::render_output(Output *output)
{
	auto *info = output_infos_[output].get();
	info->render();
}

bool
Renderer::is_buffer_egl(struct wl_resource *resource)
{
	EGLint format{};
	return dpy_.query_wayland_buffer_wl(dpy_.dpy, resource, EGL_TEXTURE_FORMAT, &format);
}

egl_display::egl_display(Renderer *renderer, NativeDisplayType backend_dpy)
    : renderer_{renderer}, dpy{eglGetDisplay(backend_dpy)}
{
	if (dpy == EGL_NO_DISPLAY) {
		throw std::runtime_error("Could not setup EGL");
	}

	EGLint egl_major{0}, egl_minor{0};
	eglInitialize(dpy, &egl_major, &egl_minor);

	std::fprintf(stderr, "EGL version %d.%d initialized!\n", egl_major, egl_minor);

	EGLint egl_config_attribs[] = {
	    EGL_RED_SIZE,
	    1,

	    EGL_GREEN_SIZE,
	    1,

	    EGL_BLUE_SIZE,
	    1,

	    EGL_DEPTH_SIZE,
	    EGL_DONT_CARE,

	    EGL_STENCIL_SIZE,
	    EGL_DONT_CARE,

	    EGL_RENDERABLE_TYPE,
	    EGL_OPENGL_ES2_BIT,

	    EGL_SURFACE_TYPE,
	    EGL_WINDOW_BIT,

	    EGL_NONE,
	};

	EGLint config_num;
	if (!eglGetConfigs(dpy, nullptr, 0, &config_num)) {
		throw std::runtime_error("Could not get EGL configs");
	}

	configs_.resize(config_num);
	if (!eglChooseConfig(dpy, egl_config_attribs, configs_.data(), config_num, &config_num)) {
		throw std::runtime_error("Could not get EGL configs");
	}

	if (config_num == 0) {
		throw std::runtime_error("Could not get EGL configs");
	}

	configs_.resize(config_num);
	configs_.shrink_to_fit();

	std::stringstream client_exts{eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS)};
	std::stringstream display_exts{eglQueryString(dpy, EGL_EXTENSIONS)};

	std::string ext;
	while (std::getline(client_exts, ext, ' ')) {
		client_exts_.insert(ext);
	}

	while (std::getline(display_exts, ext, ' ')) {
		display_exts_.insert(ext);
	}

	std::fprintf(stderr, "Client extensions:\n");
	for (const auto &ext : client_exts_) {
		std::fprintf(stderr, "\t%s\n", ext.c_str());
	}

	std::fprintf(stderr, "\n");

	std::fprintf(stderr, "Display extensions:\n");
	for (const auto &ext : display_exts_) {
		std::fprintf(stderr, "\t%s\n", ext.c_str());
	}

	if (display_exts_.find("EGL_WL_bind_wayland_display") == display_exts_.end()) {
		throw std::runtime_error("EGL extensions EGL_WL_bind_wayland_display required but "
					 "not found");
	}

	bind_wayland_display_wl =
	    reinterpret_cast<decltype(bind_wayland_display_wl)>(eglGetProcAddress("eglBindWaylandDisplayWL"));
	unbind_wayland_display_wl = reinterpret_cast<decltype(unbind_wayland_display_wl)>(
	    eglGetProcAddress("eglUnbindWaylandDisplayWL"));

	query_wayland_buffer_wl =
	    reinterpret_cast<decltype(query_wayland_buffer_wl)>(eglGetProcAddress("eglQueryWaylandBufferWL"));

	bind_wayland_display_wl(dpy, renderer_->compositor_->display());
}

egl_display::~egl_display()
{
	unbind_wayland_display_wl(dpy, renderer_->compositor_->display());

	if (dpy != EGL_NO_DISPLAY) {
		eglTerminate(dpy);
		dpy = EGL_NO_DISPLAY;
	}
}

OutputInfo::OutputInfo(Renderer *renderer, Output *output, OutputOpts &opts)
    : renderer_{renderer}, output_{output}, surface_{create_surface(opts)}
{
}

egl_surface
OutputInfo::create_surface(OutputOpts &opts)
{
	EGLSurface surface =
	    eglCreateWindowSurface(renderer_->dpy_.dpy, renderer_->config_, opts.backend_window, nullptr);

	if (surface == EGL_NO_SURFACE) {
		throw std::runtime_error("Could not create EGL surface for display");
	}

	return {&renderer_->dpy_, surface};
}

OutputInfo::~OutputInfo() {}

void
OutputInfo::render()
{
	eglMakeCurrent(renderer_->dpy_.dpy, surface_.surface_, surface_.surface_,
		       renderer_->context_.context_);

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glViewport(0, 0, output_->current_mode().width_, output_->current_mode().height_);

	auto &shader = renderer_->renderable_shader_;

	shader.activate();

	GLuint vbo, index_buf;
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &index_buf);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(renderable_rect_vertices), renderable_rect_vertices,
		     GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			      reinterpret_cast<void *>(util::offset_of(&Vertex::pos)));

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
			      reinterpret_cast<void *>(util::offset_of(&Vertex::texCoord)));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buf);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(renderable_rect_indices), renderable_rect_indices,
		     GL_STATIC_DRAW);

	for (auto *r : renderer_->compositor_->renderables_) {
		if (r->layer() == Renderable::Layer::Hidden) {
			continue;
		}

		auto surface_renderable = dynamic_cast<SurfaceRenderable *>(r);
		if (surface_renderable) {
			GLint buffer_uniform = shader.uniform_location("buffer");

			surface_renderable->bind_texture_to_loc(buffer_uniform);
			if (!surface_renderable->is_opaque_) {
				glEnable(GL_BLEND);
			} else {
				glDisable(GL_BLEND);
			}
		}

		float rx = float(r->offset_x());
		float ry = float(r->offset_y());
		if (r->parent()) {
			auto *rp = r->parent();
			while (rp) {
				rx += rp->offset_x();
				ry += rp->offset_y();

				rp = rp->parent();
			}
		}

		affine::Mat4 translate_matrix = affine::create_translation_matrix({rx, ry});

		GLint modelview_uniform = shader.uniform_location("modelview");

		affine::Mat4 scale_matrix = affine::create_scaling_matrix(r->width(), r->height(), 0);

		affine::Mat4 modelview_matrix = translate_matrix * scale_matrix;

		std::array<float, 16> modelview_data = modelview_matrix;

		glUniformMatrix4fv(modelview_uniform, 1, GL_FALSE, modelview_data.data());

		GLint projection_uniform = shader.uniform_location("projection");

		affine::Mat4 projection_matrix =
		    affine::create_ortho_matrix(0, float(output_->current_mode().width_),
						float(output_->current_mode().height_), 0, 0, 1);

		std::array<float, 16> matrix_data = projection_matrix;

		glUniformMatrix4fv(projection_uniform, 1, GL_FALSE, matrix_data.data());

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
	}

	shader.deactivate();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDeleteBuffers(1, &index_buf);
	glDeleteBuffers(1, &vbo);

	eglSwapBuffers(renderer_->dpy_.dpy, surface_.surface_);
}

SurfaceRenderable::SurfaceRenderable(Renderer *renderer, Surface *surface)
    : orbycomp::SurfaceRenderable{surface}, renderer_{renderer}
{
}

static void
load_shm_image_to_texture(int32_t width, int32_t height, enum wl_shm_format format, void *data)
{
	if (!data) {
		return;
	}

	switch (format) {
	case WL_SHM_FORMAT_ARGB8888:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE,
			     data);
		break;
	case WL_SHM_FORMAT_XRGB8888:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE,
			     data);
		break;
	default:
		break;
	}
}

static void
load_shm_image_to_texture_with_damage(
    int32_t width, int32_t height, enum wl_shm_format format, void *data, const Region &damage)
{
	if (!data) {
		return;
	}

	for (const auto &rect : damage) {
		switch (format) {
		case WL_SHM_FORMAT_ARGB8888:
			glTexSubImage2D(GL_TEXTURE_2D, 0, rect.x1, rect.y1, rect.x2 - rect.x1,
					rect.y2 - rect.y2, GL_BGRA_EXT, GL_UNSIGNED_BYTE, data);
			break;
		case WL_SHM_FORMAT_XRGB8888:
			glTexSubImage2D(GL_TEXTURE_2D, 0, rect.x1, rect.y1, rect.x2 - rect.x1,
					rect.y2 - rect.y2, GL_BGRA_EXT, GL_UNSIGNED_BYTE, data);
			break;
		default:
			break;
		}
	}
}

void
SurfaceRenderable::update()
{
	std::sort(renderer_->compositor_->renderables_.begin(), renderer_->compositor_->renderables_.end(),
		  renderable_sort);

	if (layer_ == Layer::Hidden) {
		return;
	}

	x_ = surface_->x();
	y_ = surface_->y();

	if (surface_->geometry_) {
		x_off_ = surface_->geometry_->x_;
		y_off_ = surface_->geometry_->y_;
	}

	auto our_pos = std::find_if(renderer_->compositor_->renderables_.begin(),
				    renderer_->compositor_->renderables_.end(), [this](const auto *a) {
					    const auto *check_a = dynamic_cast<const SurfaceRenderable *>(a);

					    return check_a == this;
				    });

	Region input_region{};
	for (auto it = our_pos + 1; it != renderer_->compositor_->renderables_.end(); ++it) {
		auto *r = *it;
		input_region += r->input_region();
	}

	calculate_input_region(input_region);

	for (auto it = std::reverse_iterator(our_pos); it != renderer_->compositor_->renderables_.rend();
	     ++it) {
		auto *r = *it;
		if (r->layer() == Layer::Hidden) {
			break;
		}

		auto *renderable = dynamic_cast<GLESRenderable *>(r);

		renderable->calculate_input_region(input_region);
	}

	renderer_->compositor_->schedule_full_repaint();
}

void
SurfaceRenderable::upload_texture(bool attach)
{
	if (auto *shm_buf = wl_shm_buffer_get(surface_->buffer())) {
		width_ = wl_shm_buffer_get_width(shm_buf);
		height_ = wl_shm_buffer_get_height(shm_buf);

		enum wl_shm_format format =
		    static_cast<enum wl_shm_format>(wl_shm_buffer_get_format(shm_buf));

		is_opaque_ = format == WL_SHM_FORMAT_XRGB8888;

		glBindTexture(GL_TEXTURE_2D, texture_.id());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		wl_shm_buffer_begin_access(shm_buf);

		void *data = wl_shm_buffer_get_data(shm_buf);
		if (attach) {
			load_shm_image_to_texture(width_, height_, format, data);
		} else {
			Region damage = surface_->damage();
			surface_->reset_damage();
			load_shm_image_to_texture_with_damage(width_, height_, format, data, damage);
		}

		wl_shm_buffer_end_access(shm_buf);

		wl_buffer_send_release(surface_->buffer());
	} else if (renderer_->is_buffer_egl(surface_->buffer())) {
		EGLint plane_count{};
		EGLint format{};

		EGLint buffer_width{};
		EGLint buffer_height{};

		auto &dpy = renderer_->dpy_;

		dpy.query_wayland_buffer_wl(dpy.dpy, surface_->buffer(), EGL_TEXTURE_FORMAT, &format);

		switch (format) {
			// TODO: Deal with formats such as YUV
		case EGL_TEXTURE_Y_U_V_WL:
			plane_count = 3;
			break;
		case EGL_TEXTURE_Y_UV_WL:
		case EGL_TEXTURE_Y_XUXV_WL:
			plane_count = 2;
			break;
		case EGL_TEXTURE_EXTERNAL_WL:
		case EGL_TEXTURE_RGB:
		case EGL_TEXTURE_RGBA:
		default:
			plane_count = 1;
			break;
		}

		dpy.query_wayland_buffer_wl(dpy.dpy, surface_->buffer(), EGL_WIDTH, &buffer_width);

		dpy.query_wayland_buffer_wl(dpy.dpy, surface_->buffer(), EGL_HEIGHT, &buffer_height);

		width_ = buffer_width;
		height_ = buffer_height;

		glBindTexture(GL_TEXTURE_2D, texture_.id());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// TODO: Deal with multi-planar formats properly
		std::vector<EGLImage> planes;
		for (EGLint i = 0; i < plane_count; ++i) {
			EGLint img_attrs[]{
			    EGL_WAYLAND_PLANE_WL,
			    i,

			    EGL_IMAGE_PRESERVED_KHR,
			    EGL_TRUE,

			    EGL_NONE,
			};

			EGLImage img = eglCreateImageKHR(dpy.dpy, nullptr, EGL_WAYLAND_BUFFER_WL,
							 surface_->buffer(), img_attrs);

			glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, img);

			eglDestroyImageKHR(dpy.dpy, img);
		}

		wl_buffer_send_release(surface_->buffer());
	}
}

void
SurfaceRenderable::bind_texture_to_loc(GLint uniform_loc)
{
	glUniform1i(uniform_loc, 0);

	glActiveTexture(GL_TEXTURE0 + 0);
	glBindTexture(GL_TEXTURE_2D, texture_.id());
}

void
SurfaceRenderable::calculate_input_region(Region &region)
{
	Region tmp{surface_->input_region()};
	Region clip{{0, 0, width_, height_}};

	input_region_ = tmp.intersect(clip);
	input_region_.translate(offset_x(), offset_y());

	input_region_ -= region;

	region += input_region_;
}

void
SurfaceRenderable::render_finish(uint32_t msec_delta)
{
	orbycomp::SurfaceRenderable::render_finish(msec_delta);
}

SurfaceRenderable::~SurfaceRenderable()
{
	auto &renderables = renderer_->compositor_->renderables_;

	renderables.erase(std::remove(renderables.begin(), renderables.end(), this));
}

BitmapRenderable::BitmapRenderable(
    Renderer *renderer, int32_t x, int32_t y, int32_t width, int32_t height, bool is_alpha)
    : orbycomp::BitmapRenderable{x, y, width, height, is_alpha}, renderer_{renderer}
{
}

void
BitmapRenderable::update()
{
}

void
BitmapRenderable::upload_texture(bool attach)
{
}

void
BitmapRenderable::render_finish(uint32_t msec_delta)
{
}

void
BitmapRenderable::calculate_input_region(Region &region)
{
}

BitmapRenderable::~BitmapRenderable()
{
	auto &renderables = renderer_->compositor_->renderables_;

	renderables.erase(std::remove(renderables.begin(), renderables.end(), this));
}

}; // namespace gles

}; // namespace orbycomp
