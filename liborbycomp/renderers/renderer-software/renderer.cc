/*
 * renderer.cc
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "renderer.hh"
#include <orbycomp/base/compositor.hh>

#include <iostream>

extern "C" orbycomp::Renderer *
renderer_init(orbycomp::RendererOptions &opts)
{
	return new orbycomp::softrend::Renderer(static_cast<orbycomp::softrend::Opts &>(opts));
}

namespace orbycomp
{

namespace softrend
{

static inline bool
renderable_sort(Renderable *a, Renderable *b)
{
	return Renderable::comparator_(*a, *b);
}

Renderer::Renderer(Opts &opts) : compositor_{opts.compositor} {}

Renderer::~Renderer() {}

std::shared_ptr<orbycomp::SurfaceRenderable>
Renderer::get_surface_renderable(Surface *surface)
{
	auto ret = std::make_shared<SurfaceRenderable>(this, surface);

	compositor_->renderables_.push_back(ret.get());
	std::sort(compositor_->renderables_.begin(), compositor_->renderables_.end(), renderable_sort);

	return ret;
}

std::shared_ptr<orbycomp::BitmapRenderable>
Renderer::get_bitmap_renderable(int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha)
{
	auto ret = std::make_shared<BitmapRenderable>(this, x, y, width, height, has_alpha);

	compositor_->renderables_.push_back(ret.get());
	std::sort(compositor_->renderables_.begin(), compositor_->renderables_.end(), renderable_sort);

	return ret;
}

void
Renderer::set_cursor(Pointer *pointer, Renderable *renderable, int32_t hotx, int32_t hoty)
{
}

void
Renderer::hide_cursor()
{
}

void
Renderer::setup_output(Output *output, RendererOutputOptions &opts)
{
	OutputOpts &soft_opts = static_cast<OutputOpts &>(opts);

	outputs_.try_emplace(output, std::make_unique<OutputInfo>(this, output, soft_opts.get_fb));
}

void
Renderer::render_output(Output *output)
{
	outputs_[output]->render();
}

OutputInfo::OutputInfo(Renderer *renderer, Output *output, GetFBFunc get_fb)
    : renderer_{renderer}, output_{output}, get_fb_{get_fb}
{
}

OutputInfo::~OutputInfo() {}

void
OutputInfo::render()
{
	auto &renderables = renderer_->compositor_->renderables_;

	auto fb{get_fb_()};

	auto first_renderable = std::find_if_not(renderables.begin(), renderables.end(), [](auto *r) {
		return r->layer() == Renderable::Layer::Hidden;
	});

	if (first_renderable == renderables.end()) {
		return;
	}

	for (auto iter = first_renderable; iter != renderables.end(); ++iter) {
		auto *r = *iter;
		auto *soft_renderable = dynamic_cast<SoftwareRenderable *>(r);

		auto &texture = soft_renderable->get_texture();

		rendering::software::BitmapSurface surf{texture.get_bitmap_surface()};

		const auto render_region = [this, &r, &fb, &surf](const Region &region, bool is_alpha) {
			const auto *output = this->output_;
			const auto &mode = output->current_mode();
			const auto width = mode.width_;
			const auto height = mode.height_;

			const auto output_end_x = output->x() + width;
			const auto output_end_y = output->y() + height;

			for (const auto &rect : region) {
				int32_t src_x = rect.x1 - r->x() + r->x_off();
				int32_t src_y = rect.y1 - r->y() + r->y_off();
				int32_t src_width = rect.x2 - rect.x1;
				int32_t src_height = rect.y2 - rect.y1;

				int32_t dest_x_begin = rect.x1 - output->x();
				int32_t dest_y_begin = rect.y1 - output->y();
				int32_t dest_x_end = rect.x2 - output->x();
				int32_t dest_y_end = rect.y2 - output->y();

				if (dest_x_begin < 0) {
					// Two cases: either this whole region is outside of the display,
					// in which cas we can just ignore this, or only this side is
					// in the negative relative.
					if (dest_x_end < 0) {
						continue;
					} else {
						src_x += dest_x_begin;
						src_width += dest_x_begin;
						dest_x_begin = 0;
					}
				}

				if (dest_y_begin < 0) {
					// Two cases: either this whole region is outside of the display,
					// in which cas we can just ignore this, or only this side is
					// in the negative relative.
					if (dest_y_end < 0) {
						continue;
					} else {
						src_y += dest_y_begin;
						src_height += dest_y_begin;
						dest_y_begin = 0;
					}
				}

				if (dest_x_end >= output_end_x) {
					// Again, if the beginning of the region is outside, ignore this,
					// otherwise clip appropriately.
					if (dest_x_begin >= output_end_x) {
						continue;
					} else {
						auto overflow = dest_x_end - output_end_x;
						src_width -= overflow;
					}
				}

				if (dest_y_end >= output_end_y) {
					// Again, if the beginning of the region is outside, ignore this,
					// otherwise clip appropriately.
					if (dest_y_begin >= output_end_y) {
						continue;
					} else {
						auto overflow = dest_y_end - output_end_y;
						src_height -= overflow;
					}
				}

				rendering::software::BitmapSurface::BlitRegion region{src_x, src_y, src_width,
										      src_height};

				surf.blit_to(region, fb, dest_x_begin, dest_y_begin, is_alpha);
			}
		};

		render_region(r->opaque_region(), false);
		render_region(soft_renderable->transparent_region(), true);
	}
}

SurfaceRenderable::SurfaceRenderable(Renderer *renderer, Surface *surface)
    : orbycomp::SurfaceRenderable{surface}, renderer_{renderer}
{
}

void
SurfaceRenderable::update()
{
	std::sort(renderer_->compositor_->renderables_.begin(), renderer_->compositor_->renderables_.end(),
		  renderable_sort);

	if (layer_ == Layer::Hidden) {
		return;
	}

	x_ = surface_->x();
	y_ = surface_->y();

	if (surface_->geometry_) {
		x_off_ = surface_->geometry_->x_;
		y_off_ = surface_->geometry_->y_;
	}

	auto our_pos = std::find_if(renderer_->compositor_->renderables_.begin(),
				    renderer_->compositor_->renderables_.end(), [this](const auto *a) {
					    const auto *check_a = dynamic_cast<const SurfaceRenderable *>(a);

					    return check_a == this;
				    });

	Region opaque_region{};
	Region input_region{};
	for (auto it = our_pos + 1; it != renderer_->compositor_->renderables_.end(); ++it) {
		auto *r = *it;
		input_region += r->input_region();
		opaque_region += r->opaque_region();
	}

	calculate_opaque_region(opaque_region);
	calculate_input_region(input_region);

	for (auto it = std::reverse_iterator(our_pos); it != renderer_->compositor_->renderables_.rend();
	     ++it) {
		auto *r = *it;
		if (r->layer() == Layer::Hidden) {
			break;
		}

		auto *renderable = dynamic_cast<SurfaceRenderable *>(r);

		renderable->calculate_opaque_region(opaque_region);
		renderable->calculate_input_region(input_region);
	}

	renderer_->compositor_->schedule_full_repaint();
}

void
SurfaceRenderable::upload_texture(bool attach)
{
	if (auto *soft_buf = wl_shm_buffer_get(surface_->buffer())) {
		auto width = wl_shm_buffer_get_width(soft_buf);
		auto height = wl_shm_buffer_get_height(soft_buf);
		auto stride = wl_shm_buffer_get_stride(soft_buf);

		has_alpha_ = wl_shm_buffer_get_format(soft_buf) == WL_SHM_FORMAT_ARGB8888;

		width_ = width;
		height_ = height;

		auto size = height * stride;

		texture_ = Texture{width, height, stride};

		wl_shm_buffer_begin_access(soft_buf);

		uint8_t *data = static_cast<uint8_t *>(wl_shm_buffer_get_data(soft_buf));
		std::copy_n(data, size, texture_.data());

		wl_shm_buffer_end_access(soft_buf);

		surface_->reset_damage();

		wl_buffer_send_release(surface_->buffer());
	} else {
		throw std::runtime_error("Only SHM buffers are usable with software rendering");
	}
}

void
SurfaceRenderable::render_finish(uint32_t msec_delta)
{
	orbycomp::SurfaceRenderable::render_finish(msec_delta);
}

void
SurfaceRenderable::calculate_input_region(Region &region)
{
	Region tmp{surface_->input_region()};
	Region clip{{0, 0, width_, height_}};

	input_region_ = tmp.intersect(clip);
	input_region_.translate(offset_x(), offset_y());

	input_region_ -= region;

	region += input_region_;
}

void
SurfaceRenderable::calculate_opaque_region(Region &region)
{
	if (has_alpha_) {
		Region tmp{surface_->opaque_region()};
		Region clip{{0, 0, width_, height_}};

		opaque_region_ = tmp.intersect(clip);
		opaque_region_.translate(offset_x(), offset_y());

		opaque_region_ -= region;

		region += opaque_region_;

		transparent_region_ = clip.translated(offset_x(), offset_y()) - opaque_region_;
	} else {
		opaque_region_ = Region{{0, 0, width_, height_}};
		opaque_region_.translate(offset_x(), offset_y());

		opaque_region_ -= region;

		region += opaque_region_;

		transparent_region_ = {};
	}
}

const Region &
SurfaceRenderable::transparent_region()
{
	return transparent_region_;
}

SurfaceRenderable::~SurfaceRenderable()
{
	auto &renderables = renderer_->compositor_->renderables_;

	renderables.erase(std::remove(renderables.begin(), renderables.end(), this));
}

BitmapRenderable::BitmapRenderable(
    Renderer *renderer, int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha)
    : orbycomp::BitmapRenderable{x, y, width, height, has_alpha}, renderer_{renderer}
{
}

BitmapRenderable::~BitmapRenderable()
{
	auto &renderables = renderer_->compositor_->renderables_;

	renderables.erase(std::remove(renderables.begin(), renderables.end(), this));
}

void
BitmapRenderable::update()
{
	std::sort(renderer_->compositor_->renderables_.begin(), renderer_->compositor_->renderables_.end(),
		  renderable_sort);

	if (layer_ == Layer::Hidden) {
		return;
	}
}

void
BitmapRenderable::upload_texture(bool attach)
{
}

void
BitmapRenderable::render_finish(uint32_t msec_delta)
{
}

void
BitmapRenderable::calculate_input_region(Region &region)
{
	// TODO: Make bitmap renderables interactable
}

void
BitmapRenderable::calculate_opaque_region(Region &region)
{
	if (has_alpha_) {
		opaque_region_ = Region{};
		transparent_region_ = Region{{x_, y_, width_, height_}};
		transparent_region_ -= region;
	} else {
		transparent_region_ = Region{};
		opaque_region_ = Region{{x_, y_, width_, height_}};
		opaque_region_ -= region;

		region += opaque_region_;
	}
}

const Region &
BitmapRenderable::transparent_region()
{
	return transparent_region_;
}

Texture::Texture(int32_t width, int32_t height, int32_t stride)
    : width_{width}, height_{height}, stride_{stride}, data_{}
{
	data_.resize(stride_ * height_);
}

}; // namespace softrend

}; // namespace orbycomp
