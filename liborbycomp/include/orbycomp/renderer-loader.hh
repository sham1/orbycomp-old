/*
 * renderer-loader.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <dlfcn.h>
#include <memory>
#include <orbycomp/renderer.hh>
#include <string>

namespace orbycomp
{

class RendererHandle
{

	friend RendererHandle load_renderer(const std::string &renderer_name, RendererOptions &opts);

	struct dynamic_handle_closer {
		void
		operator()(void *handle) const
		{
			// dlclose(handle);
		}
	};

	std::unique_ptr<void, dynamic_handle_closer> handle_;

	std::unique_ptr<Renderer> renderer_;

	RendererHandle(std::unique_ptr<Renderer> &&renderer,
		       std::unique_ptr<void, dynamic_handle_closer> &&handle)
	    : handle_(std::move(handle)), renderer_(std::move(renderer))

	{
	}

public:
	RendererHandle() = default;

	~RendererHandle() = default;

	inline Renderer &
	operator*() const
	{
		return *renderer_.get();
	}

	inline Renderer *
	operator->() const noexcept
	{
		return renderer_.get();
	}

	inline Renderer *
	get() const
	{
		return renderer_.get();
	}

	RendererHandle &
	operator=(RendererHandle &&other)
	{
		this->renderer_ = std::move(other.renderer_);
		this->handle_ = std::move(other.handle_);

		return *this;
	}

	RendererHandle(RendererHandle &&other)
	    : handle_{std::move(other.handle_)}, renderer_{std::move(other.renderer_)}
	{
	}

	RendererHandle &operator=(const RendererHandle &other) = delete;
	RendererHandle(const RendererHandle &other) = delete;
};

RendererHandle load_renderer(const std::string &renderer_name, RendererOptions &opts);

} // namespace orbycomp
