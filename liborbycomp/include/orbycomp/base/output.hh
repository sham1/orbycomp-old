/*
 * output.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <string>
#include <vector>
#include <wayland-server.h>

namespace orbycomp
{

class Compositor;

class Output
{
public:
	struct Mode {
		int32_t width_{0};
		int32_t height_{0};
		int32_t refresh_{0};
		enum wl_output_mode flags_ { static_cast<enum wl_output_mode>(0) };

		Mode() = default;

		Mode(int32_t width, int32_t height, int32_t refresh, enum wl_output_mode flags)
		    : width_{width}, height_{height}, refresh_{refresh}, flags_{flags}
		{
		}
	};

	const Mode &current_mode() const;

	const std::vector<Mode> &
	modes() const
	{
		return modes_;
	}

	int32_t
	x() const
	{
		return x_;
	}

	int32_t
	y() const
	{
		return y_;
	}

	std::pair<int32_t, int32_t>
	get_physical_dims() const
	{
		return std::pair(phys_width_, phys_height_);
	}

	int32_t
	scale() const
	{
		return scale_;
	}

	enum wl_output_transform
	transform() const
	{
		return transform_;
	}

	const std::string &
	make() const
	{
		return make_;
	}

	const std::string &
	model() const
	{
		return model_;
	}

	virtual ~Output();

protected:
	Compositor *compositor_;

	int32_t x_{};
	int32_t y_{};

	std::vector<Mode> modes_{};

	// In millimeters
	int32_t phys_width_, phys_height_;
	enum wl_output_subpixel subpixel_ { WL_OUTPUT_SUBPIXEL_UNKNOWN };

	int32_t scale_{1};
	enum wl_output_transform transform_ { WL_OUTPUT_TRANSFORM_NORMAL };

	std::string make_{};
	std::string model_{};

	std::unique_ptr<struct wl_global, decltype(&wl_global_destroy)> global_;
	struct wl_list resource_list;

	Output(Compositor *compositor,
	       int32_t x,
	       int32_t y,
	       std::vector<Mode> &&modes,
	       int32_t phys_width,
	       int32_t phys_height,
	       enum wl_output_subpixel subpixel,
	       int32_t scale,
	       enum wl_output_transform transform,
	       const std::string &make,
	       const std::string &model);

private:
	static void on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id);

	static void wl_output_release(struct wl_client *client, struct wl_resource *resource);

	constexpr static struct wl_output_interface iface = {
	    Output::wl_output_release,
	};
};

} // namespace orbycomp
