/*
 * renderable.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <orbycomp/region.hh>

namespace orbycomp
{

class Surface;

class Renderable
{
public:
	virtual ~Renderable() {}

	enum class Layer : int {
		Hidden = 0,
		Background = 1,
		Widget = 2,
		Window = 3,
		Overlay = 4,
		Lock = 5,
	};

	virtual void update() = 0;
	virtual void upload_texture(bool attach) = 0;

	constexpr static struct Comparator {
		bool operator()(const Renderable &a, const Renderable &b) const;
	} comparator_{};

	inline std::int32_t
	x() const
	{
		return x_;
	}
	inline std::int32_t
	y() const
	{
		return y_;
	}
	inline std::int32_t
	width() const
	{
		return width_;
	}
	inline std::int32_t
	height() const
	{
		return height_;
	}

	inline std::int32_t
	x_off() const
	{
		return x_off_;
	}

	inline std::int32_t
	y_off() const
	{
		return y_off_;
	}

	/**
	 * Returns the renderable's X position offset by whatever
	 * geometry offset it has.
	 *
	 * It's equivalent to
	 *
	 * `x() - x_off()`
	 */
	inline std::int32_t
	offset_x() const
	{
		return x() - x_off_;
	}

	/**
	 * Returns the renderable's Y position offset by whatever
	 * geometry offset it has.
	 *
	 * It's equivalent to
	 *
	 * `y() - y_off()`
	 */
	inline std::int32_t
	offset_y() const
	{
		return y() - y_off_;
	}

	inline const Region &
	opaque_region() const
	{
		return opaque_region_;
	}

	inline const Region &
	visible_region() const
	{
		return visible_region_;
	}

	inline const Region &
	input_region() const
	{
		return input_region_;
	}

	inline Layer
	layer() const
	{
		return layer_;
	}

	inline Layer &
	layer()
	{
		return layer_;
	}

	inline std::int32_t
	relative_pos() const
	{
		return relative_pos_;
	}

	inline std::int32_t &
	relative_pos()
	{
		return relative_pos_;
	}

	void
	set_parent(Renderable *parent)
	{
		parent_ = parent;
	}

	Renderable *
	parent() const
	{
		return parent_;
	}

	virtual void render_finish(uint32_t msec_delta) = 0;

protected:
	std::int32_t x_;
	std::int32_t y_;
	std::int32_t width_;
	std::int32_t height_;
	std::int32_t x_off_{0};
	std::int32_t y_off_{0};

	Layer layer_{Layer::Hidden};

	Renderable *parent_{nullptr};

	Renderable(int32_t x, int32_t y, int32_t width, int32_t height)
	    : x_{x}, y_{y}, width_{width}, height_{height}
	{
	}

	// This is the position within the layer.
	// Higher means higher on the layer.
	std::int32_t relative_pos_;

	Region opaque_region_{};

	Region visible_region_{};

	Region input_region_{};
};

// Implementors of this interface class must
// fill in `width_` and `height_` after the fact
// according to the underlying `struct wl_buffer`.
class SurfaceRenderable : public Renderable
{
public:
	virtual ~SurfaceRenderable() {}

	virtual void render_finish(uint32_t tv_msec) override;

	inline Surface *
	surface()
	{
		return surface_;
	}

protected:
	Surface *surface_;

	SurfaceRenderable(Surface *surface);
};

class BitmapRenderable : public Renderable
{
public:
	virtual ~BitmapRenderable() {}

	inline const uint8_t *
	data() const
	{
		return buffer_.data();
	}

	inline uint8_t *
	data()
	{
		return buffer_.data();
	}

	size_t
	stride() const
	{
		return width_ * 4;
	}

protected:
	std::vector<uint8_t> buffer_;
	bool has_alpha_;

	BitmapRenderable(int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha)
	    : Renderable(x, y, width, height), has_alpha_{has_alpha}
	{
		buffer_.resize(stride() * height_);
	}
};

} // namespace orbycomp
