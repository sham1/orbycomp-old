/*
 * xwayland.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <ev.h>
#include <memory>
#include <optional>
#include <orbycomp/xwm.hh>
#include <string>

namespace orbycomp
{

class Compositor;

class XWaylandCtx
{
	friend XWindowManager;

public:
	XWaylandCtx(Compositor *compositor);
	~XWaylandCtx();

private:
	Compositor *compositor_{nullptr};

	int dpynum_{0};

	void create_x_lock_file(std::string &lockfile_path);
	int bind_abstract_sock();
	int bind_unix_sock();

	int abstract_fd_{-1};
	int unix_fd_{-1};
	int wm_fd_{-1};

	pid_t x_server_pid_{-1};

	// For lazy initialization of the X server
	struct ev_io abstract_sock_listener_ {
	};
	struct ev_io unix_sock_listener_ {
	};

	static void on_connect_event(EV_P_ struct ev_io *w, int revents);

	struct wl_client *xwayland_client_{nullptr};
	bool spawn_x_server(std::string &display_name);

	struct ev_signal on_xserver_ready_listener_ {
	};
	static void on_xserver_ready(EV_P_ struct ev_signal *w, int revents);

	std::optional<XWindowManager> xwm_{std::nullopt};
};

}; // namespace orbycomp
