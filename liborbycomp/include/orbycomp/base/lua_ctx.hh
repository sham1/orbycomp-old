/*
 * lua_ctx.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <orbycomp/lua.hh>

namespace orbycomp
{

class Compositor;

class LuaCtx
{
public:
	LuaCtx(Compositor *compositor);
	~LuaCtx();

	void load_user_init();

	lua::Context &
	ctx()
	{
		return ctx_;
	}

private:
	Compositor *compositor_;

	lua::Context ctx_{lua::Context::create_with_stdlibs()};
};

}; // namespace orbycomp
