/*
 * pixel-formats.hh
 *
 * Copyright (C) 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <wayland-server.h>

namespace orbycomp
{

struct PixelFormat {
	std::uint32_t fourcc_format;

	std::uint32_t red_depth;
	std::uint32_t green_depth;
	std::uint32_t blue_depth;
	std::uint32_t alpha_depth;

	std::uint32_t bpp;

	std::uint32_t plane_count;

	static const PixelFormat *get_by_shm(enum wl_shm_format format);
	static const PixelFormat *get_by_fourcc(uint32_t format);
};

} // namespace orbycomp
