/*
 * keyboard.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <orbycomp/renderer.hh>
#include <sigc++/sigc++.h>
#include <variant>
#include <wayland-server.h>

namespace orbycomp
{

class Seat;

class Pointer
{
	Seat *seat_;

	// wl_resource::link
	struct wl_list resources_ {
	};

	double x_{0};
	double y_{0};

	std::shared_ptr<BitmapRenderable> default_cursor_{};

	class SurfaceFocus
	{
		friend Pointer;

		Pointer *pointer_;

		struct wl_resource *surface_resource_;
		struct wl_resource *pointer_resource_;

		struct wl_listener on_surface_destroy_;
		struct wl_listener on_pointer_destroy_;

		static void on_surface_destroy_cb(struct wl_listener *listener, void *user_data);
		static void on_pointer_destroy_cb(struct wl_listener *listener, void *user_data);

	public:
		SurfaceFocus(Pointer *pointer,
			     struct wl_resource *surface_resource,
			     struct wl_resource *pointer_resource);
		~SurfaceFocus();

		void leave_focus();
	};

	std::variant<std::monostate, SurfaceFocus> focus_{};

	bool surface_cursor_realized_{false};
	sigc::connection on_cursor_surface_commit_;

	sigc::connection mouse_move_connection_;
	void on_move_cb(double x, double y, uint64_t tv_usec);

	void update_surface_move(SurfaceFocus *surface_focus,
				 SurfaceRenderable *surface_renderable,
				 double x,
				 double y,
				 uint64_t tv_usec);
	void focus_on_surface(struct wl_client *surface_client,
			      SurfaceRenderable *surface_renderable,
			      struct wl_resource *surface_resource,
			      double x,
			      double y);

	using GrabCB = std::function<void(double x, double y, double dx, double dy)>;

	struct Grab {
		friend Pointer;

		Pointer *pointer_;

		struct wl_resource *pointer_resource_;
		struct wl_listener on_pointer_destroy_;

		static void on_pointer_destroy_cb(struct wl_listener *listener, void *user_data);

		GrabCB on_move_;

		std::optional<std::function<void()>> on_release_;

		Grab(Pointer *pointer,
		     struct wl_resource *pointer_resource,
		     GrabCB &&on_move,
		     std::optional<std::function<void()>> &&on_release);
		~Grab();
	};
	std::optional<Grab> grab_{};

public:
	Pointer(Seat *seat);
	~Pointer();

	void create_resource(struct wl_client *client, uint32_t id, int seat_version);

	static void on_resource_destroy(struct wl_resource *resource);

	static void set_cursor(struct wl_client *client,
			       struct wl_resource *resource,
			       uint32_t serial,
			       struct wl_resource *surface,
			       int32_t hotspot_x,
			       int32_t hotspot_y);

	static void release(struct wl_client *client, struct wl_resource *resource);

	static constexpr struct wl_pointer_interface iface = {
	    Pointer::set_cursor,
	    Pointer::release,
	};

	enum class ButtonState {
		Press,
		Release,
	};

	void set_location(uint64_t tv_usec, double x, double y);
	void move(uint64_t tv_usec, double dx, double dy, double dx_unaccel, double dy_unaccel);
	void feed_button_event(uint64_t tv_usec, uint32_t button, ButtonState state);

	void set_grab(struct wl_client *client,
		      GrabCB &&on_move,
		      std::optional<std::function<void()>> &&on_release);

	inline double
	x() const
	{
		return x_;
	}

	inline double
	y() const
	{
		return y_;
	}

	sigc::signal<void(Pointer *, double, double, uint64_t)> on_move_signal;
};

} // namespace orbycomp
