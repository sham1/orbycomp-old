/*
 * compositor.hh
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <ev.h>
#include <memory>
#include <orbycomp-conf.h>
#include <orbycomp/backend-loader.hh>
#include <orbycomp/base/data-device.hh>
#include <orbycomp/base/keybinds.hh>
#include <orbycomp/base/lua_ctx.hh>
#include <orbycomp/base/output.hh>
#include <orbycomp/base/seat.hh>
#include <orbycomp/base/shell.hh>
#include <orbycomp/base/xwayland.hh>
#include <orbycomp/renderer-loader.hh>
#include <orbycomp/util/idle-event-queue.hh>
#include <orbycomp/xcursor.hh>
#include <orbycomp/xdg-shell.hh>
#include <queue>
#include <sigc++/sigc++.h>
#include <unordered_map>
#include <wayland-server-protocol.h>

#ifdef ENABLED_DBUS
#include <orbycomp/ev-dbus.hh>
#endif

namespace orbycomp
{

class Compositor
{
public:
	Compositor(std::unique_ptr<struct wl_display, decltype(&wl_display_destroy)> &&display,
		   std::unique_ptr<struct ev_loop, decltype(&ev_loop_destroy)> &&loop);

	void run();

	~Compositor();

	inline struct ev_loop *
	loop()
	{
		return loop_.get();
	}

	void
	ensure_display()
	{
		if (!display_) {
			std::fprintf(stderr, "Hello!\n");
			if (!find_usable_socket()) {
				throw std::system_error(errno, std::generic_category(),
							"Could not create Wayland socket");
			}
			setenv("WAYLAND_DISPLAY", dpyname_.c_str(), 1);
		}
	}

	inline struct wl_display *
	display()
	{
	        ensure_display();
		return display_.get();
	}

	Seat *get_seat(const std::string &name);
	std::vector<Output *> outputs_{};

	std::vector<Renderable *> renderables_{};

	KeybindContext &
	keybind_ctx()
	{
		return keybind_ctx_;
	}

	// `true` for becoming active, `false` for becoming inactive.
	sigc::signal<void(bool)> activity_change_signal;

	sigc::signal<void(Seat *)> seat_added_signal;

	sigc::signal<void(Surface &)> on_surface_create_signal;

	sigc::signal<void(Output *)> on_output_added_signal;

	void set_active(bool active);
	inline bool
	get_activity() const
	{
		return active_;
	}

	inline BackendBase &
	backend()
	{
		return *backend_;
	}

	inline Renderer &
	renderer()
	{
		return *backend_->get_renderer();
	}

	inline xcursor::CursorContext &
	cursor_ctx()
	{
		return cursor_ctx_;
	}

	inline const std::unordered_map<std::string, Seat *> &
	seats() const
	{
		return seats_;
	}

	inline LuaCtx &
	lua()
	{
		return lua_;
	}

	inline shell::Shell &
	shell()
	{
		return shell_;
	}

	Seat *current_seat();

	void add_uncancellable_idle_task(std::function<void(void)> &&func);
	util::EventToken add_idle_task(std::function<void(void)> &&func);

	sigc::signal<void(Seat *)> on_seat_destroy_signal;

	sigc::signal<void(Output *)> on_output_destroy_signal;

	void schedule_full_repaint();
	util::EventToken repaint_idle_token_;
	std::unordered_set<Output *> scheduled_outputs_;
	void repaint_scheduled_outputs();

private:
	std::unique_ptr<struct wl_display, decltype(&wl_display_destroy)> display_;
	std::unique_ptr<struct ev_loop, decltype(&ev_loop_destroy)> loop_;

	std::string dpyname_;

	LuaCtx lua_;

	KeybindContext keybind_ctx_;

public:
#ifdef ENABLED_DBUS
	dbus::Context system_context;
	dbus::Context session_context;
#endif

private:
	void create_compositor_global();
	bool find_usable_socket();

	struct ev_io wayland_watcher;
	struct ev_prepare wayland_prepare;
	struct ev_signal sigint_handler;

	struct ev_prepare idle_task_executor;
	static void execute_idle_task(EV_P_ ev_prepare *w, int revents);
	util::EventQueue<void(void)> idle_task_queue{};

	void dispatch_wayland_events();
	void prepare_wayland();

	friend void wayland_io_cb(EV_P_ ev_io *w, int revents);
	friend void wayland_prepare_cb(EV_P_ ev_prepare *w, int revents);

	xcursor::CursorContext cursor_ctx_;

	std::unordered_map<std::string, Seat *> seats_;

	Keybind debug_escape_bind{Modifier::Control | Modifier::Alt, XKB_KEY_BackSpace,
				  [this](Modifier, xkb_keysym_t) { ev_break(this->loop()); }};

	bool active_{true};

	std::unique_ptr<struct wl_global, decltype(&wl_global_destroy)> global_;
	Backend backend_;

	shell::Shell shell_;
	shell::xdg::Shell xdg_shell_;

	DataDeviceManager data_manager_;

	XWaylandCtx xwayland_ctx_;

	static void on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id);

	static void create_surface(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static void create_region(struct wl_client *client, struct wl_resource *resource, uint32_t id);

	static constexpr struct wl_compositor_interface iface = {
	    Compositor::create_surface,
	    Compositor::create_region,
	};

	std::unique_ptr<struct wl_global, decltype(&wl_global_destroy)> subcompositor_global_;
	static void
	on_subcompositor_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id);

	static void subcompositor_destroy(struct wl_client *client, struct wl_resource *resource);
	static void subcompositor_get_subsurface(struct wl_client *client,
						 struct wl_resource *resource,
						 uint32_t id,
						 struct wl_resource *surface,
						 struct wl_resource *parent);
	static constexpr struct wl_subcompositor_interface subcomp_iface = {
	    Compositor::subcompositor_destroy,
	    Compositor::subcompositor_get_subsurface,
	};

	// Lua methods
	static int lua_get_default_keymap(struct lua_State *L);
	static int lua_get_keymaps_table(struct lua_State *L);
	static int lua_execute(struct lua_State *L);
	static constexpr struct luaL_Reg compositor_reg[] = {
	    {"default_keymap", &Compositor::lua_get_default_keymap},
	    {"keymaps", &Compositor::lua_get_keymaps_table},
	    {"execute", &Compositor::lua_execute},
	    {nullptr, nullptr},
	};
};

} // namespace orbycomp
