/*
 * shell.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <list>
#include <memory>
#include <optional>
#include <orbycomp/base/keybinds.hh>
#include <orbycomp/base/surface.hh>
#include <orbycomp/lua.hh>

namespace orbycomp
{

class Compositor;

class Seat;
class Keyboard;
class Pointer;

namespace shell
{

class ShellImpl;
class ShellSeat;
class Toplevel;
class Surface;
class Popup;

class Shell
{
public:
	Shell(Compositor *compositor);
	void push_lua_global();

	void maximize_focus_window();

	void close_focus_window();

	inline Compositor *
	compositor()
	{
		return compositor_;
	}

	void add_window(Toplevel *toplevel);
	void remove_window(Toplevel *toplevel);

	void add_popup(Popup *popup);
	void remove_popup(Popup *popup);

private:
	friend ShellSeat;

	Compositor *compositor_;

	lua::Context::Ref create_shell_ref();
	lua::Context::Ref shell_ref_;

	std::unordered_map<Seat *, std::unique_ptr<ShellSeat>> seats_;

	void on_add_seat(Seat *seat);
	void on_destroy_seat(Seat *seat);

	std::list<Surface *> stack_{};

	static int lua_maximize_top(lua_State *L);
	static int lua_close_top(lua_State *L);
	static constexpr struct luaL_Reg shell_reg[] = {
	    {"maximize_top", Shell::lua_maximize_top},
	    {"close_top", Shell::lua_close_top},
	    {nullptr, nullptr},
	};
};

class ShellSeat
{
public:
	ShellSeat(Shell *shell, Seat *seat);
	~ShellSeat();

private:
	Shell *shell_;
	Seat *seat_;

	sigc::connection on_seat_caps_change_;

	void on_caps_changed(std::optional<Keyboard> &keyboard, std::optional<Pointer> &pointer);
};

class Surface
{
public:
	Surface() {}
	virtual ~Surface() {}

	virtual void map() = 0;
	virtual void unmap() = 0;

	virtual Toplevel *toplevel() = 0;
	virtual Popup *popup() = 0;

	virtual orbycomp::Surface *surface() = 0;
};

class Toplevel
{
public:
	Toplevel() {}
	virtual ~Toplevel() {}

	virtual Surface *surface() = 0;

	virtual void set_size(int32_t width, int32_t height) = 0;
	virtual void set_activity(bool active) = 0;
	virtual void set_maximized(bool maximized) = 0;

	virtual void close() = 0;
};

class Popup
{
public:
	Popup() {}
	virtual ~Popup() {}

	virtual Surface *surface() = 0;

	virtual Surface *transient_for() = 0;

	virtual void set_activity(bool active) = 0;
};

}; // namespace shell

} // namespace orbycomp
