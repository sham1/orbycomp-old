/*
 * surface.hh
 *
 * Copyright (C) 2019, 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <optional>
#include <orbycomp/affine.hh>
#include <orbycomp/base/renderable.hh>
#include <orbycomp/region.hh>
#include <sigc++/sigc++.h>
#include <wayland-server-protocol.h>

namespace orbycomp
{

class Compositor;

class SubSurface;

class Surface
{
	friend SubSurface;

private:
	static const Region infinite_region;

	struct PendingBufferState {
		struct wl_resource *new_buffer;
		int32_t xoff;
		int32_t yoff;
	};

	Compositor *compositor_;

	struct PendingState {
		std::optional<PendingBufferState> attach;
		std::optional<Region> surface_damage;
		std::optional<struct wl_list> frame_cbs;
		std::optional<Region> opaque_region;
		std::optional<Region> input_region;
		std::optional<enum wl_output_transform> transform;
		std::optional<int32_t> scale;
		std::optional<Region> buffer_damage;
	} pending_state;

	void attach_buffer(struct wl_resource *buffer, int32_t xoff, int32_t yoff);

	void add_to_surface_damage(Region::Rect rect);

	void add_frame(struct wl_client *client, uint32_t callback);

	void commit_state(PendingState &pending_state);

	void add_to_buffer_damage(Region::Rect rect);

	// wl_surface implementation begin
	static void destroy(struct wl_client *client, struct wl_resource *resource);

	static void attach(struct wl_client *client,
			   struct wl_resource *resource,
			   struct wl_resource *buffer,
			   int32_t xoff,
			   int32_t yoff);

	static void damage(struct wl_client *client,
			   struct wl_resource *resource,
			   int32_t x,
			   int32_t y,
			   int32_t width,
			   int32_t height);

	static void frame(struct wl_client *client, struct wl_resource *resource, uint32_t callback);

	static void
	set_opaque_region(struct wl_client *client, struct wl_resource *resource, struct wl_resource *region);

	static void
	set_input_region(struct wl_client *client, struct wl_resource *resource, struct wl_resource *region);

	static void commit(struct wl_client *client, struct wl_resource *resource);

	static void
	set_buffer_transform(struct wl_client *client, struct wl_resource *resource, int32_t transform);

	static void set_buffer_scale(struct wl_client *client, struct wl_resource *resource, int32_t scale);

	static void buffer_damage(struct wl_client *client,
				  struct wl_resource *resource,
				  int32_t x,
				  int32_t y,
				  int32_t width,
				  int32_t height);

	static constexpr struct wl_surface_interface surface_iface = {
	    Surface::destroy,
	    Surface::attach,
	    Surface::damage,
	    Surface::frame,
	    Surface::set_opaque_region,
	    Surface::set_input_region,
	    Surface::commit,
	    Surface::set_buffer_transform,
	    Surface::set_buffer_scale,
	    Surface::buffer_damage,
	};
	// wl_surface implementation end

	static void reset_pending(PendingState &pending_state);

	struct wl_resource *resource_;

	int32_t x_{0}, y_{0};

	struct wl_resource *buffer_{nullptr};

	Region opaque_region_{};
	Region input_region_{};

	Region damage_region{};

	enum wl_output_transform transform { WL_OUTPUT_TRANSFORM_NORMAL };
	int32_t scale{1};

	// struct wl_resource
	struct wl_list frames {
	};

	void calculate_transformation_matrices();
	affine::Mat4 surface_to_buffer_transform{affine::Mat4::get_identity()};
	affine::Mat4 buffer_to_surface_transform{affine::Mat4::get_identity()};

	std::shared_ptr<SurfaceRenderable> renderable_{};

	Surface *parent_{nullptr};
	SubSurface *subsurf_{nullptr};

	std::list<Surface *> children_above{};
	std::list<Surface *> children_below{};

	struct wl_listener on_attached_buffer_destroy_;
	static void on_attached_buffer_destroy_cb(struct wl_listener *listener, void *data);

public:
	Surface(Compositor *compositor, struct wl_resource *resource);

	~Surface();

	static void create_surface(struct wl_client *client, uint32_t id, Compositor *compositor);

	struct wl_resource *
	resource()
	{
		return resource_;
	}

	const int32_t &
	x() const
	{
		return x_;
	}

	const int32_t &
	y() const
	{
		return y_;
	}

	int32_t &
	x()
	{
		return x_;
	}

	int32_t &
	y()
	{
		return y_;
	}

	struct wl_resource *
	buffer() const
	{
		return buffer_;
	}

	sigc::signal<void(Surface *)> on_commit_signal_{};

	// Can be used to indicate where within the surface's buffer the
	// "real" content resides.
	struct Geometry {
		int32_t x_;
		int32_t y_;
		int32_t width_;
		int32_t height_;

		Geometry(int32_t x, int32_t y, int32_t width, int32_t height)
		    : x_{x}, y_{y}, width_{width}, height_{height}
		{
		}
	};

	std::optional<Geometry> geometry_{};

	inline Region
	damage() const
	{
		return damage_region;
	}

	void reset_damage();

	inline const Region &
	opaque_region() const
	{
		return opaque_region_;
	}

	inline const Region &
	input_region() const
	{
		return input_region_;
	}

	inline SurfaceRenderable *
	renderable()
	{
		return renderable_.get();
	}

	void call_frame_cbs(uint32_t render_msec);
};

inline bool
operator==(const Surface::Geometry &lhs, const Surface::Geometry &rhs)
{
	return lhs.x_ == rhs.x_ && lhs.y_ == rhs.y_ && lhs.width_ == rhs.width_ && lhs.height_ == rhs.height_;
}

inline bool
operator!=(const Surface::Geometry &lhs, const Surface::Geometry &rhs)
{
	return !(lhs == rhs);
}

class SubSurface
{
	friend Surface;

private:
	enum class Placing { PlaceAbove, PlaceBelow };
	struct ZOrderPlacement {
		Placing relation_;
		Surface *surface_;

		ZOrderPlacement(Placing relation, Surface *surface) : relation_{relation}, surface_{surface}
		{
		}
	};
	std::optional<ZOrderPlacement> pending_placement;

	Surface::PendingState state_cache;
	void commit_to_cache(Surface::PendingState &pending_state);

	struct wl_resource *resource_;

	Surface *surface_;
	struct wl_listener on_surface_destroy_;
	static void on_surface_destroy_cb(struct wl_listener *listener, void *data);

	struct wl_listener on_parent_destroy_;
	static void on_parent_destroy_cb(struct wl_listener *listener, void *data);

	bool synchronized_{true};

	bool has_set_position{false};
	int32_t x_pos_{0};
	int32_t y_pos_{0};

	sigc::connection on_parent_commit_;
	void parent_commit_handler();
	void synchronize();

	sigc::connection on_surface_commit_;
	void surface_commit_handler();

	bool is_synchronized() const;

	static void on_resource_destroy(struct wl_resource *resource);

	static void destroy(struct wl_client *client, struct wl_resource *resource);
	static void
	set_position(struct wl_client *client, struct wl_resource *resource, int32_t x, int32_t y);
	static void
	place_above(struct wl_client *client, struct wl_resource *resource, struct wl_resource *sibling);
	static void
	place_below(struct wl_client *client, struct wl_resource *resource, struct wl_resource *sibling);
	static void set_sync(struct wl_client *client, struct wl_resource *resource);
	static void set_desync(struct wl_client *client, struct wl_resource *resource);

	static constexpr struct wl_subsurface_interface iface = {
	    SubSurface::destroy,     SubSurface::set_position, SubSurface::place_above,
	    SubSurface::place_below, SubSurface::set_sync,     SubSurface::set_desync,
	};

public:
	SubSurface(struct wl_client *client, Surface *surface, Surface *parent, uint32_t id);
	~SubSurface();
}; // namespace orbycomp

} // namespace orbycomp
