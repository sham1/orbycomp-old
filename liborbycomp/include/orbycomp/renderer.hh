/*
 * backend.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <orbycomp/base/renderable.hh>

namespace orbycomp
{

class Surface;
class Pointer;
class Compositor;
class Output;

class RendererOutputOptions;

class Renderer
{
public:
	virtual ~Renderer() {}

	virtual std::shared_ptr<SurfaceRenderable> get_surface_renderable(Surface *surface) = 0;

	virtual std::shared_ptr<BitmapRenderable>
	get_bitmap_renderable(int32_t x, int32_t y, int32_t width, int32_t height, bool has_alpha = true) = 0;

	virtual void set_cursor(Pointer *pointer, Renderable *renderable, int32_t hotx, int32_t hoty) = 0;

	virtual void hide_cursor() = 0;

	virtual void setup_output(Output *output, RendererOutputOptions &opts) = 0;
	virtual void render_output(Output *output) = 0;
};

class RendererOptions
{
public:
	Compositor *compositor;

	virtual ~RendererOptions() {}
};

class RendererOutputOptions
{
public:
	virtual ~RendererOutputOptions() {}
};

} // namespace orbycomp
