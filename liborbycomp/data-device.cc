/*
 * data-device.hh
 *
 * Copyright (C) 2020, 2021 Jani Juhani Sinervo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <fcntl.h>
#include <orbycomp/base/compositor.hh>
#include <orbycomp/base/data-device.hh>
#include <unistd.h>

namespace orbycomp
{

DataDeviceManager::DataDeviceManager(Compositor *compositor)
    : compositor_{compositor}, global_{wl_global_create(compositor_->display(),
							&wl_data_device_manager_interface,
							3,
							this,
							DataDeviceManager::on_global_bind),
				       wl_global_destroy}
{
	wl_list_init(&global_resources_);

	for (const auto &[_, seat_] : compositor_->seats()) {
		data_devices_.try_emplace(seat_, std::make_unique<DataDevice>(this, seat_));
	}

	seat_added_signal_connection_ = compositor_->seat_added_signal.connect([this](Seat *seat) {
		if (data_devices_.find(seat) != data_devices_.end()) {
			return;
		}

		data_devices_.try_emplace(seat, std::make_unique<DataDevice>(this, seat));
	});
}

DataDeviceManager::~DataDeviceManager()
{
	struct wl_resource *resource, *tmp;
	wl_resource_for_each_safe(resource, tmp, &global_resources_) { wl_resource_destroy(resource); }
}

static void
remove_from_list(struct wl_resource *resource)
{
	wl_list_remove(wl_resource_get_link(resource));
}

void
DataDeviceManager::create_data_source(struct wl_client *client, struct wl_resource *resource, uint32_t id)
{
	DataDeviceManager *self = static_cast<DataDeviceManager *>(wl_resource_get_user_data(resource));

	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> resource_{
	    wl_resource_create(client, &wl_data_source_interface, 3, id), wl_resource_destroy};
	if (!resource_) {
		wl_resource_post_no_memory(resource);
		return;
	}

	try {
		DataSource *source = new DataSource(self, resource_.get());
		wl_resource_set_implementation(resource_.get(), &DataSource::iface, source,
					       DataSource::on_resource_destroy);

		resource_.release();
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
DataDeviceManager::get_data_device(struct wl_client *client,
				   struct wl_resource *resource,
				   uint32_t id,
				   struct wl_resource *seat)
{
	DataDeviceManager *self = static_cast<DataDeviceManager *>(wl_resource_get_user_data(resource));

	Seat *seat_ptr = static_cast<Seat *>(wl_resource_get_user_data(seat));

	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> resource_{
	    wl_resource_create(client, &wl_data_device_interface, 3, id), wl_resource_destroy};

	if (!resource_) {
		wl_resource_post_no_memory(resource);
		return;
	}

	auto device_iter = std::find_if(self->data_devices_.begin(), self->data_devices_.end(),
					[seat_ptr](const auto &pair) { return pair.first == seat_ptr; });

	if (device_iter == self->data_devices_.end()) {
		std::fprintf(stderr, "Could not find data device for seat!\n");
		return;
	}

	try {
		device_iter->second->add_resource(resource_.get());
		resource_.release();
	} catch (const std::bad_alloc &) {
		wl_resource_post_no_memory(resource);
	}
}

void
DataDeviceManager::on_global_bind(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	DataDeviceManager *self = static_cast<DataDeviceManager *>(data);
	std::unique_ptr<struct wl_resource, decltype(&wl_resource_destroy)> resource{
	    wl_resource_create(client, &wl_data_device_manager_interface, version, id), wl_resource_destroy};

	if (!resource) {
		wl_client_post_no_memory(client);
		return;
	}

	wl_resource_set_implementation(resource.get(), &DataDeviceManager::iface, self, remove_from_list);

	wl_list_insert(&self->global_resources_, wl_resource_get_link(resource.get()));

	resource.release();
}

DataSource::DataSource(DataDeviceManager *manager, struct wl_resource *resource)
    : manager_{manager}, resource_{resource}
{
}

DataSource::~DataSource() {}

void
DataSource::on_resource_destroy(struct wl_resource *resource)
{
	DataSource *self = static_cast<DataSource *>(wl_resource_get_user_data(resource));

	delete self;
}

void
DataSource::offer(struct wl_client *client, struct wl_resource *resource, const char *mime_type)
{
	DataSource *self = static_cast<DataSource *>(wl_resource_get_user_data(resource));

	if (std::find(self->mimes_.begin(), self->mimes_.end(), mime_type) != self->mimes_.end()) {
		self->mimes_.insert(mime_type);

		// TODO: Advertise to targets.
	}
}

void
DataSource::destroy(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

void
DataSource::set_actions(struct wl_client *client, struct wl_resource *resource, uint32_t dnd_actions)
{
}

DataDevice::DataDevice(DataDeviceManager *manager, Seat *seat) : manager_{manager}, seat_{seat}
{
	wl_list_init(&resources_);
}

DataDevice::~DataDevice()
{
	struct wl_resource *resource, *tmp;
	wl_resource_for_each_safe(resource, tmp, &resources_) { wl_resource_destroy(resource); }
}

void
DataDevice::add_resource(struct wl_resource *resource)
{
	wl_resource_set_implementation(resource, &DataDevice::iface, this, DataDevice::on_resource_destroy);

	wl_list_insert(&resources_, wl_resource_get_link(resource));
}

void
DataDevice::on_resource_destroy(struct wl_resource *resource)
{
	wl_list_remove(wl_resource_get_link(resource));
}

void
DataDevice::start_drag(struct wl_client *client,
		       struct wl_resource *resource,
		       struct wl_resource *source,
		       struct wl_resource *origin,
		       struct wl_resource *icon,
		       uint32_t serial)
{
}

void
DataDevice::set_selection(struct wl_client *client,
			  struct wl_resource *resource,
			  struct wl_resource *source,
			  uint32_t serial)
{
	DataDevice *self = static_cast<DataDevice *>(wl_resource_get_user_data(resource));

	if (!source) {
		return;
	}

	DataSource *source_ptr = static_cast<DataSource *>(wl_resource_get_user_data(source));

	if (source_ptr->actions_ != DataSource::DNDAction::NoAction) {
		wl_resource_post_error(source, WL_DATA_SOURCE_ERROR_INVALID_SOURCE,
				       "Cannot use a drag-and-drop source as a selection");

		return;
	}

	self->set_selection(source_ptr, serial);
}

void
DataDevice::release(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}
void
DataDevice::set_selection(DataSource *source, uint32_t serial)
{
	if (selection_ && selection_serial_ - serial < std::numeric_limits<uint32_t>::max() / 2) {
		return;
	}

	if (selection_) {
		wl_data_source_send_cancelled(selection_->resource_);
		selection_ = nullptr;
	}

	selection_ = source;
	selection_serial_ = serial;

	if (seat_->keyboard()) {
		// TODO: Deal with other objects that may be interested
		// at knowing when a new selection is set.
		Surface *surface = seat_->keyboard()->get_focus_surface();
		if (surface) {
			send_selection(wl_resource_get_client(surface->resource()));
		}
	}
}

void
DataDevice::send_selection(struct wl_client *client)
{
	struct wl_resource *resource;
	wl_resource_for_each(resource, &resources_)
	{
		if (wl_resource_get_client(resource) != client) {
			continue;
		}

		if (selection_) {
			try {
				DataOffer *offer = new DataOffer(this, client, selection_);
				wl_data_device_send_selection(resource, offer->resource_);
			} catch (const std::bad_alloc &) {
				wl_resource_post_no_memory(resource);
			}
		} else {
			wl_data_device_send_selection(resource, nullptr);
		}
	}
}

DataOffer::DataOffer(DataDevice *device, struct wl_client *client, DataSource *source)
    : device_{device}, resource_{wl_resource_create(client, &wl_data_offer_interface, 3, 0)}, source_{source}
{
	if (!resource_) {
		throw std::bad_alloc();
	}

	wl_resource_set_implementation(resource_, &DataOffer::iface, this, DataOffer::on_resource_destroy);
}

DataOffer::~DataOffer() {}

void
DataOffer::on_resource_destroy(struct wl_resource *resource)
{
	DataOffer *self = static_cast<DataOffer *>(wl_resource_get_user_data(resource));
	delete self;
}

void
DataOffer::accept(struct wl_client *client,
		  struct wl_resource *resource,
		  uint32_t serial,
		  const char *mime_type)
{
}

void
DataOffer::receive(struct wl_client *client, struct wl_resource *resource, const char *mime_type, int32_t fd)
{
	DataOffer *self = static_cast<DataOffer *>(wl_resource_get_user_data(resource));

	if (self->source_) {
		wl_data_source_send_send(self->source_->resource_, mime_type, fd);
	} else {
		close(fd);
	}
}

void
DataOffer::destroy(struct wl_client *client, struct wl_resource *resource)
{
}

void
DataOffer::finish(struct wl_client *client, struct wl_resource *resource)
{
}

void
DataOffer::set_actions(struct wl_client *client,
		       struct wl_resource *resource,
		       uint32_t dnd_actions,
		       uint32_t preferred_action)
{
}

} // namespace orbycomp
